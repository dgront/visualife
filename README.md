# VisuaLife : scientific visualization in a browser!

VisuaLive is an interactive visualization library that allows quickly display your data in a web browser.
This unique approach allows you to use Python to quickly add interactivity to the data. The library can be used
for general plotting, however it's aim is to display biological data: sequences and structures of proteins and DNA
as well as their interactions, annotations etc. 

Documentation fot this project is hosted on [Read the Docs](https://visualife.readthedocs.io/en/latest/index.html).
It provides a [gallery of examples](https://visualife.readthedocs.io/en/latest/doc/examples_gallery.html), 
[tutorials](https://visualife.readthedocs.io/en/latest/doc/tutorials.html) and API reference.

In doc_examples/svg_examples you will find examples that produces SVG output. 
In order to run the examples that are distributed with the repository in the doc_examples/html_examples folder,
use any HTTP server, e.g. the one which comes with your Python installation:

python3 -m http.server

