#! /usr/bin/env python

from visualife.core.SvgViewport import SvgViewport
from browser import console, document, html, window
from visualife.core.styles import *


class HtmlViewport(SvgViewport):

    #__slots__ = ['__svg','__svg_height', '__svg_width','__bind_ids','__bind_events','__bind_funcs']

    def __init__(self, div_element, svg_width, svg_height, id="visualife_drawing", **kwargs):
        """Draws graphics on a WWW page, in a given ``<svg>`` element of a HTML page.
        :param div_element: (``string``) name of a DIV element where a SVG element will be inserted
        :param x_min: (``float``) minimum value for x coordinate of graphics
        :param y_min: (``float``) minimum value for y coordinate of graphics
        :param x_max: (``float``) maximum value for x coordinate of graphics
        :param y_max: (``float``) maximum value for y coordinate of graphics
        :param svg_width: (``int``) width of an SVG element in pixels; the ``x_max - x_min`` range will be
            projected on ``svg_width`` pixels
        :param svg_height: (``int``) height of an SVG element in pixels; the ``y_max - y_min`` range will be
            projected on ``svg_height`` pixels
        :param id: (``string``) ID string will be used to identify the content of this viewport in an HTML page;
            it will be assigned to the respective SVG element
        :param kwargs: see below; note that the ``**kwargs`` dictionary will be passed to the base class constructor

        :Keyword Arguments:
            * *if_interactive* (``bool``) --
              when set to ``True``, this viewport will handle drag and zoom events; default is ``False``.
              One can use ``True`` for inner viewports
            * *download_button* (``bool``) --
              when set to true, a "download" button will be drawn in the figure; this allows user to download
              the current state of graphics in this viewport in SVG format
        """
        super().__init__('', svg_width, svg_height, id, **kwargs)

        self.__svg = div_element
        self.__bind_ids = []
        self.__bind_events = []
        self.__bind_funcs = []
        self.__if_download = kwargs.get("download_button", False)
        self.__if_download_ready = False
        self.__is_dragged = False
        self.__dragged_start = (0, 0)
        self.__view_box = (0, 0, 0, 0)
        self.__if_interactive = kwargs.get("if_interactive", False)
        if not isinstance(self.__svg, HtmlViewport) and not isinstance(self.__svg, str):
            if "font-awesome" not in document:
                document <= html.LINK(id="font-awesome",
                    href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css",
                    rel="stylesheet", type="text/css")

            self.__style_tag = html.STYLE(""".default_text_style {%s}
                .default_drawing_style {%s}""" % (self.text_style,self.style))
            self.__svg <= self.__style_tag

    @property
    def svg(self):
        """
        Returns <svg> element of a WWW page
        """
        return self.__svg

    def clear(self):
        """
        Clears the viewport by removing all its children elements
        """
        self.__bind_ids = []
        self.__bind_events = []
        self.__bind_funcs = []
        self.__if_download_ready = False
        super().clear()
        self.__svg.innerHTML = ""

    def error_msg(self,msg):
        """
        Prints error message.

        This polymorphic method prints a given error message to browser's console; SvgViewport will print to std.cerr;
        """
        console.log(msg)

    def viewport_name(self):
        """Returns the name if this viewport, which is always "HTML"

        The method allows dynamic distinction between SVG and HTML viewports
        """
        return "HTML"

    def close(self):
        """Closes the SVG tag and sets styles for drawing and text"""

        svg_as_text = super().close()     # ---- This also closes inner_viewports that are stored by the base class

        if not self.is_inner_viewport:
            svg_as_text = """<style>
                .default_text_style {%s}
                .default_drawing_style {%s}
                </style>\n""" % (self.text_style, self.style) + "\n" + svg_as_text
        if not isinstance(self.__svg, HtmlViewport) and not isinstance(self.__svg, str):
            self.__svg.innerHTML = svg_as_text

        if self.__if_download:
            self.__add_download_button()

        return svg_as_text

    def define_binding(self, id_str, on_what, func):
        """ Binds a function on given event to the element with given id 
        
        :param id_str: id of element you want to bind event to
        :param on_what: HTML event as `mouseover` or `click`
        :param func: function to bind
        """
        self.__bind_ids.append(id_str)
        self.__bind_events.append(on_what)
        self.__bind_funcs.append(func)

    def text_length(self, text, **kwargs):
        """ Measures the dimensions (in pixels) of a text as it would appear on a page

        :param text: text to be drawn
        :param kwargs: styling parameters as to be sent to ``SvgViewport.text()`` method
        :return: width and height of the text element, in pixels
        """
        backup = self.innerHTML
        self._SvgViewport__innerHTML = ""
        self.text("test-text", 0, 0, text, **kwargs)
        svg_text = self.innerHTML
        document <= html.DIV(id='test-test', style={'visibility': 'hidden'})
        document['test-test'].innerHTML = svg_text
        bb = document['test-text'].getBoundingClientRect()
        w = bb.right - bb.x
        h = bb.bottom - bb.y
        self._SvgViewport__innerHTML = backup
        del document['test-test']
        return w, h

    def __show_dwnld_button(self, evt):
        document[self.__svg.id + ':' + 'download_plot'].style.visibility="visible"

    def __hide_dwnld_button(self, evt):
        document[self.__svg.id + ':' + 'download_plot'].style.visibility="hidden"

    def __drag_start(self, evt):
        evt.preventDefault()
        self.__save_view_box()
        self.__is_dragged = True
        self.__dragged_start = evt.clientX, evt.clientY

    def __drag_end(self, evt):
        evt.preventDefault()
        self.set_viewbox(self.__view_box[0], self.__view_box[1], self.__view_box[2], self.__view_box[3])
        self.__is_dragged = False

    def __drag_move(self, evt):

        if not self.__is_dragged: return
        evt.preventDefault()

        x, y = evt.clientX, evt.clientY
        nx = self.__view_box[0] - (x - self.__dragged_start[0]) * self.scale_x()
        ny = self.__view_box[1] - (y - self.__dragged_start[1]) * self.scale_y()
        document[self.id].setAttribute('viewBox', "%f %f %f %f" % (nx, ny, self.__view_box[2], self.__view_box[3]))

    def __hoover_viewport(self, evt):
        if self.is_inner_viewport:
            document[self.parent_viewport.id].appendChild(evt.target)

    def __zoom(self, evt):
        self.__save_view_box()
        scale = 0.99 if evt.deltaY < 0 else 1.05
        nx = self.__view_box[0]
        ny = self.__view_box[1]
        w = self.__view_box[2] * scale
        h = self.__view_box[3] * scale
        document[self.id].setAttribute('viewBox', "%f %f %f %f" % (nx, ny, w, h))
        self.set_viewbox(nx, ny, w, h)

    def __add_download_button(self):
        """Adds a button to download current image"""

        # --- don't add a download button for inner viewports
        if self.__if_download_ready or isinstance(self.__svg, HtmlViewport): return

        download_id = self.__svg.id + ':' + 'download_plot'
        tooltip = html.A(id=download_id, style={"font-size": "10px", "background": "white",
                    "opacity": "0.8", "text-align": "center"})
        triangle = html.DIV(Class="fa fa-download", style={"font-size": "18px"})
        tooltip <= triangle
        tooltip <= html.DIV("download<br> plot as SVG")
        box = self.__svg.getBoundingClientRect()
        tooltip.style = {'position': 'absolute', 'visibility': "hidden", 'top': str(box.top + 20) + 'px',
                         'left': str(box.right - 30 - box.left) + 'px'}
        tooltip["href_lang"] = 'image/svg+xml'
        tooltip["download"] = "plot.svg"
        self.__svg.parent <= tooltip

        document[self.__svg.id].bind('mouseover',self.__show_dwnld_button)
        document[self.__svg.id].bind('mouseout',self.__hide_dwnld_button)
        document[download_id].bind('mouseover',self.__show_dwnld_button)
        document[download_id].bind('mouseout',self.__hide_dwnld_button)
        svgAsXML = window.XMLSerializer.new().serializeToString(self.__svg)
        document[download_id]["href"] = "data:image/svg+xml;utf8," + window.encodeURIComponent(svgAsXML)
        self.__if_download_ready = True

    def apply_binding(self):

        for i in range(len(self.__bind_ids)):
            document[self.__bind_ids[i]].bind(self.__bind_events[i], self.__bind_funcs[i])

        # ---------- Stuff necessary for viewport dragging and zooming
        if self.__if_interactive:
            svg_el = self.__save_view_box()
            svg_el.bind('mouseenter', self.__hoover_viewport)  # Mouse-enter brings a viewport to front
            svg_el.bind('mousedown', self.__drag_start)  # Pressing the mouse
            svg_el.bind('mouseup', self.__drag_end)  # Releasing the mouse
            svg_el.bind('mouseleave', self.__drag_end)  # Mouse gets out of the SVG area
            svg_el.bind('mousemove', self.__drag_move)  # Mouse is moving
            svg_el.bind('wheel', self.__zoom)  # Mouse is moving

    def __save_view_box(self):
        """copies actual parameters of the viewbox into private data of this object"""
        svg_el = document[self.id]
        vb = svg_el.viewBox.baseVal
        self.__view_box = (vb.x, vb.y, vb.width, vb.height)
        return svg_el