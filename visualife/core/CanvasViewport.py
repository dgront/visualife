#! /usr/bin/env python

import sys
from visualife.core.styles import hex_to_rgb
from math import pi,sin,cos,floor
try:
    from browser import console,window,html,document
except:
    pass

class CanvasViewport:
    """
    Creates a Canvas drawing

    .. raw:: html

          <canvas id="canvas"  height="100" width="400"></canvas>
          <script type="text/python">
            from browser import document
            from visualife.core import CanvasViewport
            from visualife.core.styles import known_color_scales

            cnvs = document["canvas"].getContext("2d")
            drawing = CanvasViewport(cnvs,400,100)
            j=1
            i=1
            width=30
            height=30
            xx,yy=[],[]
            c=known_color_scales["magma"]
            for z in range(40):
                x = 1.5 * width * i
                y = height/2 * j
                if j % 2 == 0:
                    x += 0.75 * width
                i += 1
                if i == 9:
                    j += 1
                    i = 1
                xx.append(x)
                yy.append(y)
               
            drawing.rhomb_group("rhombs",xx,yy,c,15)
          </script>


    It has been created by the following code:

    .. code-block:: Python

        from browser import document
            from visualife.core import CanvasViewport
            from visualife.core.styles import known_color_scales

            cnvs = document["canvas"].getContext("2d")
            drawing = CanvasViewport(cnvs,400,100)
            j=1
            i=1
            width=30
            height=30
            xx,yy=[],[]
            c=known_color_scales["magma"]
            for z in range(40):
                x = 1.5 * width * i
                y = height/2 * j
                if j % 2 == 0:
                    x += 0.75 * width
                i += 1
                if i == 9:
                    j += 1
                    i = 1
                xx.append(x)
                yy.append(y)
               
            drawing.rhomb_group("rhombs",xx,yy,c,15)
            

    """

    __slots__ = ['__canvas','__viewport_width','__viewport_height']

    def __init__(self, canvas, width, height):
        """
        Defines a drawing area
        """
        self.__canvas = canvas
        self.__viewport_width = width
        self.__viewport_height = height
        self.__if_stroke = False
        self.__last_scale = 1.0

    def __prepare_attributes(self, **kwargs):
        """Sets all attributes connected with drawing style and text style 
        """
        if 'fill' in kwargs : self.__canvas.fillStyle = str(kwargs['fill'])
        if 'stroke_width' in kwargs:
            if kwargs['stroke_width'] == 0:
                self.__if_stroke = False
            else:
                self.__canvas.lineWidth = kwargs['stroke_width']
        self.__canvas.strokeStyle="#000000"
        if 'stroke' in kwargs :
            if kwargs['stroke'] == "none" :
                self.__if_stroke = False
            else:
                self.__canvas.strokeStyle = kwargs['stroke']
        font_str = ' '
        if 'font_weight' in kwargs:
            font_str+=kwargs['font_weight']+" "
        font_str += " %dpx " % kwargs.get('font_size',12)
        font_str += " "+ kwargs.get('font_family',"Arial")
        self.__canvas.font= font_str

        if 'text_anchor' in kwargs:
            if kwargs['text_anchor'] == "middle":
                self.__canvas.textAlign="center"
            else:
                self.__canvas.textAlign=kwargs['text_anchor']

    def rect(self,id_str,x,y,w,h,**kwargs):
        """Draws a rect 

        :param id_str: id of this rect
        :param x: x coordinate
        :param y: y coordinate
        :param w: width of this rect
        :param h: height of this rect
        :return: None
        """
        self.__prepare_attributes(**kwargs)
        if 'fill' in kwargs:
            self.__canvas.fillRect(x,y,w,h)

        if self.__if_stroke:
            self.__canvas.strokeRect(x,y,w,h)

    def square(self, id_str, x, y, a, **kwargs):
        """Draws a square

        :param id_str: id of this square
        :param x: x coordinate of the center of this square
        :param y: y coordinate of the center of this square
        :param a: side length 
        :return: None
        """
        self.__prepare_attributes(**kwargs)
        if 'fill' in kwargs:
            self.__canvas.fillRect(x-a/2, y-a/2, a, a)
        if self.__if_stroke:
            self.__canvas.strokeRect(x-a/2, y-a/2, a, a)

    def circle(self,id_str,x,y,r,**kwargs):
        """Draws a circle

        :param id_str: id of this circle
        :param x: x coordinate
        :param y: y coordinate
        :param r: radius length 
        :return: None
        """
        self.__prepare_attributes(**kwargs)
        self.__canvas.beginPath()
        self.__canvas.arc(x, y, r,0,2*pi)
        self.__canvas.fill()
        self.__canvas.stroke()


    def line(self,id_str,xb, yb, xe, ye, **kwargs):
        """Draws a line

        :param id_str: id of this line
        :param xb: x coordinate of line begin
        :param yb: y coordinate of line begin
        :param xe: side length of line end
        :param ye: side length of line end
        :return: None
        """
        self.__prepare_attributes(**kwargs)
        self.__canvas.beginPath()
        self.__canvas.moveTo(xb, yb)
        self.__canvas.lineTo(xe, ye)
        self.__canvas.stroke()

    def ellipse(self,id_str,x, y, rx, ry, **kwargs):
        """Draws an ellipse

        :param id_str: id of this ellipse
        :param x: x coordinate of a center
        :param y: y coordinate of a center
        :param rx: x radius length 
        :param ry: y radius length 
        :return: None
        """
        raise NotImplementedError

    def polygon(self,id_str,points,**kwargs):
        """Draws a polygon

        :param id_str: id of this polygon
        :param points: list of points
        :return: None
        """
        self.__prepare_attributes(**kwargs)
        self.__canvas.beginPath()
        self.__canvas.moveTo(points[0][0], points[0][1])
        for p in points[1:]:
            self.__canvas.lineTo(p[0], p[1])
        self.__canvas.closePath()
        self.__canvas.fill()
        self.__canvas.stroke()

    def triangle(self,id_str,x,y,r,**kwargs):
        """Draws a tiangle

        :param id_str: id of this triangle
        :param x: x coordinate of a center
        :param y: y coordinate of a center
        :param r: side length 
        :return: None
        """
        angle = 2 * pi / 3.0
        points = [[x + r * sin(0 * angle), y + r * cos(0 * angle)],
                            [x + r * sin(1 * angle), y + r * cos(1 * angle)],
                            [x + r * sin(2 * angle), y + r * cos(2 * angle)]]
        self.polygon(id_str,points,**kwargs)


    def rhomb(self,id_str,x,y,r,**kwargs):
        """Draws a rhomb

        :param id_str: id of this rhomb
        :param x: x coordinate of a center
        :param y: y coordinate of a center
        :param r: side length 
        :return: None
        """
        points = [[x, y + r], [x + r, y], [x, y - r], [x - r, y]]
        self.polygon(id_str,points,**kwargs)

    
    def text(self,id_str,x,y,text,**kwargs):
        """Draws a text

        :param id_str: id of this text
        :param x: x coordinate of a center
        :param y: y coordinate of a center
        :param text: text to be written
        :return: None
        """
        self.__prepare_attributes(**kwargs)
        self.__canvas.fillText(text, x, y)

    def circles_group(self,gid,x,y,c,r,**kwargs):
        """Draws a circles group

        :param id_str: id of this group
        :param x: list of x coordinates 
        :param y: list of y coordinates 
        :param r: radius length or a list of radius lengths
        :return: None
        """

        if not isinstance(r, list):
                r = [r]
        for i in range(len(x)):
            self.circle(gid+":"+str(i),x[i],y[i],r[i%len(r)],fill=c[i%len(c)].__str__())

    def squares_grid(self, gid, x0, y0, w, h, rows, columns, **kwargs):
        """Draws a squares group

        :param id_str: id of this group
        :param x: list of x coordinates 
        :param y: list of y coordinates 
        :param c: list of colors for the squares
        :param a: side length
        :return: None
        """
        def color_pixel(i,color):
            c = hex_to_rgb(color)
            imageData.data[i + 0] = c[0];  # R value
            imageData.data[i + 1] = c[1];    # G value
            imageData.data[i + 2] = c[2];  # B value
            imageData.data[i + 3] = 255;  # A value

        colors=kwargs.get("colors",["pink"])

        imageData = self.__canvas.createImageData(w*columns, h*rows)
        for ir in range(h*rows):
                    for iw in range(columns*w):
                        ii = ir*imageData.width*4+iw*4
                        cc= int(iw/w)+floor(ir/h)*columns
                        #print(cc)
                        color_pixel(ii,colors[(cc)%len(colors)])

        self.__canvas.putImageData(imageData, x0, y0);

    def squares_group(self, id_str, x, y, c, a, **kwargs):
        """Draws a squares group

        :param id_str: id of this group
        :param x: list of x coordinates 
        :param y: list of y coordinates 
        :param c: list of colors for the squares
        :param a: side length
        :return: None
        """
        self.__prepare_attributes(**kwargs)

        for i in range(len(x)):
            self.__canvas.fillStyle = str(c[i])

            self.__canvas.fillRect(x[i] - a / 2, y[i] - a / 2, a, a)
        if 'stroke' in kwargs:
            for i in range(len(x)):
                self.__canvas.strokeRect(x[i] - a / 2, y[i] - a / 2, a, a)

    def triangle_group(self,gid,x,y,c,r,**kwargs):
        """Draws a triangle group

        :param id_str: id of this group
        :param x: list of x coordinates 
        :param y: list of y coordinates 
        :param r: side length 
        :return: None
        """
        for i in range(len(x)):
            self.triangle(gid+":"+str(i),x[i],y[i],r,fill=c[i%len(c)].__str__())

    def rhomb_group(self,gid,x,y,c,r,**kwargs):
        """Draws a rhomb group

        :param id_str: id of this group
        :param x: list of x coordinates 
        :param y: list of y coordinates 
        :param r: side length 
        :return: None
        """
        for i in range(len(x)):
            self.rhomb(gid+":"+str(i),x[i],y[i],r,fill=c[i%len(c)].__str__())

    def translate(self,delta_x,delta_y):
        """Sets delta_x and delta_y as new (0,0) point """
        self.__canvas.translate(delta_x,delta_y)

    def viewport_name(self):
      """Returns the name if this viewport, which is always "CANVAS"

      """
      return "CANVAS"

    def get_width(self):
        """Returns viewport width
        """
        return self.__viewport_width

    def get_height(self):
        """Returns viewport height
        """
        return self.__viewport_height

    def error_msg(self,msg):
        """
        Prints error message 

        Prints error message to screen or browser's console
        """
        print(msg)

    def clear(self, x=0, y=0):
        """Clears viewport"""
        self.__canvas.clearRect(0, 0,x+self.get_width(), y+self.get_height())

    def close(self):
        """ Does nothing; implemented to comply with the base class interface
        """
        pass

    def binary_map(self, matrix_or_string: list or str, dimension_rows: int, dimension_columns: int, cmap):
        """Draws a graphic representation of 2D matrix

        :param matrix_or_string: ``list[list]`` or a ``string`` holding the data
        :param dimension_rows: number of rows
        :param dimension_columns: number of columns
        :param cmap: ColorMap object used to convert values into colors
        """
        def draw(id):
            self.__canvas.scale(scale_for_now, scale_for_now)
            self.__canvas.imageSmoothingEnabled = False
            self.__canvas.drawImage(id, 0, 0, dimension_columns, dimension_rows,
                                    0, 0, dimension_columns, dimension_rows)

        scale = float(min(self.__viewport_width / dimension_columns,self.__viewport_height/dimension_rows))
        scale_for_now = 1.0
        print(self.__last_scale, scale, scale_for_now)
        if self.__last_scale !=scale:
            scale_for_now = float(scale/self.__last_scale)
            self.__last_scale = scale
        print(self.__last_scale, scale, scale_for_now)
        if type(matrix_or_string).__name__ == "JSObj" and matrix_or_string.length == 4 * dimension_columns * dimension_rows:
            id = window.ImageData.new(matrix_or_string, dimension_columns, dimension_rows)
            window.createImageBitmap(id).then(draw)
            return
        # --- pre-allocate memory for the array of colors
        a = window.Uint8ClampedArray.new(4 * dimension_columns * dimension_rows)
        print(type(a), a.length)
        if isinstance(matrix_or_string, str):
            k = 0
            for i in matrix_or_string:
                i_color = cmap.color_tuple(i)
                a[k] = i_color[0]
                a[k + 1] = i_color[1]
                a[k + 2] = i_color[2]
                a[k + 3] = 255
                k += 4
        elif isinstance(matrix_or_string, list):
            if isinstance(matrix_or_string[0], list):
                k = 0
                for row in matrix_or_string:
                    for i in row:
                        i_color = cmap.color_tuple(i)
                        a[k] = i_color[0]
                        a[k + 1] = i_color[1]
                        a[k + 2] = i_color[2]
                        a[k + 3] = 255
                        k += 4
            else:
                k = 0
                for i in matrix_or_string:
                    i_color = cmap.color_tuple(i)
                    a[k] = i_color[0]
                    a[k+1] = i_color[1]
                    a[k+2] = i_color[2]
                    a[k+3] = 255
                    k += 4

        id = window.ImageData.new(a, dimension_columns, dimension_rows)
        window.createImageBitmap(id).then(draw)

