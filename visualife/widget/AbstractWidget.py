import sys


class AbstractWidget:

    def __init__(self, element_id):
        """Abstract base class for VisuaLife widgets

        :param element_id: ID of a html DIV element that will contain this SequenceViewer instance
        """

        self.__element_id = element_id
        self._known_events = []
        self._event_callbacks = {}

    @property
    def element_id(self):
        """Provides ID of the page element that holds this widget (parent element)

        :return: ID of the parent HTML element
        """
        return self.__element_id

    @property
    def known_events(self):
        """Provides a list of all event names known to this widget

        :return: a deep copy (for safety) of names of all events this widget can handle
        """
        return [o for o in self._known_events]

    def set_event_callback(self, event_name, callback_function):

        if event_name not in self._known_events:
            print("ERROR: unknown event name:", event_name, file=sys.stderr)
            raise ValueError("ERROR: unknown event name:" + event_name)
        self._event_callbacks[event_name] = callback_function
