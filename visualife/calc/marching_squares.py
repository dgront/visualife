from typing import List
#import numba
import functools
import operator

#@numba.jit(nopython=True)
def get_case_id(Point_A_data: float, Point_B_data: float,
                Point_C_data: float, Point_D_data: float,
                threshold: float):
    caseId = 0
    if Point_A_data >= threshold:
        caseId |= 1
    if Point_B_data >= threshold:
        caseId |= 2
    if Point_C_data >= threshold:
        caseId |= 4
    if Point_D_data >= threshold:
        caseId |= 8
    return caseId


def get_lines(point_a: List[float], point_b: List[float], point_c: List[float], point_d: List[float],
              data_a: float, data_b: float, data_c: float, data_d: float,
              threshold: float):
    lines = []
    case_id = get_case_id(data_a, data_b, data_c, data_d, threshold)

    if case_id in (0, 15):
        return None

    if case_id in (1, 14, 10):
        pX = (point_a[0] + point_b[0]) / 2
        pY = point_b[1]
        qX = point_d[0]
        qY = (point_a[1] + point_d[1]) / 2

        line = (pX, pY, qX, qY)

        lines.append(line)

    if case_id in (2, 13, 5):
        pX = (point_a[0] + point_b[0]) / 2
        pY = point_a[1]
        qX = point_c[0]
        qY = (point_a[1] + point_d[1]) / 2

        line = (pX, pY, qX, qY)

        lines.append(line)

    if case_id in (3, 12):
        pX = point_a[0]
        pY = (point_a[1] + point_d[1]) / 2
        qX = point_c[0]
        qY = (point_b[1] + point_c[1]) / 2

        line = (pX, pY, qX, qY)

        lines.append(line)

    if case_id in (4, 11, 10):
        pX = (point_c[0] + point_d[0]) / 2
        pY = point_d[1]
        qX = point_b[0]
        qY = (point_b[1] + point_c[1]) / 2

        line = (pX, pY, qX, qY)

        lines.append(line)

    elif case_id in (6, 9):
        pX = (point_a[0] + point_b[0]) / 2
        pY = point_a[1]
        qX = (point_c[0] + point_d[0]) / 2
        qY = point_c[1]

        line = (pX, pY, qX, qY)

        lines.append(line)

    elif case_id in (7, 8, 5):
        pX = (point_c[0] + point_d[0]) / 2
        pY = point_c[1]
        qX = point_a[0]
        qY = (point_a[1] + point_d[1]) / 2

        line = (pX, pY, qX, qY)

        lines.append(line)

    return lines

# <editor-fold desc="__marching_square_data() - alternative implementation">
'''
def __marching_square_data(x_int_list, y_int_list, data_2d, threshold_item):
    lines_list = []
    height = len(y_int_list)  # rows
    width = len(x_int_list)  # cols

    if width == len(data_2d[0]) and height == len(data_2d):
        for j in range(height - 1):  # rows
            for i in range(width - 1):  # cols
                point_a_data_float = data_2d[j + 1][i]
                point_b_data_float = data_2d[j + 1][i + 1]
                point_c_data_float = data_2d[j][i + 1]
                point_d_data_float = data_2d[j][i]

                point_A = [x_int_list[i], y_int_list[j + 1]]
                point_B = [x_int_list[i + 1], y_int_list[j + 1]]
                point_C = [x_int_list[i + 1], y_int_list[j]]
                point_D = [x_int_list[i], y_int_list[j]]

                temp_list = get_lines(point_A, point_B, point_C, point_D, point_a_data_float, point_b_data_float, point_c_data_float, point_d_data_float, threshold_item)
                if temp_list:
                    lines_list = [*lines_list, *temp_list]
                # END of IF list
            # END of FOR ... range(width - 1)
        # END of FOR ... range(height - 1)
    # END of IF
    else:
        raise AssertionError
    # END of ELSE
    return lines_list
# END of marching_square()
'''
# </editor-fold>


# <editor-fold desc="__marching_square_data()">
def __marching_square_data(x_int_list, y_int_list, threshold_item, data_2d):
    linesList = []

    Height = len(y_int_list)  # rows
    Width = len(x_int_list)  # cols

    if ((Width == len(data_2d[0])) and (Height == len(data_2d))):
        for j in range(Height - 1):  # rows
            for i in range(Width - 1):  # cols
                point_a_data_float = data_2d[j + 1][i]
                point_b_data_float = data_2d[j + 1][i + 1]
                point_c_data_float = data_2d[j][i + 1]
                point_d_data_float = data_2d[j][i]

                point_A = [x_int_list[i], y_int_list[j + 1]]
                point_B = [x_int_list[i + 1], y_int_list[j + 1]]
                point_C = [x_int_list[i + 1], y_int_list[j]]
                point_D = [x_int_list[i], y_int_list[j]]

                list = get_lines(point_A, point_B, point_C, point_D,
                                    point_a_data_float, point_b_data_float, point_c_data_float, point_d_data_float,
                                    threshold_item)
                if list:
                    linesList.append(list)

    else:
        raise AssertionError

    return functools.reduce(operator.iconcat, linesList, [])
# </editor-fold>


def __marching_square_func(x_int_list, y_int_list, threshold_item, function, resolution):
    linesList = []

    Height = len(y_int_list)  # rows
    Width = len(x_int_list)  # cols

    for y in range(Height - 1):  # rows (Y-axis)
        for x in range(Width - 1):  # cols (X-axis)
            point_A = [x_int_list[x], y_int_list[y + 1]]
            point_B = [x_int_list[x + 1], y_int_list[y + 1]]
            point_C = [x_int_list[x + 1], y_int_list[y]]
            point_D = [x_int_list[x], y_int_list[y]]

            point_a_data_float = function(x, y + 1, Width, Height, resolution)  # data_2d[y + 1][x]
            point_b_data_float = function(x + 1, y + 1, Width, Height, resolution)  # data_2d[y + 1][x + 1]
            point_c_data_float = function(x + 1, y, Width, Height, resolution)  # data_2d[y][x + 1]
            point_d_data_float = function(x, y, Width, Height, resolution)  # data_2d[y][x]

            list = get_lines(point_A, point_B, point_C, point_D,
                             point_a_data_float, point_b_data_float, point_c_data_float, point_d_data_float,
                             threshold_item)
            if list:
                linesList.append(list)
            # END of IF
    return functools.reduce(operator.iconcat, linesList, [])


def marching_square(x_int_list, y_int_list, threshold_item, **kwargs):
    data = kwargs.get('data', [])
    function = kwargs.get('function', None)
    resolution = kwargs.get('resolution', None)

    if len(data) != 0:
        return __marching_square_data(x_int_list, y_int_list, threshold_item, data)
    else:
        return __marching_square_func(x_int_list, y_int_list, threshold_item, function, resolution)
    # END of IF
# END of marching_square()

