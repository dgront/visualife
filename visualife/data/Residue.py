class Residue:
    """ Represents a residue including its name, id, atoms, owner etc.
    """
    def __init__(self, rname, resid, icode=' '):
        self.__res_name = rname
        self.__res_id = resid
        self.__icode = icode
        self.__atoms = []
        self.__owner = None

    def __str__(self):
        """Returns a string representation of this residue, e.g. *VAL 13*"""
        return "%s %d" % (self.__res_name, self.__res_id)

    def __eq__(self, other):
        """Returns true if this residue equals to ``other`` residue"""
        return self.owner.chain_id == other.owner.chain_id and self.res_id == other.res_id and self.icode == other.icode

    def __lt__(self, other):
        """Returns true if this residue should appear *before* the ``other`` residue in  a PDB file"""
        if self.owner.chain_id != other.owner.chain_id: return self.owner < other.owner
        if self.__res_id != other.__res_id: return self.__res_id < other.__res_id
        return self.__icode < other.__icode

    def __gt__(self, other):
        """Returns true if this residue should appear *after* the ``other`` residue in  a PDB file"""
        if self.owner.chain_id != other.owner.chain_id: return self.owner > other.owner
        if self.res_id != other.__res_id: return self.res_id > other.res_id
        return self.icode > other.icode

    @property
    def res_name(self):
        """name of this residue in the 3-letter code

        :getter: name of this residue, e.g. ``ALA``
        :setter: Sets this residue name
        :type: ``string``
        """
        return self.__res_name

    @res_name.setter
    def res_name(self, new_name):
        self.__res_name = new_name

    @property
    def atoms(self):
        """list of atoms of this residue

        :getter: a list of atoms
        :type: ``list[Atom]``
        """
        return self.__atoms

    @property
    def res_id(self):
        """residue ID

        :getter: index of a residue (might be negative!)
        :setter: Sets this residue ID number
        :type: ``int``
        """
        return self.__res_id

    @res_id.setter
    def res_id(self, new_id):
        self.__res_id = new_id

    @property
    def icode(self):
        """insertion code for this residue

        :getter: insertion code for this residue
        :setter: sets insertion code for this residue
        :type: ``char``
        """
        return self.__icode

    @icode.setter
    def icode(self, icode):
        self.__icode = icode

    @property
    def owner(self):
        """provides the chain that owns this residue

        :getter: residue owner
        :setter: sets the new owner for this residue
        :type: ``Chain``
        """
        return self.__owner

    @owner.setter
    def owner(self, new_owner):
        self.__owner = new_owner

    def locator(self):
        """Returns a string that identifies this residue
        :return: a sting that consist of chain id (single character) + residue id (integer) + icode (most often a space)
        """
        return "%c%d%c" % (self.owner.chain_id if self.owner else " ", self.res_id, self.icode)
