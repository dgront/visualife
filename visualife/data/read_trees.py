import re

from visualife.diagrams import DendrogramNode


def read_clustering_tree(fname_or_data, if_read_file=False):

    """Read a clustering tree produced by BioShell package.
    :param fname_or_data: (``string``) - a file name or the data itself
    :param if_read_file: (``boolean``) - if true, it's assumed the input is a file name
        and the data must be loaded from a file. Otherwise, the string will be parsed
    :return: a tree nodes (``DendrogramNode``)
    """
    nodes_by_id = {}
    last_node = None
    if if_read_file:                   # --- most likely it's a file - read it content
        data = open(fname_or_data).read()
    else:
        data = fname_or_data
    for line in data.split("\n"):
        if line.find('@') < 0: continue
        tokens = line.strip().split()
        if len(tokens) >= 6:
            if tokens[0] == '>':
                idx = int(tokens[1])
                nodes_by_id[idx] = DendrogramNode(idx, tokens[5], 0)
                print("created leaf", nodes_by_id[idx])
            elif tokens[2] == '>':
                idl = int(tokens[0])
                idr = int(tokens[1])
                idx = int(tokens[3])
                d = float(tokens[5])
                last_node = DendrogramNode(idx, None, d, nodes_by_id[idl], nodes_by_id[idr])
                nodes_by_id[idx] = last_node
                print("created node", nodes_by_id[idx])
            else:
                print("skipping line: ", line)
        else:
            print("skipping line: ", line)

    return last_node


def read_newick_dictionary(newick, if_read_file=False):

    """Reads a tree in the Newick format
    :param newick: (``string``) - a file name or the data itself
    :param if_read_file: (``boolean``) - if true, it's assumed the input is a file name
        and the data must be loaded from a file. Otherwise, the string will be parsed
    :return: a dictionary of tree nodes
    """
    if if_read_file:                   # --- most likely it's a file - read it content
        newick = open(newick).read()

    tokens = re.findall(r"([^:;,()\s]*)(?:\s*:\s*([\d.]+)\s*)?([,);])|(\S)", newick+";")

    def recurse_parsing(nextid = 0, parentid = -1): # one node
        thisid = nextid;
        children = []

        name, length, delim, ch = tokens.pop(0)
        if ch == "(":
            while ch in "(,":
                node, ch, nextid = recurse_parsing(nextid+1, thisid)
                children.append(node)
            name, length, delim, ch = tokens.pop(0)

        return {"id": thisid, "name": name, "length": float(length) if length else None,
                "parentid": parentid, "children": children}, delim, nextid

    return recurse_parsing()[0]


def tree_from_dictionary(dict_root, node_class=DendrogramNode):
    """
    Transforms a tree from a dictionary representation into a "true" tree of ``DendrogramNode`` nodes
    :param node_dict: (``string``) - dictionary of nodes, e.g. loaded by ``read_newick_dictionary()`` function
    :param node_class: (``class``) - use ``DendrogramNode`` (which is the default) or ``MSADendrogramNode``
    :return: a root node of the tree
    """

    def recurse_repack(node_dict):
        name = None if len(node_dict["name"]) == 0 else node_dict["name"]
        n = node_class(node_dict["id"], name, node_dict["length"])
        children = node_dict["children"]
        for child in children:
            n.append(recurse_repack(child))
        return n

    return recurse_repack(dict_root)


if __name__ == "__main__":
    import json, sys
    if sys.argv[1].isdecimal():
        json.dump(read_newick_dictionary(sys.argv[2], True), sys.stdout, indent=int(sys.argv[1]))
    else:
        json.dump(read_newick_dictionary(sys.argv[1], True), sys.stdout)
