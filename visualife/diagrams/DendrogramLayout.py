from visualife.diagrams import *


class DendrogramLayout(TreeLayout):
    def __init__(self, id, viewport, max_screen_x, max_screen_y, **kwargs):
        """Draws a binary tree using a dendrogram layout

        Dendrogram layout is suitable for drawing clustering results, phylogenetic trees etc.

        :param id: (``string``) a string that is used to identify graphical elements of this tree.
        ID of a node will be created as ``ID-idx`` where ``ID`` is the ID of this layout object
        and ``idx`` is the index of a node
        :param viewport: (``SvgViewport``) its derivative
        :param min_screen_x: (``int``) - left screen coordinate
        :param max_screen_x: (``int``) - right screen coordinate
        :param min_screen_y: (``int``) - top screen coordinate
        :param max_screen_y: (``int``) - bottom screen coordinate
        :param kwargs:  - see below

        :Keyword Arguments ``(**kwargs)``:
            * *width* (``number``) --
              width of each node in pixels
            * *height* (``number``) --
              height of each node in pixels
            * *separation* (``number``) --
              minimum separation between nodes in pixels
        """
        super().__init__(id, viewport, max_screen_x, max_screen_y, **kwargs)

    def draw(self, root_node: TreeNode, **kwargs):
        """Draws a binary tree given by its root node
        :param root_node: (``BTreeNode``) - root of the tree
        :param kwargs: see arguments parsed TreeLayout.set_style();
        """

        self.set_style(**kwargs)
        root_node.x = 0
        root_node.y = 0
        all_nodes = root_node.nodes()
        self._set_location(root_node, self.leaves_on_axis, self.scale_edges)

        node_centers = []
        leaf_centers = []
        self._viewport.start_group("edges", stroke_width=self.edge_width, stroke=self.edge_color)
        for node in all_nodes:
            id_str = self.id + "-" + str(node.id)
            ny = node.y * self.y_max
            for child in node.children:
                cx = child.x
                cy = child.y * self.y_max
                self._viewport.line(id_str + "-" + str(child.id) + "-h", node.x, ny, cx, ny)
                self._viewport.line(id_str + "-" + str(child.id) + "-v", cx, ny, cx, cy)

            if len(node.children) > 0:
                node_centers.append((id_str, node.x, ny))
            else:
                leaf_centers.append((id_str, node.x, ny))

        self._viewport.close_group()

        self._draw_inner_nodes(node_centers, root_node.count_nodes())
        self._draw_leaf_nodes(leaf_centers, root_node.count_nodes())

    def _set_location(self, root: TreeNode, leaves_on_axis=False, scale_edges=False):

        leaves = TreeNode.collect_leaves(root)
        w = len(leaves) * self.wx + (len(leaves) - 1) * self.sep  # --- total width of the dendrogram
        leaves[0].x = self.x_margin
        for i in range(len(leaves)):
            leaves[i].x = leaves[i - 1].x + self.sep + self.wx

        def recursion(node: TreeNode):
            """Set Y based on level index and X as the average of left and right node"""
            if node.parent:
                if scale_edges and isinstance(node, DendrogramNode):
                    node.y = node.parent.y + node.distance
                else:
                    node.y = node.parent.y + self.wy
            x = 0
            nc = 0
            for child in node.children:
                recursion(child)
                x += child.x
                nc += 1
            if nc > 0:
                node.x = x / nc
        recursion(root)

        self._tree_x_min = min(root.nodes(), key=lambda n: n.x).x
        self._tree_x_max = max(root.nodes(), key=lambda n: n.x).x
        self._tree_y_min = min(root.nodes(), key=lambda n: n.y).y
        self._tree_y_max = max(root.nodes(), key=lambda n: n.y).y
        if scale_edges:
            for n in root.nodes(): n.y = n.y / self._tree_y_max
        if leaves_on_axis:
            if scale_edges:
                for n in leaves: n.y = 1
            else:
                for n in leaves: n.y = self._tree_y_max

