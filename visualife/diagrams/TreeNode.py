class TreeNode:

    def __init__(self, id, value):
        """Creates a tree node.
        Leaves should be appended later
        :param id: (``string``) a string that is used to identify graphical elements of this tree.
        ID of a node will be created as ``ID-idx`` where ``ID`` is the ID of a layout object and ``idx`` is the index of this node
        :param value: value stored at this node
        """
        self.__id = id
        self.value = value
        self._parent = None
        self._children = []
        self.x = 0
        self.y = 0
        self.w = 0
        self.__nodes_list = []

    @property
    def id(self):
        return self.__id

    @property
    def parent(self):
        return self._parent

    def is_leaf(self):
        return len(self._children) == 0

    @property
    def children(self):
        """Returns a reference  of the list of children nodes"""

        # out = [c for c in self._children if c]
        return self._children

    @property
    def children_deepcopy(self):
        """Returns a deep copy  of the list of children nodes without None elements"""

        out = [c for c in self._children if c]
        return out

    def append(self, n):
        """Appends a children node i.e. a leaf to this tree"""
        self._children.append(n)
        n._parent = self

    def nodes(self, if_update=False):
        """
        Returns a list of all nodes of this tree
        :param if_update: (``boolean``) - if True, the list will be collected from scratch,
            otherwise a cached copy is returned
        :return: a list of all nodes of this tree
        """
        if len(self.__nodes_list) == 0 or if_update:
            self.__nodes_list = []
            for node in self:
                self.__nodes_list.append(node)
        return self.__nodes_list

    def __str__(self):
        leaves = []
        for c in self.children_deepcopy:
            leaves.append(c.id)
        return "%d (%s) : " % (self.__id, self.value) + str(leaves)

    def __iter__(self):
        """Iterate in pre-order depth-first search order
        """
        yield self
        for child in self._children:
            if child is not None:
                yield from child
            else:
                yield None

    def values(self):
        for node in self.nodes():
            if node is not None and node.value is not None:
                yield node.value
            else:
                yield None

    @staticmethod
    def collect_leaves(root_node, leaves=[]):

        """ Collects all leaves of a given tree from the left to the right
        :param root_node:  (``TreeNode``) - the root of a tree
        :param leaves: (``list``) - a list where the leaves will be appended
        :return: the list of leaves
        """
        def recursion(node: TreeNode, all_leaves: list):
            if len(node._children) == 0:
                leaves.append(node)
            else:
                for c in node._children: recursion(c, all_leaves)

        recursion(root_node, leaves)
        return leaves

    def count_nodes(self):

        """Count all the nodes of a given tree
        :param root_node:  (``TreeNode``) - the root of a tree
        :return: the number of nodes
        """

        return len(self.nodes())


class BTreeNode(TreeNode):

    def __init__(self, id, value, left=None, right=None):
        """Creates a binary tree node.

        :param id: (``string``) a string that is used to identify graphical elements of this tree.
        ID of a node will be created as ``ID-idx`` where ``ID`` is the ID of a layout object and ``idx`` is the index of this node
        :param value: value stored at this node
        :param left: (``BTreeNode``) reference to the root of a left subtree
        :param right: (``BTreeNode``) reference to the root of a right subtree
        """
        super().__init__(id, value)
        self._children = [left, right]

        if left: left._parent = self
        if right: right._parent = self

    @property
    def left(self):
        return self._children[0]

    @left.setter
    def left(self, new_left):
        self._children[0] = new_left
        new_left.__parent = self

    @property
    def right(self):
        return self._children[1]

    @right.setter
    def right(self, new_right):
        self._children[1] = new_right
        new_right.__parent = self


class DendrogramNode(TreeNode):

    def __init__(self, id, value, distance, *nodes):
        """Dendrogram node adds a distance value and a group_id properties to a TreeNode class"""
        super().__init__(id, value)
        self.__distance = distance
        self.__group_id = 0             # --- by default all nodes are in the group 0
        for n in nodes: self.append(n)

    @property
    def distance(self):
        return self.__distance

    @property
    def group_id(self):
        return self.__group_id

    @group_id.setter
    def group_id(self, id):
        self.__group_id = id

    @staticmethod
    def group_id_from_leaves(root):
        def recursion(node: DendrogramNode):
            if node.is_leaf(): return node.group_id
            ids = []
            for c in node.children:
                ids.append(recursion(c))
            if len(ids) == ids.count(ids[0]):
                node.group_id = ids[0]

        recursion(root)


class MSADendrogramNode(TreeNode):

    def __init__(self, id, value, distance, sequence, *nodes):
        """A node of a phylogenetic tree: dendrogram node with a sequence"""
        super().__init__(id, value)
        self.__sequence = sequence
        self.__distance = distance
        for n in nodes: self.append(n)

    @property
    def distance(self):
        return self.__distance

    @property
    def sequence(self):
        return self.__sequence

    @sequence.setter
    def sequence(self, sequence):
        self.__sequence = sequence


def clone_tree(node: BTreeNode, n_levels: int):
    """Creates a deep copy of a given tree, that might be limited in depth"""

    if n_levels < 0: return None
    if node:
        if isinstance(node, DendrogramNode):
            copy = DendrogramNode(node.id, node.value, node.distance)
        else:
            copy = BTreeNode(node.id, node.value)
        copy.x = node.x
        copy.y = node.y
        copy.w = node.w
        if node.left:
            l = clone_tree(node.left, n_levels - 1)
            if l: copy.left = l
        if node.right:
            r = clone_tree(node.right, n_levels - 1)
            if r: copy.right = r

        return copy
    return None
