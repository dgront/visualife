import re
from visualife.diagrams import TreeNode, BTreeNode
from typing import List

def subtree_size(root: TreeNode):
    """Calculates size of every subtree of a give tree

    This function uses recursion to compute the size of a subtree rooted at every node of a given tree.
    :param root: (``TreeNode``) - the root of an input tree
    :return:  (``dict``) - a dictionary where an ``id`` of a tree node is a key and the size of the respective subree
    is the value associated with that tree
    """
    sizes = {}

    def recursion(root: TreeNode, sizes_dic: dict):
        s = 0

        for child in root.children_deepcopy:
            s += recursion(child,sizes_dic)
        sizes_dic[root.id] = s + 1
        return s + 1

    recursion(root, sizes)
    return sizes


def balance_binary_tree(root: BTreeNode):
    """Balances a binary tree

    Swaps a left subtree with a right subtree to make a binary tree look nicer
    :param root: (``BTreeNode``) - the root of an input tree
    """
    sizes = subtree_size(root)

    def recursion(root: BTreeNode):
        lsize = sizes[root.left.id] if root.left else 0
        rsize = sizes[root.right.id] if root.right else 0
        plsize = sizes[root.parent.left.id] if root.parent.left else 0
        prsize = sizes[root.parent.right.id] if root.parent.right else 0
        if (lsize > rsize and plsize > prsize) or (lsize < rsize and plsize < prsize):
            root.left, root.right = root.right, root.left

        if root.left: recursion(root.left)
        if root.right: recursion(root.right)

    if root.left: recursion(root.left)
    if root.right: recursion(root.right)


def sort_names_by_count(leaves: list[TreeNode], relevant_names_regex:list[str]):
    """ Collects distinct sub-strings extracted from node values and counts them

    Value of each node is searched with all given regex-es and a hit from the first group
    is extracted into a dictionary to count how often they occur

    :param leaves: leaves of a three that provide values (strings)
    :param relevant_names_regex: a list of RegEx patterns used to search for keys; each pattern
        must provide a capturing group. Note, that a single RegEx pattern may capture several different
        string that are treated as distinct keys in the resulting dictionary, e.g. pattern "(Salinispora_[a-z]+?)_"
        matches both 'Salinispora_arenicola' and 'Salinispora_pacifica' which are counted separately
    :return: a list of key-count tuples, e.g. [('Salinispora_arenicola', 1668), ('Salinispora_pacifica', 759)]
    """

    names = {}

    def sort_key(k): return names[k]

    for l in leaves:
        for key in relevant_names_regex:
            res = re.search(key,l.value)
            if res:
                txt = res.group(1)
                if txt in names: names[txt] += 1
                else: names[txt] = 1

    keys_sorted = sorted(names.keys(),key=sort_key, reverse=True)
    return [(k,names[k]) for k in keys_sorted]


def color_nodes_by_keys(nodes: List[TreeNode], colored_keys, palette, default_color: str = "black"):
    """ Assign a color to a node if its value string contain a key from a list.

    Say, if a ``node.value`` contains ``colored_keys[2]`` substring, it will be colored with
    ``palette[2]``
    :param root: root node, necessary to collect all the nodes of the tree
    :param leaves: leaves to be colored
    :param colored_keys:  substring to search for
    :param palette: palette used to color nodes
    :param default_color: color used for nodes that are not recognized by any of the given RegEx patterns
    :return: a list of colors; each ``node`` should be painted with ``colors[node.id]``
    """
    clrs = [default_color for _ in range(len(nodes))]  # --- Every node is black by default
    for node in nodes:
        if node.is_leaf():
            for i, key_regex in enumerate(colored_keys):
                if re.search(key_regex,node.value):
                    clrs[node.id] = palette[i % len(palette)]
                    break

    return clrs