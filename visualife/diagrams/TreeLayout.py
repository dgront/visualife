from visualife.diagrams import TreeNode, BTreeNode
from visualife.core import make_brighter


class TreeLayout:
    def __init__(self, id, viewport, max_screen_x, max_screen_y, **kwargs):
        """Base class for drawing a tree

        :param id: (``string``) a string that is used to identify graphical elements of this tree.
        ID of a node will be created as ``ID-idx`` where ``ID`` is the ID of this layout object
        and ``idx`` is the index of a node
        :param viewport: (``SvgViewport``) or its derivative
        :param max_screen_x: (``int``) - right screen coordinate
        :param max_screen_y: (``int``) - bottom screen coordinate
        :param kwargs:  - see below

        :Keyword Arguments ``(**kwargs)``:
            * *width* (``number``) --
              width of each node in pixels
            * *height* (``number``) --
              height of each node in pixels
            * *separation* (``number``) --
              minimum separation between nodes in pixels
            * *x_margin* (``number``) --
              white space margin for the diagram on both sides along X axis (in viewport units, e.g. pixels)
            * *y_margin* (``number``) --
              white space margin for the diagram on both sides along Y axis
        """
        self._viewport = viewport
        self.x_min = 0
        self.x_max = max_screen_x
        self.y_min = 0
        self.y_max = max_screen_y
        self.y_margin = kwargs.get("y_margin", 20)
        self.x_margin = kwargs.get("x_margin", 20)
        self.__id = id
        self.sep = kwargs.get("separation", 15)
        self.wx = kwargs.get("width", 20)
        self.wy = kwargs.get("height", 20)
        self.leaves_on_axis = True
        self.scale_edges = True
        self.node_size = 5.0
        self.node_color = "black"
        self.node_stroke_width = 0
        self.node_stroke = "black"
        self.node_shape = "c"
        self.leaf_size = 5.0
        self.leaf_shape = "c"
        self.leaf_color = "black"
        self.leaf_default_color = "black"
        self.leaf_stroke_width = 0
        self.leaf_stroke = "black"
        self.edge_width = 0.1
        self.edge_color = "black"
        self.edge_default_color = "black"
        self._tree_x_min = 1000000.0
        self._tree_x_max = -100000.0
        self._tree_y_min = 1000000.0
        self._tree_y_max = -100000.0
        self.label_color = "black"

    @property
    def id(self):
        """Returns ID string that identifies this drawing"""
        return self.__id

    def draw(self, root_node: TreeNode):
        """Draws a binary tree given by its root node
        :param root_node: (``BTreeNode``) - root of the tree
        """
        raise NotImplemented

    def set_style(self, **kwargs):
        """parses parameters to style this tree
        :param kwargs: see below

        :Keyword Arguments:
            * *leaves_on_axis* (``boolean``) --
                if ``True``, all leaves (singletons) will be placed on X axis;
                otherwise they will be plotted on the Y coordinate accordingly to the clustering hierarchy
            * *scale_edges* (``boolean``) --
                if ``True``, the length of radial edges will be determined proportionally to the distances
                stored in a given phylogenetic trees
            * *node_shape* (``char``) --
                a single character defining the shape of each mid-node:
                ``t`` and ``s`` draw triangles and squares respectively; both ``o`` and ``c`` draw circles
            * *node_color* (``string``, ``list[string]`` or ``callable``) --
                color of a mid-tree node
            * *node_size* (``float``) --
                size of a mid-tree node, e.g. circle radius
            * *node_stroke* (``string``) --
                color of a mid-tree node outline
            * *node_stroke_width* (``float``) --
                width of a mid-tree node outline
            * *leaf_shape* (``char``) --
                a single character defining the shape of each leaf node:
                ``t`` and ``s`` draw triangles and squares respectively; both ``o`` and ``c`` draw circles
            * *leaf_color* (``string``, ``list[string]`` or ``callable``) --
                color of a leaf node (i.e. representing a specie or a sequence)
            * *leaf_size* (``float``) --
                size of a leaf node, e.g. circle radius
            * *leaf_stroke* (``string``) --
                color of leaf node outline
            * *leaf_stroke_width* (``float``) --
                width of leaf node outline
            * *label_color* (``string``, ``list[string]`` or ``callable``) --
                color of a label string, if drawn
        """
        self.leaves_on_axis = kwargs.get("leaves_on_axis", True)
        self.scale_edges = kwargs.get("scale_edges", True)
        self.node_shape = kwargs.get("node_shape", "c")
        self.node_size = kwargs.get("node_size", 5.0)
        self.node_color = kwargs.get("node_color", "black")
        self.node_stroke_width = kwargs.get("node_stroke_width", 0)
        self.node_stroke = kwargs.get("node_stroke", "black")
        self.leaf_shape = kwargs.get("leaf_shape", "c")
        self.leaf_size = kwargs.get("leaf_size", 5.0)
        self.leaf_color = kwargs.get("leaf_color", "black")
        self.leaf_stroke_width = kwargs.get("leaf_stroke_width", 0)
        self.leaf_stroke = kwargs.get("leaf_stroke", "black")
        self.edge_width = kwargs.get("edge_width", 0.1)
        self.edge_color = kwargs.get("edge_color", "black")
        self.label_color = kwargs.get("label_color", "black")

    def get_edge_color(self, node):
        """Returns the color of a line that connects a given node
        The color is defined by ``self.edge_color`` property of this class as follows:
            - ``self.edge_color`` is callable : ``self.edge_color(node.id)`` will be returned
            - ``self.edge_color`` is a list : ``self.edge_color[node.id]`` will be returned
            - ``self.edge_color`` is a ``string`` : ``self.edge_color`` will be returned
        :param node: (``TreeNode``) - a node that defines the line color
        :return: color (``str``)
        """
        if isinstance(self.edge_color, list):
            return self.edge_color[node.id]
        elif callable(self.edge_color):
            return self.edge_color(node.id)
        return self.edge_color

    def _draw_leaf_nodes(self, group_id, nodes, n_nodes_total):
        self.__draw_nodes(group_id, nodes, n_nodes_total, self.leaf_size, self.leaf_shape, self.leaf_color, self.leaf_stroke, self.leaf_stroke_width)

    def _draw_inner_nodes(self, group_id, nodes, n_nodes_total):
        self.__draw_nodes(group_id, nodes, n_nodes_total, self.node_size, self.node_shape, self.node_color, self.node_stroke, self.node_stroke_width)

    def __draw_nodes(self, group_id, leaves, n_nodes_total, size, shapes, colors, stroke, stroke_width):

        if isinstance(colors, str):
            self._viewport.start_group(group_id, stroke_width=stroke_width, stroke=stroke, fill=colors)
        else:
            self._viewport.start_group(group_id, stroke_width=stroke_width)

        # ---------- create a list of colors: a color for each tree leaf
        if callable(colors):
            colors_list = ['black' for _ in range(n_nodes_total)]
            for idx, x, y in leaves: colors_list[idx] = colors(idx)
        elif isinstance(colors, str):
            colors_list = None               # --- mark by None because color is included in properties of the group
        else:
            colors_list = colors

        # ---------- create a list of shapes: a shape for each tree leaf
        if callable(shapes):
            shapes_list = ['c' for _ in range(n_nodes_total)]
            for idx, x, y in leaves: shapes_list[idx] = shapes(idx)
        elif isinstance(shapes, str):
            shapes_list = [shapes for _ in range(n_nodes_total)]
        else:
            shapes_list = shapes
        if colors_list is not None:
            for idx, x, y in leaves:
                id_str = self.id + "-" + str(idx)
                strk = make_brighter(colors_list[idx], 0.6) if stroke == "brighter" else stroke
                if shapes_list[idx] == "o":
                    self._viewport.circle(id_str, x, y, size, fill=colors_list[idx], stroke=strk)
                if shapes_list[idx] == "c":
                    self._viewport.circle(id_str, x, y, size, fill=colors_list[idx], stroke=strk)
                if shapes_list[idx] == "t":
                    self._viewport.triangle(id_str, x, y, size, fill=colors_list[idx], stroke=strk)
                if shapes_list[idx] == "s":
                    self._viewport.square(id_str, x, y, size, fill=colors_list[idx], stroke=strk)
        else:
            for idx, x, y in leaves:
                id_str = self.id + "-" + str(idx)
                if shapes_list[idx] == "o":
                    self._viewport.circle(id_str, x, y, size)
                if shapes_list[idx] == "c":
                    self._viewport.circle(id_str, x, y, size)
                if shapes_list[idx] == "t":
                    self._viewport.triangle(id_str, x, y, size)
                if shapes_list[idx] == "s":
                    self._viewport.square(id_str, x, y, size)
        self._viewport.close_group()


def propagate_colors(root_node: TreeNode, leaf_colors: list, mixed_color = "black"):
    """ Copies color assignment from leaves to inner nodes.
    An inner node will be coloured with the same paint as its nodes if they are of the same color
    :param root_node: - root of recursion
    :param leaf_colors: - list to store the colors
    :param mixed_color: - color that will be used to paint nodes that can't be painet by leaves
    :return: ``None``
    """
    coloring = {}

    def color_by_recursion(node: TreeNode):
        nonlocal coloring
        if node.is_leaf():
            coloring[node.id] = leaf_colors[node.id]
            return leaf_colors[node.id]
        else:
            colors = []
            for n in node.children:
                cr = color_by_recursion(n)
                colors.append(cr)
                coloring[n.id] = cr
            for c in colors[1:]:
                if c != colors[0]: break
            else:
                return c
            return mixed_color

    color_by_recursion(root_node)
    for i, c in coloring.items():
        leaf_colors[i] = c

