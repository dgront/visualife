from visualife.diagrams import TreeNode, TreeLayout


class TriangularLayout(TreeLayout):
    def __init__(self, id, viewport, min_screen_x, max_screen_x, min_screen_y, max_screen_y, **kwargs):
        """Draws a binary tree using a triangular layout

        :param id: (``string``) a string that is used to identify graphical elements of this tree.
        ID of a node will be created as ``ID-idx`` where ``ID`` is the ID of this layout object
        and ``idx`` is the index of a node
        :param viewport: (``SvgViewport``) its derivative
        :param min_screen_x: (``int``) - left screen coordinate
        :param max_screen_x: (``int``) - right screen coordinate
        :param min_screen_y: (``int``) - top screen coordinate
        :param max_screen_y: (``int``) - bottom screen coordinate
        :param kwargs:  - see below

        :Keyword Arguments ``(**kwargs)``:
            * *width* (``number``) --
              width of each node in pixels
            * *height* (``number``) --
              height of each node in pixels
            * *separation* (``number``) --
              minimum separation between nodes in pixels
        """
        super().__init__(id, viewport, min_screen_x, max_screen_x, min_screen_y, max_screen_y, **kwargs)

    def draw(self, root_node: TreeNode):
        """Draws a binary tree given by its root node
        :param root_node: (``BTreeNode``) - root of the tree
        """
        self.__set_width(root_node, 0)
        root_node.x = 0
        root_node.y = 0
        self.__set_location(root_node)

        self.__shift_all(root_node)

        wx = (self.x_max - self.x_min) / (root_node.left.w + root_node.right.w) * root_node.left.w
        for node in root_node:
            if node==None : continue
            nx = node.x + wx + self.x_margin
            ny = node.y+self.y_margin
            id_str = self.id + "-" + str(node.id)
            if node.left:
                lx = node.left.x + wx + self.x_margin
                ly = node.left.y + self.y_margin
                self._viewport.line(id_str + "-" + str(node.left.id), nx, ny, lx, ly)
            if node.right:
                rx = node.right.x + wx + self.x_margin
                ry = node.right.y + self.y_margin
                self._viewport.line(id_str + "-" + str(node.right.id), nx, ny, rx, ry)
            self._viewport.circle(id_str, nx, ny, 2)

        self._viewport.close()

    def __set_width(self, root: TreeNode, level):

        for c in root.children:
            if c is not None: self.__set_width(c, level + 1)

        if len(root.children) == 0:
            root.w = self.wx
        elif len(root.children) == 1:
            root.w = root.children[0].w
        else:
            root.w = -self.sep
            for c in root.children:
                if c is not None:
                    root.w += c.w + self.sep

    def __set_location(self, root: TreeNode):

        if root.left:
            root.left.y = root.y + self.wy
            if root.left.right:
                root.left.x = root.x - root.left.right.w - self.sep / 2
            else:
                root.left.x = root.x - root.left.w / 2 - self.sep / 2
            self.__set_location(root.left)
        if root.right:
            root.right.y = root.y + self.wy
            if root.right.left:
                root.right.x = root.x + root.right.left.w - self.sep / 2
            else:
                root.right.x = root.x + root.right.w / 2 + self.sep / 2
            self.__set_location(root.right)

    def __lr_shifts(self, root: TreeNode):
        """Gets a node and returns two dicts of x coordinates of left and rigth subtrees nodes.
        NOTE: nodes of the input tree must already have x, y coordinates assigned!
        """
        l = {}
        r = {}

        def recursion(root: TreeNode, l:dict, r:dict):
            l[root.id] = [root.x]
            r[root.id] = [root.x]
            if root.left:
                recursion(root.left, l, r)
                tmp = l[root.id]
                tmp.extend(l[root.left.id])
                l[root.id] = tmp
            if root.right:
                recursion(root.right, l, r)
                tmp = r[root.id]
                tmp.extend(r[root.right.id])
                r[root.id] = tmp
        recursion(root, l, r)

        return l,r

    def __shift_one(self,root: TreeNode, l, r):
        """Takes a node and shifts subtrees coordinates to look good while beeing drawn 
        """
        if root==None: return False
        if root.left and root.right:
            leftmost_allowed = root.x + self.sep / 2  # ---------- the leftmost pos a right subtree can be
            rightmost_allowed = root.x - self.sep / 2  # ---------- the rightmost pos a left subtree can be
            if root.left :
                left_can_move_by = leftmost_allowed - root.left.x

            if root.right:
                right_can_move_by = rightmost_allowed-root.right.x

            print("Shifting right subtree by", right_can_move_by)
            for nr in root.right:
                if nr!=None:
                    nr.x += right_can_move_by
            print("Shifting left subtree by", left_can_move_by)
            for nr in root.left: 
                if nr!=None:
                    nr.x += left_can_move_by

        return False

    def __shift_all(self, root):
        """Goes through every node in a loop and shifts subtrees coordinates to look good while beeing drawn 
        """
        l,r = self.__lr_shifts(root)
        if_shifted = True
        while if_shifted:
            for n in root:
                print(n)
                if_shifted = self.__shift_one(n,l,r)
            if if_shifted:
                print("yes")
                l, r = self.__lr_shifts(root)

