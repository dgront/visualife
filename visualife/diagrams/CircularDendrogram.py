from math import pi, sin, cos,acos
from visualife.diagrams import TreeNode, DendrogramLayout
from visualife.core.shapes import circle_segment
from visualife.core import AxisX


class CircularDendrogram(DendrogramLayout):

    def __init__(self, id, viewport, max_screen_x, max_screen_y, **kwargs):
        """Draws a binary tree using a circular dendrogram layout

        Circular dendrogram layout is suitable for drawing clustering results, phylogenetic trees etc.

        :param id: (``string``) a string that is used to identify graphical elements of this tree.
        ID of a node will be created as ``ID-idx`` where ``ID`` is the ID of this layout object
        and ``idx`` is the index of a node
        :param viewport: (``SvgViewport``) its derivative
        :param max_screen_x: (``int``) - right screen coordinate
        :param max_screen_y: (``int``) - bottom screen coordinate
        :param kwargs: see below

        :Keyword Arguments ``(**kwargs)``:
            * *width* (``number``) --
              width of each node in pixels
            * *height* (``number``) --
              height of each node in pixels
            * *separation* (``number``) --
              minimum separation between nodes in pixels
            * *r_max* (``number``) --
              maximum radius of the diagram excluding annotations such as labels
        """
        super().__init__(id, viewport, max_screen_x, max_screen_y, **kwargs)
        self.__px = 1
        self.__rotation = 0                         # --- both rotation and arc values are in radians!
        self.__arc = 350.0 / 180 * pi
        self.__r_max = kwargs.get("r_max", None)    # --- maximum radius of the diagram
        self.__cx = 1                               # --- X, Y of the diagram circle's center
        self.__cy = 1

    def draw(self, root_node: TreeNode, **kwargs):
        """Draws a binary tree given by its root node
        :param root_node: (``BTreeNode``) - root of the tree
        :param kwargs: see arguments parsed TreeLayout.set_style();
            this method defines also two additional keywords described below

        :Keyword Arguments:
            * *rotation* (``float``) --
                rotate the whole plot by an angle given in degrees
            * *arc* (``float``) --
                the angle of the plot; 360 makes the full circle
        """

        self.set_style(**kwargs)
        self.scale_edges = True             # --- should be turned on for a circular layout
        self.__rotation = kwargs.get("rotation", 0) / 180.0 * pi
        self.__arc = kwargs.get("arc", 350) / 180.0 * pi
        dx = self.x_max - self.x_min - 2 * self.x_margin
        dy = self.y_max - self.y_min - 2 * self.y_margin

        self.__cx = dx/2 + self.x_margin
        self.__cy = dy/2 + self.y_margin
        cx, cy = self.__cx, self.__cy       # --- optimization for speed
        if self.__r_max is None:
            self.__r_max = min(dx, dy) / 2
        self.wx, self.wy = 0, 1
        self.sep = 1
        self._set_location(root_node, self.leaves_on_axis, self.scale_edges)

        all_nodes = root_node.nodes()

        node_centers = []
        leaf_centers = []
        group_line_color = self.edge_color if isinstance(self.edge_color, str) else self.edge_default_color
        self._viewport.start_group(self.id+"+edges", stroke_width=self.edge_width, stroke=group_line_color)
        self.__px = self.__arc / (self._tree_x_max - self._tree_x_min)
        for node in all_nodes:
            r, a = self.polar_position(node)
            children = node.children
            if len(children) == 0:
                leaf_centers.append((node.id, r * cos(a) + cx, r * sin(a) + cy))
            else:
                left = children[0]
                right = children[-1]
                la = (left.x - self._tree_x_min) * self.__px + self.__rotation
                ra = (right.x - self._tree_x_min) * self.__px + self.__rotation
                circle_segment(self._viewport, self.id + "-" + str(node.id) + "-" + str(node.id) + "-v", cx, cy, r, r, la / pi * 180,
                                   ra / pi * 180, stoke=self.get_edge_color(node))

                for child in children:
                    nr, na = self.polar_position(child)
                    self.__line_in_radial(self.id + "-" + str(node.id) + "-" + str(child.id) + "-h", nr, na, r, na, cx, cy, self.get_edge_color(child))

                node_centers.append((node.id, r * cos(a) + cx, r * sin(a) + cy))
        self._viewport.close_group()

        self._draw_inner_nodes(self.id+":nodes", node_centers, root_node.count_nodes())
        self._draw_leaf_nodes(self.id+":leaves", leaf_centers, root_node.count_nodes())
        if kwargs.get("draw_labels",False):
            self._draw_labels(self.id+":labels", root_node, leaf_centers, kwargs.get("label_outside", True),
                              **kwargs)

    def draw_axis(self):
        a = (self.__rotation - (2 * pi - self.__arc) / 2.0) * 180 / pi
        axis = AxisX(self.__cy, self.__cx, self.__cx + self.__r_max, 0, self._tree_y_max, 'B')
        axis.tics_at_values([0.0, 0.2, 0.4], "%.1f")
        self._viewport.start_group(self.id+":axis-group", transform="rotate(%f, %f, %f)" % (a, self.__cx, self.__cy))
        axis.draw(self._viewport)
        self._viewport.close_group()

    @property
    def arc(self):
        """angle (from 0 to 360deg) the diagram takes.
        The property is defined by kwargs sent to the ``draw()`` method
        :return: angle of the diagram (in degrees)
        """
        return self.__arc * 180 / pi

    @property
    def cx(self):
        """X coordinate of the diagram's center in the viewport units
        The property is computed by the ``draw()`` method
        :return: X viewport coordinate
        """
        return self.__cx

    @property
    def cy(self):
        """Y coordinate of the diagram's center in the viewport units
        The property is computed by the ``draw()`` method
        :return: Y viewport coordinate
        """
        return self.__cy

    @property
    def r_max(self):
        """radius of the circular dendrogram, i.e. the maximum distance of its leaves from its center
        :return:  radius of the circular dendrogram
        """
        return self.__r_max

    def polar_position(self, node):
        """Provides polar coordinates for a given node.

        This method is used to place diagram's nodes; is takes *arc* and *rotation* parameters into account
        :param node: (``TreeNode``) - diagram's node
        :return: (``tuple(float, float)``) - radius and angle values
        """
        a = (node.x - self._tree_x_min) * self.__px + self.__rotation
        r = node.y * self.__r_max
        return r, a

    def cartesian_position(self, node):
        """Provides Cartesian coordinates for a given node.

        This method is used to place diagram's nodes; is takes *arc* and *rotation* parameters into account
        :param node: (``TreeNode``) - diagram's node
        :return: (``tuple(float, float)``) - X and Y values in  viewport coordinates
        """
        r, a = self.polar_position(node)

        return r * cos(a) + self.__cx, r * sin(a) + self.__cy

    def __line_in_radial(self, id, ri, ai, rj, aj, cx, cy, color):
        xi = ri * cos(ai) + cx
        xj = rj * cos(aj) + cx
        yi = ri * sin(ai) + cy
        yj = rj * sin(aj) + cy
        self._viewport.line(id, xi, yi, xj, yj, stroke=color)

    def _draw_labels(self,id,root_node,centers,label_outside, **kwargs):
        nodes = root_node.nodes()
        anchor2 = "end"
        anchor1 = "start"

        r_offset = kwargs.get("label_r_offset", 0)

        # ---------- All sequence names (labels) are placed in a single group
        self._viewport.start_group(id, Class="", **kwargs)

        for node in centers:
            l = nodes[node[0]].value
            r, a = self.polar_position(nodes[node[0]])
            x = (r_offset+r) * cos(a) + self.__cx
            y = (r_offset+r) * sin(a) + self.__cy
            fc = cos(a)
            fs = sin(a)
            if label_outside:
                anchor1="end"
                anchor2="start"
            if -0.00000000001<=fc< 0.000002: fc=0
            if -0.00000000001<=fs< 0.000002: fs = 0
            clr = self.label_color
            if callable(self.label_color):
                clr = self.label_color(node[0])
            elif isinstance(clr,list):
                clr = self.label_color[node[0]]
            if fc>=0 and fs>=0: # IV quater
                self._viewport.text(node[0], x, y, l, text_anchor=anchor2, angle=acos(fc)*180/pi, Class="", fill=clr)
            elif fc>=0 and fs<0: # I quater
                self._viewport.text(node[0], x, y, l, text_anchor=anchor2, angle=-acos(fc)*180/pi, Class="", fill=clr)
            elif fc<0 and fs>=0: # III quater
                self._viewport.text(node[0], x, y, l, text_anchor=anchor1,
                                    angle=acos(fc) * 180 / pi +180, Class="", fill=clr)
            elif fc<0 and fs<0: # II quater
                self._viewport.text(node[0], x, y, l, text_anchor=anchor1, angle=-acos(fc)*180/pi+180, Class="", fill=clr)
        self._viewport.close_group()




