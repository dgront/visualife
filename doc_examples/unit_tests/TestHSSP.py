import unittest, io, re
from visualife.data import HSSP


class TestHSSP(unittest.TestCase):

    def test_hssp_loading(self):
        hssp = HSSP("../inputs/1crn.hssp")
        self.assertEqual(46, hssp.seq_length)
        self.assertEqual(176, hssp.seq_aligned)


if __name__ == '__main__':
    unittest.main()
