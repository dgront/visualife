import unittest, io, re
from visualife.data import ScoreFile


data_fsc_1 = """SEQUENCE: 
SCORE: total_score         rms        Fnat        I_sc        Irms dslf_ca_dih dslf_cs_ang dslf_ss_dih dslf_ss_dst      fa_atr      fa_dun     fa_elec     fa_pair      fa_rep      fa_sol hbond_bb_sc hbond_lr_bb    hbond_sc hbond_sr_bb description 
SCORE:    -283.024       4.789       0.343      -2.717       2.534       1.335       1.635      18.973      -6.452    -612.922      26.503      -5.977      -8.795      96.770     256.214      -8.123     -27.806      -8.631      -5.749 proteins_0151
SCORE:    -282.071       4.518       0.329      -3.956       2.426       1.335       1.635      18.973      -6.452    -614.994      26.643      -5.970      -9.218      98.173     257.911      -7.843     -27.738      -8.786      -5.740 proteins_0116
SCORE:    -283.319       4.122       0.443      -2.797       1.964       1.335       1.635      18.973      -6.452    -616.944      26.589      -5.986      -9.314      97.771     258.919      -7.993     -27.786      -8.341      -5.725 proteins_0114
SCORE:    -283.635       5.704       0.229      -2.625       3.106       1.335       1.635      18.973      -6.452    -614.349      25.941      -5.898      -9.471      97.099     257.109      -7.395     -27.705      -8.654      -5.802 proteins_0005"""

out_1 = """SCORE: total_score rms Fnat I_sc Irms dslf_ca_dih dslf_cs_ang dslf_ss_dih dslf_ss_dst fa_atr fa_dun fa_elec fa_pair fa_rep fa_sol hbond_bb_sc hbond_lr_bb hbond_sc hbond_sr_bb description
SCORE: -283.02 4.79 0.34 -2.72 2.53 1.33 1.64 18.97 -6.45 -612.92 26.50 -5.98 -8.79 96.77 256.21 -8.12 -27.81 -8.63 -5.75 proteins_0151
SCORE: -282.07 4.52 0.33 -3.96 2.43 1.33 1.64 18.97 -6.45 -614.99 26.64 -5.97 -9.22 98.17 257.91 -7.84 -27.74 -8.79 -5.74 proteins_0116
SCORE: -283.32 4.12 0.44 -2.80 1.96 1.33 1.64 18.97 -6.45 -616.94 26.59 -5.99 -9.31 97.77 258.92 -7.99 -27.79 -8.34 -5.72 proteins_0114
SCORE: -283.63 5.70 0.23 -2.62 3.11 1.33 1.64 18.97 -6.45 -614.35 25.94 -5.90 -9.47 97.10 257.11 -7.39 -27.70 -8.65 -5.80 proteins_0005
"""

data_fsc_2 = """SCORE: total_score         rms        Fnat        I_sc        Irms dslf_ca_dih dslf_cs_ang dslf_ss_dih dslf_ss_dst      fa_atr      fa_dun     fa_elec     fa_pair      fa_rep      fa_sol hbond_bb_sc hbond_lr_bb    hbond_sc hbond_sr_bb description 
SCORE:    -283.024       4.789       0.343      -2.717       2.534       1.335       1.635      18.973      -6.452    -612.922      26.503      -5.977      -8.795      96.770     256.214      -8.123     -27.806      -8.631      -5.749 proteins_0151
SCORE:    -282.071       4.518       0.329      -3.956       2.426       1.335       1.635      18.973      -6.452    -614.994      26.643      -5.970      -9.218      98.173     257.911      -7.843     -27.738      -8.786      -5.740 proteins_0116
SCORE:    -283.319       4.122       0.443      -2.797       1.964       1.335       1.635      18.973      -6.452    -616.944      26.589      -5.986      -9.314      97.771     258.919      -7.993     -27.786      -8.341      -5.725 proteins_0114
SCORE:    -283.635       5.704       0.229      -2.625       3.106       1.335       1.635      18.973      -6.452    -614.349      25.941      -5.898      -9.471      97.099     257.109      -7.395     -27.705      -8.654      -5.802 proteins_0005"""

out_2 = """SEQUENCE: 
SCORE: total_score rms Fnat I_sc Irms dslf_ca_dih dslf_cs_ang dslf_ss_dih dslf_ss_dst fa_atr fa_dun fa_elec fa_pair fa_rep fa_sol hbond_bb_sc hbond_lr_bb hbond_sc hbond_sr_bb description
SCORE: -283.02 4.79 0.34 -2.72 2.53 1.33 1.64 18.97 -6.45 -612.92 26.50 -5.98 -8.79 96.77 256.21 -8.12 -27.81 -8.63 -5.75 proteins_0151
SCORE: -282.07 4.52 0.33 -3.96 2.43 1.33 1.64 18.97 -6.45 -614.99 26.64 -5.97 -9.22 98.17 257.91 -7.84 -27.74 -8.79 -5.74 proteins_0116
SCORE: -283.32 4.12 0.44 -2.80 1.96 1.33 1.64 18.97 -6.45 -616.94 26.59 -5.99 -9.31 97.77 258.92 -7.99 -27.79 -8.34 -5.72 proteins_0114
SCORE: -283.63 5.70 0.23 -2.62 3.11 1.33 1.64 18.97 -6.45 -614.35 25.94 -5.90 -9.47 97.10 257.11 -7.39 -27.70 -8.65 -5.80 proteins_0005
"""


class TestScoreFile(unittest.TestCase):

    def test_silent_parsing(self):
        self.__try_silent(data_fsc_1, out_1)
        self.__try_silent(data_fsc_2, out_1)

    def __try_silent(self,input, output):
        reader = ScoreFile()
        reader.read_score_file(input)
        file = io.StringIO()
        reader.write_score_file(file)
        out = re.sub(" +", " ", file.getvalue())
        self.assertEqual(out, output)


if __name__ == '__main__':
    unittest.main()
