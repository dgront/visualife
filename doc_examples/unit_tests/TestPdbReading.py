import unittest
from visualife.data import parse_pdb_data, secondary_structure_string, create_sequence, secondary_structure_residues


class TestPdbReading(unittest.TestCase):

    def __init__(self, pdb_file, chain_codes, n_residues, sequence, secondary, method_name):
        super().__init__(method_name)
        self.__chain_codes = chain_codes
        self.__n_residues = n_residues
        self.__sequence = sequence
        self.__secondary = secondary

        with open(pdb_file, 'r') as content_file:
            self.__content = content_file.read()
            self.__structures = parse_pdb_data(self.__content)

    def test_ss_residues(self):
        ss = secondary_structure_residues(self.__content, self.__structures[0].chains[0].residues)
        expected = ["THR 2", "GLY 14", "ASP 22", "GLU 42", "THR 51"]
        for i in range(5):
            self.assertEqual(str(ss[i][1][0]), expected[i])

    def test_chains(self):
        self.assertEqual(len(self.__structures[0].chains), len(self.__chain_codes))
        for i in range(len(self.__structures[0].chains)):
            self.assertEqual(self.__structures[0].chains[i].chain_id, self.__chain_codes[i])
            seq = create_sequence(self.__structures[0].chains[i].residues)
            self.assertEqual(seq, self.__sequence[i])
            sec_str = secondary_structure_string(self.__content, self.__structures[0].chains[i].residues)
            self.assertEqual(sec_str, self.__secondary[i])


if __name__ == "__main__":

    fname = "../inputs/2gb1.pdb"

    suite = unittest.TestSuite()
    suite.addTest(TestPdbReading(fname, "A", [56], ["MTYKLILNGKTLKGETTTEAVDAATAEKVFKQYANDNGVDGEWTYDDATKTFTVTE"],
                                 ["CEEEEEECCCCCCEEEEEECCHHHHHHHHHHHHHHHCCCCCEEEEECCCCEEEEEC"], "test_chains"))
    suite.addTest(TestPdbReading(fname, "A", [56], ["MTYKLILNGKTLKGETTTEAVDAATAEKVFKQYANDNGVDGEWTYDDATKTFTVTE"],
                                 ["CEEEEEECCCCCCEEEEEECCHHHHHHHHHHHHHHHCCCCCEEEEECCCCEEEEEC"], "test_ss_residues"))

    unittest.TextTestRunner().run(suite)
