import unittest, io, re
from visualife.utils import consecutive_find, detect_blocks


class TestTextUtils(unittest.TestCase):

    def test_consecutive_find(self):
        blocks = consecutive_find("AAABBCCCAAA")
        self.assertEqual(blocks, [[0, 2, 'A'], [3, 4, 'B'], [5, 7, 'C'], [8, 10, 'A']])

    def test_detect_blocks(self):
        blocks = detect_blocks("CEEEEEECCCCCCEEEEEECCHHHHHHHHHHHHHHHCCCCCEEEEECCCCEEEEEC")
        self.assertEqual(blocks, {'H': [[21, 35]], 'E': [[1, 6], [13, 18], [41, 45], [50, 54]], 'C': [[7, 12], [19, 20], [36, 40], [46, 49]]})


if __name__ == '__main__':
    unittest.main()
