import unittest
from random import normalvariate, seed
from visualife.calc import Histogram


class TestHistogram(unittest.TestCase):

    N = 1000

    def test_normal_2D(self):
        seed(10)

        hist = Histogram(range=(-3,3), width=0.1)
        for _ in range(self.N):
            hist.observe(normalvariate(0, 1.0))
        self.assertEqual((28, 50), hist.highest_bin())
        self.assertEqual(3, hist.outliers)


if __name__ == '__main__':
    unittest.main()
