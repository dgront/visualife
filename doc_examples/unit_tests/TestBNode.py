import unittest
from visualife.diagrams import BTreeNode, subtree_size, balance_binary_tree


class TestBTreeNode(unittest.TestCase):

    def __create_nice_tree(self):
        rl = BTreeNode(4, None, BTreeNode(0, 'A'), BTreeNode(1, 'B'))
        rr = BTreeNode(5, None, BTreeNode(2, 'C'), BTreeNode(3, 'D'))
        return BTreeNode(6, None, rl, rr)

    def __create_unbalanced_tree(self, n_nodes):
        nid = 0
        root = BTreeNode(nid, nid)
        for i in range(n_nodes - 1):
            nid += 1
            n = BTreeNode(nid, nid)
            nid += 1
            root = BTreeNode(nid, None, n, root)

        return root

    def test_binary_tree(self):
        rl = BTreeNode(5, None, BTreeNode(0, 'A'), BTreeNode(1, 'B'))
        rr = BTreeNode(5, None, BTreeNode(2, 'C'), BTreeNode(3, 'D'))
        root = BTreeNode(6, None, rl, rr)

        self.assertEqual(rl.left.parent, rl)
        self.assertEqual(rl.left.id, 0)
        self.assertEqual(rl.right.id, 1)
        self.assertEqual(rl.right.parent, rl)

    def test_dfs(self):
        root = self.__create_nice_tree()

        nodes = [n for n in root if n]
        self.assertEqual(7, len(nodes))

    def test_subtree_size(self):
        root = self.__create_nice_tree()
        sizes = subtree_size(root)
        self.assertEqual({0: 1, 1: 1, 2: 1, 3: 1, 4: 3, 5: 3, 6: 7}, sizes)
        print(sizes)

    def test_tree_balancing(self):

        root = self.__create_unbalanced_tree(10)
        for node in root: print(node)
        balance_binary_tree(root)
        for node in root: print(node)

if __name__ == '__main__':
    unittest.main()
