from random import normalvariate, seed
import unittest
from visualife.calc import Histogram2D, DataBins2D


class TestHistogram2D(unittest.TestCase):

    N = 1000

    def test_normal_2D(self):
        seed(10)
        h = Histogram2D(range=(-7, 7), width=0.3)
        for _ in range(self.N):
            h.observe(normalvariate(0.0, 1.0), normalvariate(0.0, 1.0))
        c1, c2 = h.get_bin_coordinates((0.1, 0.21)), h.get_bin_coordinates((0.31, 0.65))
        self.assertEqual(0, c1[0])
        self.assertEqual(0, c1[1])
        self.assertEqual(0.3, c2[0])
        self.assertEqual(0.6, c2[1])
        self.assertEqual(8, h.get_bin_by_point((0.31, 0.91)))

    def test_histogram_from_data(self):
        seed(10)
        h = Histogram2D(range=(-7, 7), width=0.3)
        data = [[10, normalvariate(0.0, 1.0), normalvariate(0.0, 1.0)] for _ in range(self.N)]
        h.observe(data, i_column=1, j_column=2)
        c1, c2 = h.get_bin_coordinates((0.1, 0.21)), h.get_bin_coordinates((0.31, 0.65))
        self.assertEqual(0, c1[0])
        self.assertEqual(0, c1[1])
        self.assertEqual(0.3, c2[0])
        self.assertEqual(0.6, c2[1])
        self.assertEqual(8, h.get_bin_by_point((0.31, 0.91)))

    def test_DataBins(self):

        seed(10)

        db = DataBins2D(range=(-7, 7), width=0.3)
        data = [[1000, normalvariate(0.0, 1.0), normalvariate(0.0, 1.0)] for _ in range(self.N)]
        db.observe(data, i_column=1, j_column=2)
        bin = db.get_bin_coordinates((0.1, -0.1))
        obs = db.get_observations(bin)
        self.assertEqual(19, len(obs))
        for o in obs:
            self.assertEqual(1000, o[0])
            self.assertEqual(3, len(o))


if __name__ == '__main__':
    unittest.main()