import sys
from browser import window, ajax, document, timer, run_script as python_runner


widget_code_tb = """

  <style>
  .terminal {
    color: white;
    font: 1.0rem Inconsolata, monospace;
    background-color: black;
    text-shadow: 0 0 5px #C8C8C8;
    width: 100%%;
    margin: 5px;
    background: repeating-linear-gradient(
      0deg,
      rgba(black, 0.15),
      rgba(black, 0.15) 1px,
      transparent 1px,
      transparent 2px
    );
  }
  .script-button {
    margin-bottom: 10px;
    margin-top: 10px;
    height: 30px;
    width: 150px;
  }
  </style>
  
  <div class="script-title" id="title-%s"></div>
  <div class="script-container" id="script-container-%s">
    <div id="script-%s" class="script-editor-long"></div>
    <div style="display: flex; flex-direction: row;">
      <div id="buttons" style="display: flex; flex-direction: column; width: 150px;">
        <button class="script-button" id="run-%s" type="button">Run script</button>
        <button class="script-button" id="reset-%s" type="button">Reset script</button>
      </div>
      <pre id="logs-pre-%s" class="terminal"></pre>
    </div>
    <div id="result-%s" class="script-result-long">
      <svg  id="svg" xmlns="http://www.w3.org/2000/svg" class="right" width="900" height="700"></svg>
    </div>
  </div>
"""


class ScriptStderr:
  def __init__(self, console_pre_id):
    self.__console_pre_id = console_pre_id

  def write(self, err):
    document[self.__console_pre_id].style.color = "red"
    document[self.__console_pre_id].innerHTML = err


class VLScriptWidget:

  def __init__(self, script_name, main_div_id, **params):
    """ Creates a widget in a given DIV
    @param params : 
      - height: integer in pixels
      - read_only: False (default) or True; if True, user won't be able to edit the script
      - hide_buttons: False (default) or True; if True, user won't be able to run the script
      - console_height: integer in pixels - height of the console panel
    """
    m = main_div_id
    document[main_div_id].innerHTML = widget_code_tb % (m,m,m,m,m,m,m)
    self.script_name = script_name
    self.script_div_id = "script-%s" %(main_div_id)
    self.console_pre_id = "logs-pre-%s" %(main_div_id)
    self.script_path = "../_static/"
    self.main_div_id = main_div_id
    self.get_script(None)
    document["run-%s" % (main_div_id)].bind("click",self.run_script)
    document["reset-%s" % (main_div_id)].bind("click",self.get_script)

    # Set height of the editor's window
    self.editor = window.ace.edit(self.script_div_id)
    if "height" in params:
      h = "%dpx" % (params["height"])
    else:
      h = "200px"
    self.editor.container.style.height = h
    self.editor.setReadOnly( params.get("read_only", False) )

    document[self.script_div_id].style.height = h

  def write(self, str):
   def set_svg():
     document[self.console_pre_id] <= str
   timer.set_timeout(set_svg, 10)


  def run_script(self,event):
    editor = window.ace.edit(self.script_div_id)
    sys.stdout = self
    sys.stderr = ScriptStderr(self.console_pre_id)
    python_runner(editor.getValue())
    document[self.console_pre_id] <= "OK\n"

  def get_script_callback(self, request):
    editor = window.ace.edit(self.script_div_id)
    editor.setValue(request.text, -1)
    editor.setTheme("ace/theme/solarized_light")
    editor.getSession().setMode("ace/mode/python")

  def get_script(self,event):
    req = ajax.ajax()
    req.open('GET', self.script_path+self.script_name, True)
    req.set_header('content-type', "application/x-www-form-urlencoded;charset=UTF-8")
    req.bind('complete', self.get_script_callback)
    req.send()
