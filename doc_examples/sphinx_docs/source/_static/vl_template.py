from browser import document
from visualife.core import HtmlViewport

drawing = HtmlViewport(document['svg'], 0, 0, 500, 500,"white")

for i in range(16):
    for j in range(16):
        circle_id = "c-%d-%d" % (i, j)
        drawing.circle(circle_id, i * 400 / 16.0 + 50, j * 400 / 16.0 + 50, 10, fill="pink")
        	
drawing.close()

