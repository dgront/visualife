from browser import document
from visualife.core.Plot import Plot
from visualife.core.HtmlViewport import HtmlViewport
from visualife.calc.math_utils import linspace
from math import pi, sin, cos

# ---------- The drawing area is 900 x 700 pixels, 'svg' is the ID string of enclosing page element
drawing = HtmlViewport(document['svg'],  900, 700)
# ---------- The plot is 800 x 600 pixels
pl = Plot(drawing, 100, 800, 100, 600, 0.0, 2 * pi, -1.0, 1.0, axes_definition="LBRU")
for l in ["B", "U", "L", "R"]:
    pl.axes[l].fill = "black"
    pl.axes[l].stroke_width = 3.0
pl.draw_axes()
pl.draw_grid()
pl.draw_plot_label()

x = linspace(0, 2*pi, num=100)
ys = [sin(ix) for ix in x]
yc = [cos(ix) for ix in x]
pl.line(x, ys, adjust_range=False)
pl.line(x, yc, adjust_range=False)
pl.draw(grid=True)
