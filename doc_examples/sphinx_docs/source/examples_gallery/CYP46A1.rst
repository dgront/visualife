Sequence and structure of CYP46A1
===================================

CYP46A1  is a member of the cytochrome P450 (CYP) superfamily of enzymes that catalyzes the conversion of
cholesterol to 24S-hydroxycholesterol. This page shows the sequence, secondary structure and three-dimensional
structure of that protein based on `2Q9F​ <https://www.rcsb.org/structure/2Q9F>`__ PDB deposit.

This page demonstrates actually four widgets, that are available from VisuaLife library:

 - SequenceViewer displays an amino acid sequence and its annotations, such as heame and cholesterol binding sites
   as well as a region for allosteric regulation by efavirenz
 - SecondaryStructure viewer shows the secondary structure of that protein (as defined in the PDB file header)
 - GLViewerWidget displays the 3D structure of that protein
 - AjaxSender widget - not visible on the page - is used for client-server communication


Allosteric regulation region according to:
K Anderson *et al.* *Mapping of the Allosteric Site in Cholesterol Hydroxylase CYP46A1 for Efavirenz, a Drug That Stimulates Enzyme Activity*
J Biol Chem **2016** May 27:11876-86. doi: `10.1074/jbc.M116.723577 <https://doi.org/10.1074/jbc.m116.723577>`__

.. raw:: html
   :file: CYP46A1.html

|

.. raw:: html

  <div class="highlight-python notranslate">
    <div class="highlight" id="show_script">
    </div>
  </div>
  <script type="text/python">
    import re
    from browser import document, window
    from visualife.utils.text_utils import fix_python_code

    for s in document.select("script"):
        s = s.innerHTML
        if re.search("from .* import ",s[0:100],re.MULTILINE):
            s = fix_python_code(s)
            document["show_script"].innerHTML = '<pre style="font-size: 10px;">'+s+'</pre>'
            break
  </script>
