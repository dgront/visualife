Fan tessalation pattern
=======================

A nice pattern by Johan Karlsson, reimplemented in Python with VisuaLife! (`original graphics <https://codepen.io/DonKarlssonSan/pen/oNLymjJ>`__ published on MIT license).



.. raw:: html
   :file: fan_tessalation.html

      

.. raw:: html

  <div class="highlight-python notranslate">
    <div class="highlight" id="show_script">
    </div>
  </div>
  <script type="text/python">
    import re
    from browser import document, window
    from visualife.utils.text_utils import fix_python_code

    for s in document.select("script"):
        s = s.innerHTML
        if re.search("from browser import ",s[0:100],re.MULTILINE):
            s = fix_python_code(s)
            document["show_script"].innerHTML = '<pre style="font-size: 10px;">'+s+'</pre>'
            break
  </script>
