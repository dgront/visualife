Python interpreter for turtle graphics (LOGO)
==============================================

Do you remember LOGO - procedural language from the '60 to draw programatically? So here is an interpreter made in
about 250 lines of Python. It recognizes the following commands:

  * **F** n -- move *forward* *n* steps,
  * **L** n -- turn *left* *n* degrees, *n* might be negative
  * **R** n -- turn *right* *n* degrees, *n* might be negative
  * **U** -- pen *up*
  * **D** -- pen *down*
  * **JUMP** x y -- turtle *jump*  to the given location
  * **SLOWER** n -- make steps smaller by *n*
  * **FASTER** n -- make steps larger by *n*
  * **REPEAT** n [*commands*] -- repeat given commands *n* times
  * **DEF** *name* := [*commands*]  -- define a procedure named *name*

The parameter *n* is mandatory. Commands in  ``[]`` block must be separated with a semicolon. Only one instruction per
line is allowed except a ``[]`` block, which must appear in a single line.

.. raw:: html
   :file: turtle.html

I've created this demo for my *"Object oriented programming and design"* course. It's purpose was to present
object-oriented design patterns in a concise form, without distracting ourselves on details. Most notably, code parsing
is quite simplified and there is no syntax error reporting. The program implements the following patterns:

  - *Command* -- each LOGO command is an object, derived from :class:`TurtleCommand` base class
  - *Factory* -- the commands are produced by :class:`Logo` class; I omitted object makers class hierarchy here for clarity
    and used just lamda functions
  - *Dispatch* -- the factory uses a dispatch (based on a Python dictonary) to join command names (strings) with
    respective makers (lambda expressions)
  - *Interpreter* -- well ... it interprets the command, right? The :class:`Turtle` class is used here as interpreter
    context
  - *Strategy* -- the turtle can draw on anything that implements :class:`GraphicDevice` interface; the program provides
    also a basic strategy: :class:`EchoDevice`
  - *Bridge* -- :class:`EchoDevice` is not very useful - it just prints output on the screen. This HTML page uses VisuaLife
    to make graphics. :class:`visualife.core.HtmlViewport` however has different interface than  :class:`EchoDevice`
    The natural way for turtle to draw a line is to use *line_to(x, y)* command since the :class:`Turtle` class
    know only its actual location; :class:`visualife.core.HtmlViewport` class on the other hand offers
    *line_to(x1, y1, x2, y2)* method. :class:`VisualifeBridge` class translates from :class:`GraphicDevice`
    to :class:`HtmlViewport` interface.



.. literalinclude:: ../_static/turtle.py
  :language: python
  :linenos:

