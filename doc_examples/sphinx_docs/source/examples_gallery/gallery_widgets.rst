.. _gallery_widgets:

Widgets
===========


.. image:: ../examples_gallery/_images/cyp46a1.png
   :target: ../examples_gallery/CYP46A1.html
   :width: 500px
   :height: 300px

.. image:: ../examples_gallery/_images/table_widget.png
   :target: ../examples_gallery/table_widget.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/msaviewer.png
   :target: ../examples_gallery/family_msa.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/sfbar.png
   :target: ../examples_gallery/features_bar.html
   :width: 650px
   :height: 200px

.. image:: ../examples_gallery/_images/aligner.png
   :target: ../examples_gallery/align_with_bioshell.html
   :width: 300px
   :height: 300px

