Interactive Ramachandran maps
==============================

The plot below is a 2D histogram of  :math:`\Phi, \Psi` angles of a protein backbone. Each of the twenty  amino acid types
imposes different structural restrictions on a protein chain, therefore the map looks different for each case.
Moreover proline residue (PRO) further restrict the geometry, which is clearly reflected by the maps.

This example uses AjaxSender widget to download histogram that have been pre-calculated in advance as well as
TooltipWidget to show tooltips.

.. raw:: html
   :file: ramachandran.html


.. raw:: html

  <div class="highlight-python notranslate">
    <div class="highlight" id="show_script">
    </div>
  </div>
  <script type="text/python">
    import re
    from browser import document, window
    from visualife.utils.text_utils import fix_python_code

    for s in document.select("script"):
        s = s.innerHTML
        if re.search("from browser import ",s[0:100],re.MULTILINE):
            s = fix_python_code(s)
            document["show_script"].innerHTML = '<pre style="font-size: 10px;">'+s+'</pre>'
            break
  </script>
