.. _doc_drawings:

Simple drawings
===============


.. image:: ../examples_gallery/_images/circle_segments.png
   :target: ../examples_gallery/circle_segments.html 
   :width: 300px
   :height: 300px


.. image:: ../examples_gallery/_images/moving_circles.png
   :target: ../examples_gallery/moving_circles.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/dots_and_grids.png
   :target: ../examples_gallery/dots_and_grids.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/show_colors.png
   :target: ../examples_gallery/show_colors.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/palettes.png
   :target: ../examples_gallery/show_color_palettes.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/polygon.png
   :target: ../examples_gallery/polygon.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/canvas.png
   :target: ../examples_gallery/canvas.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/mandelbrot.png
   :target: ../examples_gallery/mandelbrot.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/sierpinski.png
   :target: ../examples_gallery/sierpinski.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/fractal5.png
   :target: ../examples_gallery/fractal5.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/pattern.png
   :target: ../examples_gallery/fan_tessalation.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/logo.png
   :target: ../examples_gallery/turtle.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/msa_heatmap.png
   :target: ../examples_gallery/msa_as_heatmap.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/periodic_table.png
   :target: ../examples_gallery/periodic_table.html
   :width: 600px
   :height: 466px

