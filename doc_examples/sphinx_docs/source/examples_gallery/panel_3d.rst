Svg3DPanel example
================================

.. raw:: html
   :file: Svg3DPanel.html

Svg3DPanel draws 3D graphics using a projection from 3D to 2D in real time!
Use your mouse pointer to rotate the 3D cubes.


See this example on `CodePen <https://codepen.io/visualife/pen/mdPBgpy>`_ 


.. raw:: html

  <div class="highlight-python notranslate">
    <div class="highlight" id="show_script">
    </div>
  </div>
  <script type="text/python">
    import re
    from browser import document, window
    from visualife.utils.text_utils import fix_python_code

    for s in document.select("script"):
        s = s.innerHTML
        if re.search("from browser import ",s[0:100],re.MULTILINE):
            s = fix_python_code(s)
            document["show_script"].innerHTML = '<pre style="font-size: 10px;">'+s+'</pre>'
            break
  </script>
