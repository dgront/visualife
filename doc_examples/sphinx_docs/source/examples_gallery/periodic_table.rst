Periodic table
================

.. raw:: html
   :file: periodic_table.html
   
This great example was contributed by Barbara Musiałowicz!

.. literalinclude:: periodic_table.py
   :language: python
