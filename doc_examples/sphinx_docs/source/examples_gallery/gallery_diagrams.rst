.. _doc_diagrams:

Diagrams
========

.. image:: ../examples_gallery/_images/gamma_phylo_tree.png
   :target: ../examples_gallery/gamma_phylo_tree.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/simple_diagram.png
   :target: ../examples_gallery/simple_diagram.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/bisection_diagram.png
   :target: ../examples_gallery/bisection_diagram.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/vl_structure.png
   :target: ../examples_gallery/vl_structure.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/degrees.png
   :target: ../examples_gallery/fahrenheit_interactive.html
   :width: 300px
   :height: 300px
