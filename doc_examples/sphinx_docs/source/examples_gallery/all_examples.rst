List of all VisuaLife online examples
=====================================

.. toctree::
    :maxdepth: 1

    3dplot
    CYP46A1
    align_with_bioshell
    all_examples
    bars
    bisection_diagram
    bubblechart
    canvas
    chi_angles
    circle_segments
    diagram
    dots_and_grids
    fahrenheit_interactive
    family_msa
    fan_tessalation
    features_bar
    fractal5
    gallery_3d_graphics
    gallery_brython_only
    gallery_diagrams
    gallery_simple_drawing
    gallery_simple_plots
    gallery_widgets
    histogram2d
    histograms
    lineplot
    mandelbrot
    mandelbrot_wasm
    molecule
    moving_circles
    moving_plot
    msa_as_heatmap
    panel_3d
    polygon
    ramachandran
    scatterplot
    show_color_palettes
    show_colors
    sierpinski
    simple_diagram
    table_widget
    turtle
    vl_structure