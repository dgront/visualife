Phylogenetic tree of CYP enzymes in gammaproteobacteria
==========================================================

.. raw:: html
   :file: gamma_phylo_tree.html

The data has been published in: N.W. Msomi et al., *"In Silico Analysis of P450s and Their Role in Secondary Metabolism in the Bacterial Class Gammaproteobacteria"*
`Molecules 2021 26(6):1538 <https://www.mdpi.com/1420-3049/26/6/1538>`_



.. literalinclude:: gamma_phylo_tree.py
   :language: python

