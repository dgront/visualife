.. _doc_plots:

Simple plots
============

.. image:: ../examples_gallery/_images/bubblechart.png 
   :target: ../examples_gallery/bubblechart.html 
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/histograms.png
   :target: ../examples_gallery/histograms.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/line_plot.png
   :target: ../examples_gallery/lineplot.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/bar_plot.png
   :target:  ../examples_gallery/bars.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/scatter_plot.png
   :target: ../examples_gallery/scatterplot.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/moving_plot.png
   :target: ../examples_gallery/moving_plot.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/histogram2d.png
   :target: ../examples_gallery/histogram2d.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/ramachandran.png
   :target: ../examples_gallery/ramachandran.html
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/chi_angles.png
   :target: ../examples_gallery/chi_angles.html
   :width: 300px
   :height: 300px
