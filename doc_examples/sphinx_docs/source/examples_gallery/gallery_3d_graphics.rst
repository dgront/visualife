.. _doc_3d:

3D graphics
===========


.. image:: ../examples_gallery/_images/3dplot.png
   :target: ../examples_gallery/3dplot.html 
   :width: 300px
   :height: 300px

.. image:: ../examples_gallery/_images/molecule.png
   :target: ../examples_gallery/molecule.html
   :width: 300px
   :height: 300px
 
.. image:: ../examples_gallery/_images/panel_3d.png 
   :target: ../examples_gallery/panel_3d.html 
   :width: 300px
   :height: 300px
