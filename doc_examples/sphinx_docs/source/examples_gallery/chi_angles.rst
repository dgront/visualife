Interactive maps of :math:`\chi` angle distributions
====================================================

The plot below displays histograms of :math:`\chi` angles, which are dihedral angles in amino acid side chains,
as they are observed in protein structures. Each of the twenty  amino acid types features a structurally different side chain, which results in a 
specific :math:`\chi` angle map. Moreover, the amino acid types differ by the length of their side chains, so
SER, VAL, THR and CYS have only one  :math:`\chi` angle while LYS and ARG have four, denoted from :math:`\chi_1`
to :math:`\chi_4`


This example uses AjaxSender widget to download histograms that have been pre-calculated in advance as well as
TooltipWidget to show tooltips.

.. raw:: html
   :file: chi_angles.html


.. raw:: html

  <div class="highlight-python notranslate">
    <div class="highlight" id="show_script">
    </div>
  </div>
  <script type="text/python">
    import re
    from browser import document, window
    from visualife.utils.text_utils import fix_python_code

    for s in document.select("script"):
        s = s.innerHTML
        if re.search("from browser import ",s[0:100],re.MULTILINE):
            s = fix_python_code(s)
            document["show_script"].innerHTML = '<pre style="font-size: 10px;">'+s+'</pre>'
            break
  </script>
