.. _doc_brython:

Brython-only examples
========================

.. image:: ../examples_gallery/_images/mandelbrot_wasm.png
   :target: ../examples_gallery/mandelbrot_wasm.html
   :width: 300px
   :height: 300px

