.. _doc_examples:

Examples gallery
================

.. toctree::
   ../examples_gallery/gallery_simple_drawing
   ../examples_gallery/gallery_diagrams
   ../examples_gallery/gallery_simple_plots
   ../examples_gallery/gallery_3d_graphics
   ../examples_gallery/gallery_widgets
   ../examples_gallery/gallery_brython_only


