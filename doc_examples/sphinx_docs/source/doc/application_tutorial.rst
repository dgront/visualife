.. _doc_app_tutorial:

Application tutorial
=====================

VisuaLife has a module named ``widget`` which contains components for creating applications for protein analysis. In this tutorial you will see how to display various type of protein data and how to ensure communication between widgets. Application described in this tutorial is availble :doc:`here <../doc/protein_inspector>`.  

What data can VisuaLife display?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	- protein sequence by :doc:`../api/visualife.widget.SequenceViewer`
	- protein 3D structure by :doc:`../api/visualife.widget.StructureViewer`
	- protein secondary structure by :doc:`../api/visualife.widget.SecondaryStructureViewer`
	- binding sites by :doc:`../api/visualife.widget.SequenceViewer` or :doc:`../api/visualife.widget.SequenceFeaturesBar`
	- Multiple Sequence Alignment by :doc:`../api/visualife.widget.MSAViewer`

Today you will learn about first four widgets but if you would like to know more about ``MSAViewer`` don't hesitate to look at its :doc:`documentation <../api/visualife.widget.MSAViewer>`. 

Getting and parsing data
^^^^^^^^^^^^^^^^^^^^^^^^

Use :doc:`../api/visualife.widget.AjaxSender` to get the data that you need. This object takes **URL** as a first argument and a **function** to call after receiving the data. If you want to get file from your computer URL will be a path to this file:


.. literalinclude:: ../examples_gallery/msa_as_heatmap.html
   :language: python
   :lines: 68

"GET" is an optional argument that indicates request type. Default is "POST". More about HTTP requests you can find `here <https://www.w3schools.com/tags/ref_httpmethods.asp>`_.

AjaxCommunication widget is often used to fetch a protein structure in the PDB format. This can be done by the following snippet:

.. code-block:: python

  def receive_structure(evt = None):
      print(evt.text.split("\n")[:10])

  AjaxCommunication("https://files.rcsb.org/view/%s.pdb" % pdb_code, receive_structure)()

``pdb_code`` variable provides the code of a protein to be acquired (e.g. *2gb1*) and ``receive_structure()``
is the callback method that is called when ``AjaxCommunication`` finally receives the data. The protein
comes as text (in the PDB format) format and it's ``receive_structure()`` responsibility to process it further.
Here it just breaks the text into lines and prints the top 10 lines of the file.

Presented application was made in a way that it can work from every computer so the data should be download from other websites. We use https://www.ebi.ac.uk and https://www.rcsb.org which allows to get their files with "GET" request and no `CORS error <https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors>`_. We wanted to analyse every possible PDB file, so we added input window where you can type your PDB code. Below there is a part of code responsible for getting this data.

.. literalinclude:: ../_static/protein_properties.py
   :language: python
   :lines: 160-162

After receiving the respond, callback function from AjaxSender constructor is called. In this function you will need to decode the data with ``json.loads()`` function which converts JSON string to python structures as lists and dictionaries. Then you will parse the data and get the info that you need to eg. create widgets. In the below example the variable ``first_res_id`` is set from received data.

.. literalinclude:: ../_static/protein_properties.py
   :language: python
   :lines: 130-134


Creating widgets
^^^^^^^^^^^^^^^^

One thing you need to be careful about is that the order of receiving all these responses is unknown, as well as the order of function calls. Sometimes you need to already have some variable set, before other function is called. If this is the case, then it is better to send some request later, not all at once. 

In this application we need to know the number of residues to create secondary structure string. This is why we will call ``ss2_ajax()`` after setting ``N_res`` variable. 

.. literalinclude:: ../_static/protein_properties.py
   :language: python
   :lines: 118-127


We already have a sequence but because we are going to mark binding sites on the SequenceViewer, we will also send a request about it. After receiving the data SequenceViewer will be created and we will be sure to have the right order of calls.

Let's look on the ``binding_site()`` function (called after receiving the data - last line in the code above)

.. literalinclude:: ../_static/protein_properties.py
   :language: python
   :lines: 58-78
   :linenos:


In the 4th line SequenceViewer widget is created. In the next line secondary structure is set (to make it possible to colour the sequence with this info). In the line no. 6 function ``click_on_aa()`` is set as a callback - this function will be called after clicking on the letter in the sequence (more about it in the next paragraph). In the next line colour pallete is set for colouring the regions. In lines 8-20 received data are parsed according to needs and finally in line no. 21 proper aminoacides from binding sites were added to the corresponding regions. 

Communication between widgets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now let's go back to setting callback function in SequenceViewer. Function is called after clicking on an aminoacid. Every function which is called after firing an event - takes this event as an argument. From the event we can get event target's id, which will be necessery to conclude which aminoacid was clicked. Having this information we can transfer it to StructureViewer and change style of this aminoacid in this widget. It allows for example to select every aminoacid from one binding site and see how it looks like in the 3D structure.

.. literalinclude:: ../_static/protein_properties.py
   :language: python
   :lines: 44-55
   :linenos:

Because we want to change the style back after second click we are storing style info about selected aminoacides in a dictionary. After second click we can go back to previous representation in StructureViewer and unselect aminoacid in SequenceViewer. 

That's all! If you need further information, please look at :doc:`widget documentation <../widget>` and :doc:`../doc/examples_gallery`.
