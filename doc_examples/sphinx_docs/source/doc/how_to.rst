.. _how_to:

How To
============

This section provides answers to frequent questions, that looks easy to answer, but that requires digging somewhat
deeper into the documentation.

General issues
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

How to convert an SVG file produced by VL to another format?
------------------------------------------------------------

The easiest way is to use `Inkscape`_ program. The conversion can be done from command line:

.. code-block:: bash

  # on MacOS
  ~/bin/Inkscape.app/Contents/MacOS/inkscape plot.svg --export-filename=plot.png -d 500

  # on Windows
  inkscape "C:\path\plot.svg" --export-filename="C:\path\plot.png"
  
  # on Linux
  inkscape -z -e plot.png -w 1000 -d 500 plot.svg

Questions regarding plotting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

How to adjust data range on a plot?
-----------------------------------

Data range is set by :class:`Plot` class constructor, e.g.

.. code-block:: python

  pl = Plot(drawing,200,1000,200,1000,0.0,110.0,0.0,150.0,axes_definition="UBLR")
  pl.bars(x_data, y_data, adjust_range=False)
  
where the plotting area is a square 800x800 (from 200 to 1000 along each direction) and data range on X and Y
axes are [0, 110.0] and [0.0, 150.0], respectively. These ranges can be also set after a :class:`Plot` object
has been constructed:

.. code-block:: python

  pl.axes["B"].min_data_value = 0
  pl.axes["B"].max_data_value = 15

This will affect bottom axis (because of **B**). **Remember** to switch off automatic data range assignment
by setting ``adjust_range=False`` kwarg argument when plotting.

Questions regarding widgets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

How to download a PDB file?
-----------------------------------

Use AjaxCommunication widget for that purpose:

.. code-block:: python

  def receive_structure(evt = None):
      print(evt.text.split("\n")[:10])

  AjaxCommunication("https://files.rcsb.org/view/%s.pdb" % pdb_code, receive_structure)()

``pdb_code`` variable provides the code of a protein to be acquired (e.g. *2gb1*) and ``receive_structure()``
is the callback method that is called when ``AjaxCommunication`` finally receives the data. The protein
comes as text (in the PDB) format and it's ``receive_structure()`` responsibility to process it further.
Here it just breaks the text into lines and prints the top 10 lines of the file.

.. rubric::
  References

.. target-notes::

.. _`Inkscape` : https://inkscape.org

