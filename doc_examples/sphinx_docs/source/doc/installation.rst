.. _doc_installation:

Installation
============

**Including VisuaLife in your webpage**

If you want to add Visualife script to you HTML page, just do the two simple steps:

  - download `visualife.brython.js <https://bitbucket.org/dgront/visualife/raw/HEAD/visualife/visualife.brython.js>`__ file
  - add the following line to your project (remember to enter relative to path to the file)

.. code-block:: bash

    <script src="visualife.brython.js"></script>


**Local installation**

Install the latest released verion of visualife with this line:

.. code-block:: bash

  pip install visualife


You can also clone raw VisuaLife repository from
`BitBucket website <https://bitbucket.org/dgront/visualife/>`__. Here is the command that you can copy, paste and run in a terminal:

.. code-block:: bash

  git clone https://bioshell@bitbucket.org/dgront/visualife.git

