Change log
=====================================

To be released in version 1.1
++++++++++++++++++++++++++++++++

* [core]: few drawing methods made significantly faster (`#af08288  <https://bitbucket.org/dgront/visualife/commits/af08288c9bcdd899b2ec38290dfb4ca036818c72>`__)

Version 1.0
+++++++++++++

:Date: May 7, 2021

* VisuaLife has been published in Bioinformatics journal