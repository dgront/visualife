.. _doc_tempaltes:

Quick start from template files
===============================

To start working with VisuaLife library you can use these two template files:

  - `vl_template.html <https://bitbucket.org/dgront/visualife/raw/HEAD/doc_examples/sphinx_docs/source/_static/vl_template.html>`_
  - `vl_template.py <https://bitbucket.org/dgront/visualife/raw/HEAD/doc_examples/sphinx_docs/source/_static/vl_template.py>`_
 
and VL library in binary format `visualife.brython.js <https://bitbucket.org/dgront/visualife/raw/HEAD/visualife/visualife.brython.js>`_ . 
The .html file is a web page with included VL library, and the .py file is a python code that will be executed in this web page.

You also need to have brython files in ``external_libs/`` directory (`brython.js <https://bitbucket.org/dgront/visualife/raw/HEAD/visualife/external_libs/brython.js>`_ and `brython_stdlib.js <https://bitbucket.org/dgront/visualife/raw/HEAD/visualife/external_libs/brython_stdlib.js>`_)

Brython script from .py file needs to be loaded on the web page. For that you need to run HTTP server which will run the script. Simple opening in web browser won't work here.
To make these files to work use one from following solutions:
  - **for PyCharm users:** open .html file in PyCharm and then use function "run in a browser" - this option will appear in the rigth upper corner after mouseover on the .html text
  - **for console users:** in the directory with your files, run the command ``python3 -m http.server`` and then open the browser on the ``http://0.0.0.0:8000`` adress. Choose .html file to display. 