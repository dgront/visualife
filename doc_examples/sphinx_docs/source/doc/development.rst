.. _doc_development:

Development
============

Web distribution
^^^^^^^^^^^^^^^^

VisuaLife library also comes with a "package" version that is a single file that may be easily incorporated in a web page.
The file must be updated after every change in the library, otherwise the changes will not affect any of HTML examples.
SVG examples will work properly, as they use *regular* Python version of the library.

To build ``visualife.brython.js`` file make sure you have ``brython`` package installed on your machine, e.g.

   .. code-block:: bash
      
      pip3 install brython

then go to the ``distribution`` directory and simply run:

.. code-block:: bash

  ./make_vl_js.sh
  
New ``visualife.brython.js`` file will be created in ``distribution/`` directory.