.. _brython_tips:

Brython tips
============

The main objective for the VisuaLife library is to move all the development of the client-side part of a web application
to Python. Brython is the mean to achieve that goal. By translating Python code to JavaScript, it can make your data,
plots and graphics interactive. This document provides a quick list of tips intended to help you building your own web
application with VisuaLife and Brython. For a more elaborate documentation  you should head
to `official Brython website`_

Adding HTML page elements with Brython
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``html`` module of Brython distribution provides classes that correspond to HTML elements. E.g. to add the new DIV
element, use the following:

.. code-block:: python

  from browser import html, document
  el = html.DIV("A text inside a div", id="id_of_this_div"):
  document["outer_element"] <= el

Note how the newly created element ``el`` is inserted into the page.

Accessing HTML page elements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The easiest way to access an element of a page is to reach it by its ID, e.g. ``document["element_id"]``. This way
one can change element properties or alter its content:

.. code-block:: python

  document["a_div"].innerHTML = "The quick brown fox jumps over the lazy dog"
  document["a_div"].style.


Binding Python code to HTML page elements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

First define your function, e.g.:

.. code-block:: python

  def action(evt):
    print("user interacted with", evt.target.id)

The content printed with the ``print()`` method will show up in a browser's console; that is the easiest way for debugging.
Note, that the target element, i.e. the element user clicked, hovered with a mouse pointer, etc, can be accessed
as a JS object as ``evt.target`` field. Moreover, ID of that element can be found as ``evt.target.id``.

To bind this function to an element, just use:

.. code-block:: python

  document["my_element_id"].bind("event-name", action)

where ``action`` is the method to be called and ``"event-name"`` is the name of the event that should trigger that function.
Here is a list of commonly used events:

 * *input* -- user changes a form element, e.g. clicks on a radio button or enters a text to an input field
 * *click* -- user clicks on an element, e.g. on a ``<DIV>``
 * *mouseover* -- user hoovers an element with a mouse pointer

Using Javascript libraries in Brython code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Brython can be seamlessly combined with Javascript libraries. Javascript and Python syntax for calling object's methods
and reaching its properties is basically the same. The differences however are in importing modules (``import`` keyword)
and in calling a constructor - Python doesn't have ``new`` operator. Compare the two programs below; both use
`XMLHttpRequest`_ library to asynchronously download some content from a server.

.. code-block:: python

  from  browser import document, window, console

  def req_listener():
      global request
      console.log(request.responseText)

  request = window.XMLHttpRequest.new()
  request.open('GET', "http://www.example.org/example.txt")
  request.responseType = 'arraybuffer'
  request.send()
  request.onloadend = reqListener

.. code-block:: javascript

  function reqListener () {
    console.log(this.responseText);
  }

  var request = new XMLHttpRequest();
  request.addEventListener("load", reqListener);
  request.responseType = 'arraybuffer'
  request.open("GET", "http://www.example.org/example.txt");
  request.send();

The two codes looks very similar. Three thing are worth noticing:

  1. Javascript libraries are typically accessible from ``window`` module. Brython code must ``from browser import window``,then ``window.XMLHttpRequest`` can be accessed.
  2. Brython offers ``new()`` function to a Javascript class that should be used to create a constructor of that class
  3. In this particular case a global ``request`` variable is used, which in Python must be explicitly marked as ``global``

Styling with Brython
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Every HTML (or SVG) element has a `style` property. That property behaves as an object with attributes
corresponding to SVG styling properties. The list of supported properties can be found `here for HTML`_
and  `here for SVG`_. **Note** that CSS properties are spelled differently than their Brython counterparts.
Since a dash character cannot appear in a Python variable name, Bryton uses camelCase, e.g. instead of
``background-color`` CSS property one must use ``backgroundColor``.

.. code-block:: python

  from browser import html, document

  el = document["button_to_be_styled"]  # the element
  style = el.style
  style.backgroundColor = "red"
  style.color = "grey"



Parallel execution in threads
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Brython implements `WebWorkers API`_ in the `worker`_ module. This however requires extraction of the congruent
part of the code into a separate block of code.


.. rubric::
  References

.. target-notes::

.. _`official Brython website` : https://brython.info

.. _`XMLHttpRequest` : https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest

.. _`here for HTML` : https://developer.mozilla.org/en-US/docs/Web/CSS/Reference

.. _`here for SVG` : https://css-tricks.com/svg-properties-and-css/

.. _`WebWorkers API` : https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API

.. _`worker` : https://brython.info/static_doc/en/worker.html



