.. _doc_plot_tutorial:

Plot tutorial
=============

This page will guide you through your first script that makes a plot with VisuaLife

Choose your viewport: HTML or SVG?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Viewport is a class that is responsible for displaying your drawing. There are three viewports available:

  - :doc:`../api/visualife.core.SvgViewport`: writes your drawing or a plot into an ``.svg`` file on your computer; you have to provide
    the name of the output file
  - :doc:`../api/visualife.core.HtmlViewport`:  draws or makes a plot on a web page. You have to provide the ``id`` string of a web element,
    the graphics will be added to that element as SVG elements.
  - :doc:`../api/visualife.core.CanvasViewport`: it also draws or makes a plot on a web page, but uses pixels rather than SVG. It can draw much
    more objects (eg. data points in your plot), but the content is not interactive

SvgViewport and HtmlViewport  behave in the same manner. They share the same set of methods and produces graphics
that looks exactly the same. You should make your drawing in SvgViewport first and once it works as you expect,
copy & paste your Python code into a web page. Just remember to replace SvgViewport with HtmlViewport.

**Simplest SvgViewport example**

Let's begin with a very simple example that produces an SVG file:

.. literalinclude:: ../_static/simplest.py
   :language: python
   :linenos:


**Simplest HtmlViewport example**

Now the same example, but now as an HTML page:

.. literalinclude:: ../_static/simplest.html
   :language: html
   :linenos:


As you can see there are few differences in this example comparing to the SvgViewport:

  - HTML tags that create a page
  - inlucing brython and visualife libraries in the <head> part
  - ``from browser import document`` that is a brython import which allows user to access webpage elements
  - importing and using HtmlViewport instead of SvgViewport

Simple plot in terminal - creating SVG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can already make a simple drawing in both terminals so now it's time for creating a plot. You can do it in SvgViewport using simple python script. Lets say you have a file co2emission.txt on your computer and it looks like this:

.. literalinclude:: ../_static/co2emission.txt

To make a plot you will simply run:

.. code-block:: bash

  python3 plot_from_file.py co2emission.txt

This command will make a plot from first column as X and second column as Y by default. If you want to make a plot from year (1st column) and emission_change(3rd column) you can run:

.. code-block:: bash

  python3 plot_from_file.py co2emission.txt 1 3

Script also takes first row as axis labels. Below you can see how the script looks like:

.. literalinclude:: ../_static/plot_from_file.py
   :language: python
   :linenos:


First twenty lines is responsible for loading a file and packing data into lists. Then from line 25th SvgViewport and :doc:`../api/visualife.core.Plot` objects are created. Finally method Plot.scatter and Plot.line are called to draw both dots and line for this plot that will look like this:

.. image:: ../_static/co2emission.svg
   :width: 600px
   :height: 600px


How to make plots online?
^^^^^^^^^^^^^^^^^^^^^^^^^

**Simple plot in a web-browser**

Now we will do the same plot as in previous example but using HtmlViewport so it is going to be display in a browser on a HTML page. We will use brython in a <script> tag to make it work. One difference is that you need to have your file already in a script, because brython will not read file from your computer in the same way as python. Other differences were already discussed earlier in first part of these tutorial. 

.. literalinclude:: ../_static/plot_from_file.html
   :language: python
   :linenos:

The plot will look exactly the same.

**Online plot from REST data**

Sometimes there is a need to download data from the Internet. We created :doc:`../api/visualife.widget.AjaxSender` to make it easier for VisuaLife users to download data. Using this class you can send POST or GET requestes and use recived data eg. for plotting. There is also a python library ``json`` that can code and decode JSON format that is used in this requestes. In this example JSON looks like `this <https://raw.githubusercontent.com/sgreben/jp/master/examples/mvrnorm.json>`_

In lines no. 38 (script below) the AjaxSender object is constructed. In the next line call operator is used to send a request. After reciving the respons method ``on_complete`` will be called. ``json.loads()`` change string variable to python object eg. list or dictionary. Then you can get your data and plot them as in the previous example.

.. literalinclude:: ../_static/plot_from_rest_data.html
   :language: python
   :linenos:

.. raw:: html
   :file: ../_static/plot_from_rest_data.html

**Let's make it interactive!**

This is the last and probably the most interesting part in this tutorial. You are now going to add interactivity to your plot using only Python(Brython exactly)! It is very simple. All the points are in the SVG tag with id ``serie-1``. To select these points you need to select this tag by id and take its children elements (line no. 42). Then iterate over all points and bind a function that will be called on event (line no. 44). All the events can be found `here <https://www.w3schools.com/tags/ref_eventattributes.asp>`_. Function needs to take one argument - fired event.

Now when you put your mouse over the point you can see that it's id appears on the screen!

.. literalinclude:: ../_static/plot_from_rest_interactive.html
   :language: python
   :linenos:

.. raw:: html
   :file: ../_static/plot_from_rest_interactive.html
