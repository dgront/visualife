.. _doc_live_demo:

Live demo
============

Here you can try VisuaLife library


.. raw:: html

  <div id="demo"></div>
  <script type="text/python">
      from VLScriptWidget import VLScriptWidget
      sw2 = VLScriptWidget("sin_cos.py","demo", height=350)
  </script>
