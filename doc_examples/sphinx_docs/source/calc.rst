.. automodapi:: visualife.calc
   :no-inheritance-diagram:
   :inherited-members:


.. automodapi:: visualife.calc.math_utils
  :no-inheritance-diagram:
