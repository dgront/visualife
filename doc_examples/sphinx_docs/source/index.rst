Welcome to VisuaLife's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Documentation
   :name: sec-doc
   :hidden:

   doc/installation
   doc/examples_gallery
   doc/live_demo
   doc/how_to
   doc/tutorials
   doc/brython_tips
   doc/change_log
   doc/development

.. toctree::
   :maxdepth: 2
   :caption: API reference
   :name: sec-apiref
   :hidden:

   calc
   core
   data
   diagrams
   serverside
   utils
   widget
   doc/data_formats

VisuaLife is a lightweight visualisation library implemented in Python. It's intended to make impressive graphics
in a  web browsers as easily as possible - just with a few lines of Python. Yes, Python! The library uses
`Brython <https://brython.info>`__ to run on the client side. The library can be also used in *regular* Python script, run on your local machine
to produce output in SVG format. VisuaLife can help anyone who would like to quickly and easily make interactive plots,
drawings and data applications.

The source code of VisuaLife is hosted on `BitBucket <https://bitbucket.org/dgront/visualife/>`__.
In order to use it however you don't have to clone the package; just download the
`visualife.brython.js <https://bitbucket.org/dgront/visualife/raw/HEAD/visualife/visualife.brython.js>`__ file,
more details in :ref:`installation section<doc_installation>`


