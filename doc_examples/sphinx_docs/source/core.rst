.. automodapi:: visualife.core
   :no-inheritance-diagram:
   :inherited-members:
   :skip: ColorMap
   :skip: colormap_by_name
   :skip: create_style
   :skip: get_font_size


.. automodapi:: visualife.core.styles
  :no-inheritance-diagram:

.. automodapi:: visualife.core.shapes
   :no-inheritance-diagram:
   :skip: polar_to_cartesian
   :skip: regular_polygon
   :skip: linspace

.. automodapi:: visualife.core.three_d
   :no-inheritance-diagram:
   :skip: HtmlViewport
   :skip: ScoreFile
   :skip: Structure
   :skip: Residue
   :skip: Chain
   :skip: Atom
   :skip: Bond
   :skip: HSSP
   :skip: Molecule


