.. automodapi:: visualife.diagrams
   :no-inheritance-diagram:
   :skip: make_darker
   :skip: mix_colors
   :skip: rgb_to_hex
