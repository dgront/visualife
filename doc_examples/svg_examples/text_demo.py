import sys

sys.path.append('../../')

from visualife.core import SvgViewport
from visualife.core.styles import color_name_to_hex, make_darker

fill_color = color_name_to_hex("SteelBlue")
stroke_color = make_darker("SteelBlue", 0.3)
drawing = SvgViewport("text.svg", 500, 500,text_style='fill:%s;stroke:%s'%(fill_color,stroke_color))
# --- add google fonts - they require Internet connection
drawing.add_font("Liberation Regular")
drawing.add_font("Comforter")

drawing.text("text",250,250,"The quick brown fox jumps over the lazy dog",
             font_size=20, stroke_width=0.3, font_family="Comforter")

# for i in range(45,361,45) :
#   drawing.text("text",250,250,"The quick brown fox jumps over the lazy dog",font_size=20, stroke_width=0.3, angle=i)
drawing.close()
