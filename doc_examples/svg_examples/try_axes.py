import sys

sys.path.append('../../')
from visualife.core import SvgViewport
from visualife.core.axes import AxisX, AxisY

drawing = SvgViewport("try_axes.svg", 300, 300)

x_ax_b = AxisX(250.0,50.0,250,-3.14159,3.14159,'B')
x_ax_b.tics(16,4)
x_ax_b.label = "15 small and 4 large tics"
x_ax_u = AxisX(50.0,50.0,250,-3.14159,3.14159,'U')
x_ax_u.tics(17,5)
x_ax_u.label = "17 small and 5 large tics"
y_ax_l = AxisY(50,50.0,250,-3.14159,3.14159,'L')
y_ax_l.tics(19,4)
y_ax_l.label = "19 small and 4 large tics"
y_ax_r = AxisY(250,50.0,250,-3.14159,3.14159,'R')
y_ax_r.tics(10,4)
y_ax_r.label = "10 small and 4 large tics"

x_ax_b.draw(drawing)
y_ax_r.draw(drawing)
x_ax_u.draw(drawing)
y_ax_l.draw(drawing)
drawing.close()
