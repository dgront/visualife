from random import random
import sys

sys.path.append('../../')

from visualife.core.styles import make_darker
from visualife.core import Plot, SvgViewport


drawing = SvgViewport("bubblechart.svg", 600, 600,"white")

xyz_data1 = [[random(), random(), random() * 100.0] for i in range(15)]
for t in xyz_data1:
    if t[0] > t[1]: t[0], t[1] = t[1], t[0]
xyz_data2 = [[random(), random(), random() * 40.0] for i in range(30)]
for t in xyz_data2:
    if t[0] < t[1]: t[0], t[1] = t[1], t[0]

stroke_color = make_darker("SteelBlue", 0.3)
pl = Plot(drawing, 100, 500, 100, 500, 0.0, 1.0, 0.0, 1.0, axes_definition="UBLR")
pl.axes["B"].label = "y_data"
pl.axes["L"].label = "x_data"
for l in ["B", "U", "L", "R"]:
    pl.axes[l].fill = stroke_color
    pl.axes[l].stroke = stroke_color
    pl.axes[l].stroke_width = 3.0
pl.plot_label = "Random values of x and y"

pl.bubbles(xyz_data1, markersize=3.5, colors="tableau10", title="serie1", stroke="darker")
pl.bubbles(xyz_data2, markersize=1.5, cmap="blues", title="serie2", stroke="white")
pl.draw(grid=True)
drawing.close()
