import sys
sys.path.append('../../')
from visualife.core import SvgViewport

from visualife.diagrams import *

drawing = SvgViewport("makao.svg", 400, 400, bgcolor="white")
drawing.add_font("Liberation Sans")
drawing.add_font("Liberation Regular")
drawing.add_font("Comforter")
uml = Diagram(drawing, "diagrams_uml")
uml.node_style["stroke_width"] = "1px"
uml.text_style["font_family"] = 'Comforter'
# uml.add_node(RectNode("point", "Point", 100, 50, 80, 30, font="LiberationRegular"))
uml.add_node(RectNode("point", "Point", 100, 50, 80, 30))
node_base = RectNode("node_base", "NodeBase", 0, 0, 80, 30, font_style={"font_family":"Comforter"})
uml.add_node(node_base, below=uml["point"], mark="inheritance", reverse_connector=True)

uml.draw(drawing)
drawing.close()


