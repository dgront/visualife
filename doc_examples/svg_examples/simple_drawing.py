import sys
from random import random

sys.path.append('../../')

from visualife.core import SvgViewport
from visualife.core.styles import make_brighter

def random_circles(n_circles, drawing):

    for i in range(n_circles) :
      drawing.circle(str(i),random()*300, random()*300, random()*20)

def random_rectangles(n_rect, drawing):

    for i in range(n_rect) :
      drawing.rect(str(i),random()*300, random()*300, random()*20, random()*20)
    for i in range(n_rect) :
      drawing.square(str(i),random()*300, random()*300, random()*20)
    
def random_ellipses(n_el, drawing):

    for i in range(n_el): 
      drawing.ellipse(str(i),random()*300, random()*300, random()*35, random()*15)

def random_lines(n_lines, drawing):

    for i in range(n_lines) :
      drawing.line(str(i),random()*300, random()*300, random()*300, random()*300)

if __name__ == "__main__" :
    fill = "#f591a3"

    stroke = make_brighter(fill)

    drawing = SvgViewport("draw.svg",300,300,style='fill:%s;stroke:%s'%(str(fill),str(stroke)))
    
    random_circles(5,drawing)
    random_rectangles(5,drawing)
    random_ellipses(5,drawing)
    random_lines(5,drawing)
    drawing.close()
    
