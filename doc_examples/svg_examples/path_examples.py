import sys

sys.path.append('../../')

from visualife.core import SvgViewport

path_style="""fill="none"
  stroke="#000"
  stroke-linecap="round"
  stroke-linejoin="round"
  stroke-width="5"
"""

p0 = [["M",  20, 20], ["L", 80, 80]]
p1 = [["M", 20, 80], ["L", 50, 20], ["L", 80, 80]]
p2 = [["M", 20, 20], ["Q", 80, 20, 80, 80]]
p3 = [["M", 20, 50], ["C", 20,80, 80, 80, 80, 50]]
p4 = [["M", 20, 20], ["L", 80, 20], ["L", 20, 50], ["L", 80, 50], ["L", 20, 80], ["L", 80, 80]]
p5 = [["M", 20, 50], ["A", 50, 25, 0, 0, 1, 80, 80]]
p6 = [["M", 20, 50], ["S", 20, -20, 40, 50], ["S", 70, 80, 80, 40]]
p7 = [["M", 50, 20], ["Q", 20, 50, 50, 80]]
p8 = [["M", 20, 20], ["Q", 50, 20, 50, 50], ["T", 80, 80]]

paths = [p0, p1, p2, p3, p4, p5, p6, p7, p8]

if __name__ == "__main__":
    i_path = 0
    drawing = SvgViewport("paths.svg", 300, 300)
    for i in [0,100,200] :
        for j in [0, 100, 200]:
            id_str= "-%d-%d" % (i,j)
            drawing.rect("r" + id_str, 10 + i, 10 + j, 80, 80, stroke='black', stroke_width='1', fill='gainsboro')
            drawing.path("p" + id_str, paths[i_path % len(paths)], fill="none", stroke="#000", stroke_linecap="round",
                         stroke_linejoin="round", stroke_width="5", translate=[i, j])
            i_path += 1

    drawing.close()
