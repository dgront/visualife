from visualife.core import SvgViewport
from visualife.core.styles import known_color_scales, make_darker, mix_colors
from visualife.diagrams import *

drawing = SvgViewport("vl_structure.svg", 600, 500)
dia = Diagram(drawing, "vl_structure", autoconnect=False)

palette = known_color_scales["pastel1"]
# ---------- Styles for the three types of nodes: HTML-compatible, SVG-compatible or both-compatible
both_viewports_style = {'fill': palette[0], 'stroke': make_darker(palette[0])}
html_only_style = {'fill': palette[1], 'stroke': make_darker(palette[1])}
svg_only_style = {'fill': palette[2], 'stroke': make_darker(palette[2])}
# ---------- Text color for the three types of nodes: HTML-compatible, SVG-compatible or both-compatible
both_viewports_text = {'fill': make_darker(palette[0], 0.6)}
html_only_text = {'fill': make_darker(palette[1], 0.6)}
svg_only_text = {'fill': make_darker(palette[2], 0.6)}

dia.add_node(RectNode("wig", "visualife.widgets", 160, 30, 320, 30,
                    node_style=html_only_style, text_style=html_only_text))
wig = dia["wig"]
dia.add_node(RectNode("dgr", "visualife.diagrams", 0, 0, 155, 30, node_style=both_viewports_style,
                    text_style=both_viewports_text), left_below=wig)
dia.add_node(RectNode("dat", "visualife.data", 0, 0, 155, 30, node_style=both_viewports_style,
                    text_style=both_viewports_text), right_below=wig)
dia.add_node(RectNode("utl", "visualife.utils", 0, 0, 155, 30, node_style=both_viewports_style,
                    text_style=both_viewports_text), dx=10, dy=10, below=dia["dgr"])
utl = dia["utl"]
dia.add_node(RectNode("cal", "visualife.calc", 0, 0, 155, 30, node_style=both_viewports_style,
                    text_style=both_viewports_text), right_of=utl, dx=10, dy=10)

core_fill = mix_colors('white', palette[0], 0.8)

dia.add_node(RectNode("cor", "visualife.core", 0, 0, 320, 100, rx=10, ry=10,
                     node_style={'stroke': 'black', 'stroke_width': '1px', 'stroke_dasharray': 4, 'fill': core_fill},
                     text_style=both_viewports_text), left_below=utl)
cor = dia["cor"]

dia.add_node(RectNode("sha", "shapes", 0, 0, 140, 30, node_style=both_viewports_style,
                    text_style=both_viewports_text), right_below=utl, dy=35)
dia.add_node(RectNode("stl", "styles", 0, 0, 140, 30, node_style=both_viewports_style, text_style=both_viewports_text),
                    left_below=dia["cal"], dy=35)

dia.add_node(RectNode("svg", "SvgViewport", 0, 0, 90, 30, node_style=svg_only_style,
                    text_style=svg_only_text), left_below=dia["sha"], dy=40)
dia.add_node(RectNode("htm", "HtmlViewport", 0, 0, 90, 30, node_style=html_only_style, text_style=html_only_text),
                    right_of=dia["svg"], dx=10)
dia.add_node(RectNode("can", "CanvasViewport", 0, 0, 90, 30, node_style=html_only_style, text_style=html_only_text),
                    right_below=dia["stl"], dy=40)

height = cor.bottom.y - wig.top.y
dia.add_node(RectNode("srv", "visualife.serverside", wig.right.x + 20, wig.top.y + height/2.0, 30, height,
                    node_style=svg_only_style,
                    text_style={'fill': make_darker(palette[2], 0.6), 'angle':90}))
dia.draw(drawing)
drawing.close()

