import sys
from random import random

sys.path.append('../../')
from visualife.core import Plot, SvgViewport
from visualife.core.styles import make_darker


filename = sys.argv[1]
col_x = int(sys.argv[2])
col_y = int(sys.argv[3])
data_x =[]
data_y = []

with open(filename) as file:
	data = file.readlines()
	x_label = data[0].strip().split()[col_x-1]
	y_label = data[0].strip().split()[col_y-1]
	for line in data[1:20000]:

		tokens = line.strip().split()
		print(tokens)
		data_x.append(float(tokens[col_x-1]))
		data_y.append(float(tokens[col_y-1]))

print(len(data_x))
print(len(data_y))

print(min(data_x))
print(min(data_y))
print(max(data_x))
print(max(data_y))


drawing = SvgViewport(filename+".svg", 1200, 1200,"white")
pl = Plot(drawing,200,1000,200,1000,min(data_x)-1,max(data_x)+1,min(data_y)-1,max(data_y)+1,axes_definition="UBLR")

stroke_color = make_darker("SteelBlue", 0.3)
pl.axes["B"].label = x_label
pl.axes["L"].label = y_label
pl.axes["L"].are_tics_inside=False
pl.axes["L"].tics(0,5)
#pl.axes["B"].tics_at_fraction([0.0,0.25,0.5,0.75,1.0], ["A","B","C","D","E"])
#pl.axes["L"].tics_at_fraction([0.0,0.25,0.5,0.75,1.0], ["D","E","F","G","H"])
for key,ax in pl.axes.items() :
  ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
  ax.tics(0,5)
pl.draw_axes()
pl.draw_grid()
pl.plot_label = "Random values of x and y"
pl.draw_plot_label()

pl.scatter(data_x,data_y, markersize=6, markerstyle='s', title="serie-1")
drawing.close()

