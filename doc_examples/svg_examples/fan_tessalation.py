from visualife.core import SvgViewport
from visualife.core.styles import *

# Graphics drawn based on:
# https://codepen.io/DonKarlssonSan/pen/oNLymjJ

circle_style = {"stroke": "black", "stroke_width": "0.8", "fill": "white"}
path_style = {"stroke": "black", "stroke_width": "1.0", "fill": "none"}

width = 600
height = 600
radius = 50
palette = known_color_scales["tableau10"]
n_arcs = 15

def circles(drawing, x0, y0, idx, direction):

    x1 = -radius if direction == 'L' else radius
    y1 = radius if direction == 'D' else -radius
    x2 = 0 if direction in 'LR' else -2*radius
    y2 = 0 if direction in 'UD' else 2*radius
    fx = 0 if direction in 'UD' else -1 if direction == 'L' else 1
    fy = 0 if direction in 'LR' else  1 if direction == 'D' else -1
    arc1 = 0 if direction in 'DR' else 1
    arc2 = 0 if direction in 'LU' else 1
    arc3 = 0 if direction in 'DR' else 1

    clip_path = [["M", x0, y0], ["a", radius, radius, 0, 0, arc1, x1, y1],
                ["a", radius, radius, 0, 0, arc2, x2, y2],
                 ["A", radius, radius, 0, 0, arc3, x0, y0]]
    drawing.start_clip_path("clip%d" % idx)
    drawing.path("clippath%d" % idx, clip_path)
    drawing.close_clip_path()
    drawing.start_group("g%d" % idx, clip_path="clip%d" % idx)
    for i in range(n_arcs, 1, -1):
        delta = i/n_arcs * radius
        drawing.circle("circle%d-%d" % (idx, i), x0 + fx * delta, y0 + fy * delta, delta, **circle_style)
    drawing.close_group()


if __name__ == "__main__":

    drawing = SvgViewport("fan_tessalation.svg", width, height)
    idx = 0
    for i_column in range(-1, int(width / radius / 2) + 2):
        for i_row in range(-1, int(height / radius / 2) + 2):
            color = palette[(i_column + i_row) % 4]
            circle_style["stroke"] = color
            circle_style["fill"] = mix_colors("white", color, 0.9)
            if (i_column + i_row) % 2 == 0:
                circles(drawing, i_column * radius * 2, i_row * radius * 2, idx + 1, 'U')
                circles(drawing, i_column * radius * 2, i_row * radius * 2, idx + 2, 'D')
            else:
                circles(drawing, i_column * radius * 2, i_row * radius * 2, idx + 1, 'L')
                circles(drawing, i_column * radius * 2, i_row * radius * 2, idx + 2, 'R')
            idx += 2
    drawing.close()
