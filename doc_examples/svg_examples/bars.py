import sys
from random import random

sys.path.append('../../')
from visualife.core import SvgViewport, Plot
from visualife.core.styles import make_darker

x_data = []
y_data = []

for i in range(10):
    x_data.append(i)
    y_data.append(i/10)

drawing = SvgViewport("bars_plot.svg", 1200, 1200)
pl = Plot(drawing,200,1000,200,1000,axes_definition="UBLR")

stroke_color = make_darker("SteelBlue", 0.3)
pl.axes["B"].label = "counts"
pl.axes["L"].label = "random variable"


for key,ax in pl.axes.items():
    ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
    ax.tics(0,6)
pl.plot_label = "Normally distributed random numbers"

pl.bars(x_data, y_data)
pl.draw(grid=True)
drawing.close()
