import sys

sys.path.append('../../')

from visualife.core import SvgViewport
from visualife.diagrams import TriangularLayout, BTreeNode, balance_binary_tree


def nice_tree():
    rl = BTreeNode(6, None, BTreeNode(0, 'A'), BTreeNode(1, 'B'))
    rr = BTreeNode(7, None, BTreeNode(2, 'C'), BTreeNode(3, 'D'))
    root1 = BTreeNode(8, None, rl, rr)
    root2 = BTreeNode(8, None, BTreeNode(4, 'C'), BTreeNode(5, 'D'))
    return BTreeNode(8, None, root1, root2)


def unbalanced_tree(n_nodes):
    nid = 0
    root = BTreeNode(nid, nid)
    for i in range(n_nodes-1):
        nid +=1
        n = BTreeNode(nid, nid)
        nid += 1
        root = BTreeNode(nid, None, n, root)

    return root

if __name__ == "__main__":

    # root = nice_tree()
    root = unbalanced_tree(10)
    balance_binary_tree(root)
    vp = SvgViewport("tree.svg", 500, 500)
    drawing = TriangularLayout("tree", vp, 0, 500, 0, 500,width=50, height=10, separation=20)
    drawing.draw(root)