import sys

sys.path.append('../../')
from math import cos, sin, pi
from visualife.core import SvgViewport
from visualife.core.styles import color_name_to_hex, make_brighter
from visualife.core.shapes import grid, dots

drawing = SvgViewport("dots_and_grids.svg", 500, 500,"white")

stroke_color = color_name_to_hex("SteelBlue")

grid(drawing, "grid", 25, 25, 400, 400, step=50/3.0, stroke=make_brighter(stroke_color, 0.3), stroke_width=0.1)
grid(drawing, "grid", 25, 25, 400, 400, step=50, stroke=make_brighter(stroke_color, 0.3))

r = [5 * (sin(4*pi * i / 24.0) * cos(4*pi * j / 24.0)) ** 2 for i in range(24) for j in range(24)]
dots(drawing, "grid", 25+50/6.0, 25+50/6.0, 400, 400, r,color_name_to_hex("Plum"),
     step=50/3.0, fill=color_name_to_hex("Plum"), stroke="darker", stroke_width=0.5)

drawing.close()

