import sys

sys.path.append('../../')

from visualife.core import SvgViewport

if __name__ == "__main__":
    drawing = SvgViewport("gradient.svg",  300, 300)
    drawing.radial_gradient("grad1",["0%","white","0.9"],["100%","blue","1"],r="50%")
    for i in range(10):
        for j in range(10):
            drawing.circle(str(i),i * 30 + 15, j * 30 + 15, 10,gradient="grad1")
    drawing.close()
