import sys

sys.path.append('../../')

from visualife.core import SvgViewport
from visualife.core.styles import known_color_scales
from visualife.core.shapes import circle_segment

path_style = """ stroke="black" stroke-width="0.5" """

n_segments = 30
separation = 2 # in degrees!
r_inner = 50
r_outer = 150
palette = known_color_scales["viridis"]
angle_step = (360.0 - separation * n_segments) / n_segments

if __name__ == "__main__":
    i_path = 0
    drawing = SvgViewport("paths.svg",  300, 300)
    a_from = 0
    for i in range(n_segments):
        circle_segment(drawing,"segm", 150, 150, r_inner, r_outer, a_from, a_from+angle_step,
                               stroke="black", stroke_width="0.5", fill=palette[i % len(palette)])
        a_from += angle_step + separation
    drawing.close()
