import sys
sys.path.append('../../')
from visualife.core.SvgViewport import SvgViewport

from visualife.diagrams.InteractiveDiagram import *

drawing = SvgViewport("fahrenheit_diagram.svg",  250, 500)
diagram = InteractiveDiagram(drawing, "fahrenheit_diagram")
diagram.declare_variables(tFahr=0, tCels=0)

start = diagram.add_start(175, 50)
dane = InteractiveNode(RectNode("input", ["czytaj wartość", "zmiennej tFahr"], 0, 0, 150, 50), "tFahr = '0.6'")
diagram.add_node(dane, below=start, follows=start)

to_val = InteractiveNode(RectNode("convert", "zamień napis tFahr na liczbę", 0, 0, 150, 40), "tFahr = float(tFahr)")
diagram.add_node(to_val, below=dane, follows=dane)

convert = InteractiveNode(RectNode("convert2", ["przelicz stopnie", "Fahrenheita na Celsiusze"], 0, 0, 150, 50),
                          "tCels = 5/9.0 * (tFahr-32)")
diagram.add_node(convert, below=to_val, follows=to_val)

print_it = InteractiveNode(RectNode("print", ["wydrukuj wynik"], 0, 0, 150, 50),
                        """print("Temperatura w st. Celsjusza:", tCels)""")
diagram.add_node(print_it, below=convert, follows=convert)
print_it.next_command = convert

stop = diagram.add_stop(below=print_it)
print_it.next_command = stop

diagram.draw(drawing)
drawing.close()

