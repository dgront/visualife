import sys, re

sys.path.append('../../')

from visualife.core import SvgViewport
from visualife.diagrams import *
from visualife.data import read_newick_dictionary, tree_from_dictionary, read_clustering_tree


if __name__ == "__main__":
    if len(sys.argv) == 1:
        root = tree_from_dictionary(read_newick_dictionary("(A:0.1,B:0.2,(C:0.3,D:0.4)E:0.5,G:0.8)F:0.9"))
    else:
        root = read_clustering_tree(sys.argv[1], True)
        #root = tree_from_dictionary(read_newick_dictionary(sys.argv[1], True))

    vp = SvgViewport("tree.svg",  1500, 1500)
    # drawing = TriangularLayout("tree", vp, 750, 750, width=10, height=10, separation=5)
    # drawing = DendrogramLayout("tree", vp, 750, 750, width=10, height=10, separation=5)
    drawing = CircularDendrogram("tree", vp, 750, 750, width=3, height=10, separation=5)
    drawing.x_margin = 50
    drawing.y_margin = 50
    drawing.draw(root, scale_edges=True, node_size=2, node_stroke_width=0.01, node_color="grey", node_stroke="black",
                 leaf_size=3, leaf_color="black", leaf_stroke_width=0.05, leaves_on_axis=False,
                 edge_width=0.3, arc=350, rotation=210, edge_color="black")
    vp.close()
