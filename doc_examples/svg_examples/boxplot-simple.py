import sys

sys.path.append('../../')
from visualife.core import Plot, SvgViewport
from visualife.core.styles import make_darker

drawing = SvgViewport("boxplot.svg", 1200, 1200,"white")
pl = Plot(drawing,200,1000,200,1000,0.0,8.0,0.0,8.0,axes_definition="UBLR")

stroke_color = make_darker("SteelBlue", 0.3)
pl.axes["B"].label = ""
pl.axes["L"].label = ""
pl.axes["L"].are_tics_inside=False
pl.axes["L"].tics(0,4)

for key,ax in pl.axes.items() :
  ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
  ax.tics(0,5)

pl.draw_axes()
pl.plot_label = "Box plot demo"
pl.draw_plot_label()

boxplot_data = [[2, 0.3, 1, 2, 3, 4], [4, 1.3, 2, 3, 4, 5], [6, 1.3, 3, 4, 5, 6]]
pl.boxes(boxplot_data, box_width=100, title="serie-1")
drawing.close()

