import sys
sys.path.append('../../')
from visualife.core import SvgViewport

from visualife.diagrams import *


drawing = SvgViewport("diagrams_uml.svg", 400, 400, bgcolor="white")
uml = Diagram(drawing, "diagrams_uml")
uml.node_style["stroke_width"] = "1px"

uml.add_node(RectNode("point", "Point", 100, 50, 80, 30))
node_base = RectNode("node_base", "NodeBase", 0, 0, 80, 30)
uml.add_node(node_base, below=uml["point"], mark="inheritance", reverse_connector=True)

rect_node = RectNode("rect_node", "RectNode", node_base.left.x - 10, node_base.bottom.y + 40, 80, 30)
uml.add_node(rect_node)
dot_node = RectNode("dot_node", "DotNode", 0, 0, 80, 30)
uml.add_node(dot_node, right_of=rect_node, dx=20)
dia_node = RectNode("diam_node","DiamondNode", 0, 0, 80, 30)
conn_node = RectNode("conn_node","Connector", 0, 0, 80, 30)
uml.add_node(dia_node, below=rect_node, autoconnect=False)
uml.add_node(conn_node, below=dot_node, autoconnect=False)
uml.connect(dia_node.right, conn_node.left)
cnct1 = uml.connect(Point((conn_node.x+dia_node.x)/2, dia_node.y), node_base.bottom, mark="inheritance")

diagram = RectNode("diagram", "Diagram", 0, 0, 80, 30)
uml.add_node(diagram, right_of=uml["point"], autoconnect=False, dx=150)

cnct2 = uml.connect(diagram.left, diagram.left - Point(100, 0), node_base.right+Point(0,-5), mark="aggregation")
idiagram = RectNode("idiagram", "InteractiveDiagram", 0, 0, 120, 30)
uml.add_node(idiagram, below=diagram, mark="inheritance", reverse_connector=True)
uml.add_node(RectNode("inode", "InteractiveNode", 0, 0, 80, 30), below=idiagram, shift_by=Point(-20, 0),
             autoconnect=False)

mid_point = Point((node_base.right.x+idiagram.left.x)/2, node_base.y)
inode = uml["inode"]
uml.connect(inode.left + Point(0, -5), mid_point, node_base.right, mark="inheritance")
uml.connect(inode.left + Point(0, 5), mid_point + Point(0, 10), node_base.right + Point(0, 10), mark="composition")

uml.draw(drawing)
drawing.close()



