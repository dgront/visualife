import sys
sys.path.append('../../')
from visualife.core import SvgViewport

from visualife.diagrams import *

drawing = SvgViewport("simple_diagram.svg", 250, 300)
dia = Diagram(drawing, "simple_diagram")
dia.add_node(RectNode("start", "START", 80, 20, 80, 30, rx=20, ry=20))
dia.add_node(RectNode("dane", "i=2", 0, 0, 80, 30), below=dia["start"], mark="inheritance")
if1 = DiamondNode("if", "i &lt; 6 ?", 0, 0, 50)
dia.add_node(if1, below=dia["dane"], mark="inheritance")
dia.add_node(RectNode("stop", "STOP", 0, 0, 80, 30, rx=20, ry=20),
                         center_at=(if1.left + Point(-50, 50)), mark="inheritance")
dia.connect_xy(if1.left, dia["stop"].top)
print_i = RectNode("print", "print(i+1)", 0, 0, 80, 40)
dia.add_node(print_i, center_at=(if1.right + Point(90, 60)), mark="inheritance")
add = RectNode("add", "i += 2", 0, 0, 80, 40)
dia.add_node(add, above=print_i, mark="inheritance", reverse_connector=True)
dia.connect_xy(if1.right, add.left)

dia.connect(print_i.bottom, print_i.bottom + Point(0, 10), Point(print_i.right.x + 10, print_i.bottom.y + 10),
            Point(print_i.right.x + 10, if1.top.y - 10), if1.top - Point(0, 20), mark="inheritance")
dia.draw(drawing)

drawing.close()


