import sys

sys.path.append('../../src/')
from visualife.core import SvgViewport
from visualife.core.styles import color_by_name
from visualife.widget import SequenceFeaturesBar

drawing = SvgViewport("sequence_features.svg", 0, 0, 600, 600)

stroke_color = color_by_name("SteelBlue").create_darker(0.3)
seq_bar = SequenceFeaturesBar(drawing, ["seq1", "seq2"], stroke=stroke_color, stroke_width=1, fill="white")
seq_bar.draw()
