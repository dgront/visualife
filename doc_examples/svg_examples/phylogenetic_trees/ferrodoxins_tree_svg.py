import json, sys, logging
from math import sin, cos, pi

from visualife.core import SvgViewport
from visualife.core.shapes import circle_segment
from visualife.data import tree_from_dictionary
from visualife.core.styles import known_color_scales
from visualife.diagrams import *
from visualife.core import AxisX

IF_SUBTYPE_TREE = True                          # --- if True, the script will color a tree by subtypes
width, height = 600, 500
root = None                                     # --- root of the node that is drawn
count_by_group = []                             # --- count how many times each group_id has been assigned to a leaf
leaves = []                                     # --- leaf nodes i.e. the actual proteins

# ---------- WHITE color scheme
#bg_color = "white"
#edge_color = "black"
# ---------- BLACK vs WHITE color scheme
bg_color = "black"
edge_color = "white"

palette = known_color_scales["tableau10"]

families = {
    "_2_4Fe_4S_S": "2[4Fe_4S]",
    "_2Fe_2SS": "2Fe_2S",
    "_4Fe_4SS": "4Fe_4S",
    "_3Fe_4SS": "3Fe_4S",
    "_7Fe_8SS": "7Fe_8S",
    "_2_4Fe_4S_AlvS": "2[4Fe_4S]Alv",
    "1764_2Fe_2SS": "2Fe_4S"}

colors_by_family_dict = {
    "2[4Fe_4S]": palette[0], "2Fe_2S": palette[1], "4Fe_4S": palette[2], "3Fe_4S": palette[3], "7Fe_8S": palette[4],
    "2[4Fe_4S]Alv": palette[5]}

colors_by_taxa = {"Archaea": palette[-3], "Eukaryota": palette[-2]}

def report_counts_by_group():
    n = 0
    for key in sorted(count_by_group, key=count_by_group.get, reverse=True):
        n += count_by_group[key]
        print(key, count_by_group[key], n, n/len(leaves))


def draw_legend(viewport:SvgViewport, x:float, y:float, w:float):

    for name, color in colors_by_family_dict.items():
        color = colors_by_family_dict[name]
        name = name.replace("_","-")
        viewport.rect(name + "-color", x - w, y, w, w, fill=color, stroke=make_darker(color, 0.5))
        viewport.text(name + "-label", x + 1.5 * w, y + 0.75 * w, name, text_anchor="start", font_size=8, fill=edge_color)
        y += w * 1.5

    for name, color in colors_by_taxa.items():
        name = name.replace("_","-")
        viewport.rect(name + "-color", x, y, w/2, w, fill=color, stroke_width=0)
        viewport.text(name + "-label", x + 1.5 * w, y + 0.75 * w, name, text_anchor="start", font_size=8, fill=edge_color)
        y += w * 1.5


def draw_legend_by_size(viewport:SvgViewport, x:float, y:float, w:float):

    factor = 0.2
    left = len(leaves)
    n_max = 0
    axis_y = y
    for name in sorted(count_by_group, key=count_by_group.get, reverse=True)[:10]:
        color = colors_by_family_dict[name]
        wx = count_by_group[name] * factor
        left -= count_by_group[name]
        n_max = max(count_by_group[name], n_max)
        viewport.rect(name + "-color", x - wx, y, wx, w, fill=color, stroke=make_darker(color, 0.5))
        viewport.text(name + "-label", x + 1.5 * w, y + 0.75 * w, name, text_anchor="start", font_size=8, fill=edge_color)
        y += w * 1.5

    viewport.rect("left-color", x - wx, y, wx, w, fill=edge_color)
    viewport.text("left-label", x + 1.5 * w, y + 0.75 * w, "other families", text_anchor="start", font_size=8, fill=edge_color)
    y += w * 1.5

    for name, color in colors_by_taxa.items():
        viewport.rect(name + "-color", x, y, w/2, w, fill=color, stroke_width=0)
        viewport.text(name + "-label", x + 1.5 * w, y + 0.75 * w, name, text_anchor="start", font_size=8, fill=edge_color)
        y += w * 1.5

    axis = AxisX(axis_y - w / 2, x, x - n_max * factor, 0, n_max, 'u')
    axis.are_tics_inside = False
    axis.tics_width = 1
    axis.tics_at_values([0, 100, 200], "%d")
    axis.draw(viewport)

def assign_ferrodoxin_family(leaves:list):
    global count_by_group
    count_by_group = {}
    for node in leaves:
        for key, val in families.items():
            if node.value.startswith(key):
                node.group_id = val
                if node.group_id in count_by_group: count_by_group[node.group_id] += 1
                else: count_by_group[node.group_id] = 1
                break
        else:
            logging.error("can't parse name", node.value)
    return count_by_group


def assign_ferrodoxin_subfamily(leaves:list):

    global count_by_group
    count_by_group = {}
    for node in leaves:
        tokens = node.value.split("_")
        for t in tokens:
            if t.startswith("ST") or t.startswith("2SST") or t.startswith("4SST"):
                node.group_id = t.replace("2S", "").replace("4S", "")
                if node.group_id in count_by_group: count_by_group[node.group_id] += 1
                else: count_by_group[node.group_id] = 1
                break
    return count_by_group


def colors_by_family(root, leaves, default_color):

    clrs = [default_color for _ in range(len(root.nodes()))]  # --- Every node is black by default
    assign_ferrodoxin_family(leaves)                          # --- Assign integer to every leaf
    DendrogramNode.group_id_from_leaves(root)                 # --- Propagate assignment from leaves to upper nodes
    # ---------- Assign colors to nodes based on family assignment
    for node in root.nodes():
        if node.group_id in colors_by_family_dict:
            clrs[node.id] = colors_by_family_dict[node.group_id]
    return clrs


def colors_by_subfamily(root, leaves, default_color):
    global colors_by_family_dict

    clrs = [default_color for _ in range(len(root.nodes()))]  # --- Every node is black by default
    assign_ferrodoxin_subfamily(leaves)                       # --- List distinct FDX subtypes (ST* names)
    DendrogramNode.group_id_from_leaves(root)                 # --- Propagate assignment from leaves to upper nodes
    # ---------- Find 10 largest groups
    largest_10 = [key for key in sorted(count_by_group, key=count_by_group.get, reverse=True)[:10]]
    # ---------- Assign colors to nodes based on group_id assignment
    colors_by_family_dict = {}
    for node in root.nodes():
        if node.group_id and node.group_id in largest_10:
            clrs[node.id] = palette[int(node.group_id[2:]) % 10]
            if node.group_id not in colors_by_family_dict:
                colors_by_family_dict[node.group_id] = clrs[node.id]

    return clrs


if __name__ == "__main__":
    # ---------- Load the phylogenetic tree
    fdx_tree = sys.argv[1] if len(sys.argv) > 1 else "../INPUTS/ferredoxins/ferredoxins_tree.json"
    root = tree_from_dictionary(json.load(open(fdx_tree)))
    n_nodes = root.count_nodes()
    leaves = TreeNode.collect_leaves(root)
    print(n_nodes, "nodes loaded,",len(leaves),"leaves")

    # ---------- Load the assignment of species
    fdx_spec = sys.argv[2] if len(sys.argv) > 2 else "../INPUTS/ferredoxins/ferredoxin_species.json"
    species_dict = json.load(open(fdx_spec))
    species_for_index = ["UNKNOWN" for _ in range(n_nodes)]
    # ---------- Assign taxa for each FDX
    for node in leaves:
        for token in node.value.split("_")[3:]:
            if token in species_dict:
                species_for_index[node.id] = species_dict[token]

    vp = SvgViewport("ferredoxins.svg", width, height, bgcolor=bg_color)
    drawing = CircularDendrogram("tree", vp, width, height, width=3, height=3, separation=1, y_margin=30, x_margin=30)

    draw_legend(vp, width * 0.87, 20, 8)
    colors = colors_by_family(root, leaves, edge_color)         # ---------- Assign family ID and colors based on FDX name
    drawing.draw(root, scale_edges=True, node_size=0.5, node_stroke_width=0.05, node_color=colors, node_stroke=edge_color,
                 leaf_size=1, leaf_color=colors, leaf_stroke_width=0.1, leaves_on_axis=True,
                 draw_labels=False, edge_width=0.1, arc=350, rotation=60, edge_color=edge_color, stroke=0)

    cx, cy = drawing.cx, drawing.cy
    r_offset = 5
    a_offset = drawing.arc / len(leaves)            # --- how many degrees per each dendrogram leaf?
    for l in leaves:
        taxa = species_for_index[l.id]
        if taxa in colors_by_taxa:
            r, a = drawing.polar_position(l)
            a = a * 180 / pi
            # x, y = (r + r_offset) * cos(a) + cx, (r + r_offset) * sin(a) + cy
            # vp.circle("c",x,y,1)
            circle_segment(vp, "segm-" + str(l.id), cx, cy, r + r_offset, r + 2 * r_offset,
                           a - a_offset/2, a + a_offset/2, fill=colors_by_taxa[taxa], stroke_width=0)
    drawing.draw_axis()
    vp.close()
