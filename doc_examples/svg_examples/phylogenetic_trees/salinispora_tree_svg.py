import json, sys
from math import sin, cos, pi

from typing import List, Tuple

print(sys.path)

from visualife.core import SvgViewport
from visualife.core.shapes import circle_segment
from visualife.data import tree_from_dictionary
from visualife.core.styles import known_color_scales
from visualife.diagrams import *
from visualife.diagrams.TreeLayout import *

from phylo_trees_helper import *


width, height = 600, 500
root = None                                     # --- root of the node that is drawn
count_by_group = []                             # --- count how many times each group_id has been assigned to a leaf
leaves = []                                     # --- leaf nodes i.e. the actual proteins

palette = known_color_scales["tableau10"]
default_color = "#8A8A8A"
colored_cyp_names = ["CYP105", "CYP107", "CYP211", "CYP125", "CYP154", "CYP1005", "CYP208", "CYP244"]
colored_cyp_regex = ["("+cyp+")[A-Z]" for cyp in colored_cyp_names]
species_regex = "(Salinispora_[a-z]+?)_"


if __name__ == "__main__":
    # ---------- Load the phylogenetic tree
    fdx_tree = sys.argv[1] if len(sys.argv) > 1 else "../INPUTS/Salinispora/Salinispora_tree.json"
    root = tree_from_dictionary(json.load(open(fdx_tree)))
    n_nodes = root.count_nodes()
    leaves = TreeNode.collect_leaves(root)
    print(n_nodes, "nodes loaded,",len(leaves), "leaves")

    species_count = sort_names_by_count(leaves, [species_regex])
    species_names = [n[0] for n in species_count]
    cyp_counts = sort_names_by_count(leaves,colored_cyp_regex)
    print(cyp_counts)
    print(species_count)

    vp = SvgViewport("Salinispora.svg", width, height, bgcolor="white")
    drawing = CircularDendrogram("tree", vp, width, height, width=3, height=3, separation=1, y_margin=30, x_margin=30)

    # --- to draw legend with CYP boxes proportional to their count
    draw_legend_by_size(vp, leaves, cyp_counts, width * 0.90, 20, 8, palette, default_color=default_color)

    #--- the species legend
    draw_legend(vp, species_names, width * 0.11, 460, 8, palette[::-1])

    # ---------- Assign color by species
    colors_by_class = color_nodes_by_keys(root.nodes(), [c[0] for c in species_count], palette[::-1], default_color)
    # ---------- Assign color by selected P450
    colors_by_cyp = color_nodes_by_keys(root.nodes(), [c[0] for c in cyp_counts], palette, default_color)
    # ---------- Propagate leaf colors on nodes: a node has the same color as its leaves
    propagate_colors(root, colors_by_cyp)

    shorten_labels(leaves, "CYP", "_")
    drawing.draw(root, node_size=0.5, node_stroke_width=0.05, node_color=colors_by_cyp, node_stroke="black",
                 scale_edges=True, leaf_size=1, leaf_color=colors_by_cyp, leaf_stroke_width=0.1, leaves_on_axis=True,
                 draw_labels=True, edge_width=0.40, arc=350, rotation=60, edge_color=colors_by_cyp, stroke=0,
                 font_size=0.70, font_weight="bold", text_anchor="start", label_color=colors_by_cyp, label_r_offset=5)

    cx, cy = drawing.cx, drawing.cy
    draw_rings_annotation(vp, cx, cy, drawing.r_max, ("CYP names",6), ("species",22),
                          arc=350, rotation=60, stroke_width=0.2, length=80)
    r_offset = 20
    a_offset = 2*drawing.arc / len(leaves)            # --- how many degrees per each dendrogram leaf?
    for l in leaves:
        r, a = drawing.polar_position(l)
        a = a * 180 / pi
        x, y = (r + r_offset) * cos(a) + cx, (r + r_offset) * sin(a) + cy
        circle_segment(vp, "segm-" + str(l.id), cx, cy, r + r_offset, r + 2 + r_offset,
                       a - a_offset / 2, a + a_offset / 2, fill=colors_by_class[l.id], stroke_width=0)
    drawing.draw_axis()
    vp.close()

