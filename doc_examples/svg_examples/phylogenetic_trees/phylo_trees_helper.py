from math import sin, cos, pi

from visualife.core import SvgViewport
from visualife.diagrams import TreeNode
from visualife.core import make_darker
from typing import List, Tuple

def draw_legend(viewport:SvgViewport, colored_keys:List[str],
                x:float, y:float, w:float, palette, title=""):

    if len(title) > 0:
        viewport.text("title-label", x + 1.5 * w, y + 0.75 * w, title, fill="black",
                      text_anchor="start", font_size=8, font_weight="bold")
        y += w * 1.5

    for i, name in enumerate(colored_keys):
        color = palette[i]
        viewport.text(name + "-label", x + 1.5 * w, y + 0.75 * w, name, fill=color,
                      text_anchor="start", font_size=8, font_weight="bold")
        y += w * 1.5


def draw_legend_by_size(viewport:SvgViewport, leaves: List[TreeNode], colored_keys:List[Tuple[str, int]],
                        x:float, y:float, w:float, palette, **kwargs):
    default_color = kwargs.get("default_color","black")
    factor = 0.2
    left = len(leaves)
    n_max = 0
    axis_y = y
    for i, (name, count) in enumerate(colored_keys):
        color = palette[i % len(palette)]
        wx = count * factor
        left -= count
        n_max = max(count, n_max)
        viewport.rect(name + "-color", x - wx, y, wx, w, fill=color, stroke=make_darker(color, 0.5))
        viewport.text(name + "-label", x + 1.5 * w, y + 0.75 * w, name, text_anchor="start", font_size=8)
        y += w * 1.5

    viewport.rect("left-color", x - wx, y, wx, w, fill=default_color)
    viewport.text("left-label", x + 1.5 * w, y + 0.75 * w, "other", text_anchor="start", font_size=8)


def shorten_labels(leaves, key: str, separator: str = "_"):
    for leaf in leaves:
        lv = leaf.value.split(separator)
        for l in lv:
            if l.find(key) > -1:
                leaf.value = l
                break


def draw_rings_annotation(viewport: SvgViewport, cx, cy, r_max, *names, **kwargs):
    ring_annotation_sep = 0.3  # --- in degrees
    a = (kwargs.get("rotation", 0) - ring_annotation_sep) / 180 * pi
    width = kwargs.get("stroke_width", 0.2)
    length = kwargs.get("length", 100)
    font_size = kwargs.get("font_size", 8)
    for name, r_offset in names:
        x, y = (r_max + r_offset) * cos(a) + cx, (r_max + r_offset) * sin(a) + cy
        x_to = x + length - r_offset * cos(a)
        viewport.line("annotation-" + name, x, y, x_to, y, stroke_width=width)
        viewport.text("annotation-label" + name, x_to + 2, y + font_size / 3.0, name,
                      text_anchor="start", font_size=font_size)
