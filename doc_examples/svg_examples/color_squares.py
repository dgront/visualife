import sys

sys.path.append('../../')

from visualife.core import SvgViewport
from visualife.core.styles import mix_colors, rgb_norm_to_hex

if __name__ == "__main__":
    drawing = SvgViewport("rectangles.svg",  300, 300)
    red = rgb_norm_to_hex((1.0, 0.0, 0.0))
    blue = rgb_norm_to_hex((0.0, 0.0, 1.0))
    white = rgb_norm_to_hex((1.0, 1.0, 1.0))
    black = rgb_norm_to_hex((0.0, 0.0, 0.0))
    colors = []
    xs = []
    ys = []
    for i in range(10):
        a_mix = mix_colors(blue, red, i * 0.1)
        for j in range(10):
            fill_color = mix_colors(white, a_mix, 1 - j * 0.1)
            colors.append(fill_color)
            xs.append(i * 30 + 5)
            ys.append(j * 35 + 5)
    drawing.squares_group("sg",xs,ys,colors,20)
    drawing.close()
