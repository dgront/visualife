import math, sys

sys.path.append('../../')

from visualife.core import SvgViewport
from visualife.calc.marching_squares import marching_square
from visualife.core.styles import make_brighter
import time

def sin_sin_x_y(x, y, x_N_int, y_N_int, resolution):
    x_N2_float = x_N_int / resolution
    y_N2_float = y_N_int / resolution
    return math.sin(x / x_N2_float) * math.sin(y / y_N2_float)


# <editor-fold desc="def marching_square_test():">
def marching_square_data_test():
    x_n_int = 400
    y_n_int = 800
    resolution = 8
    x_n2_float = x_n_int / resolution
    y_n2_float = y_n_int / resolution
    x_list = [i for i in range(x_n_int)]
    y_list = [i for i in range(y_n_int)]

    data_matrix = [[(math.sin(i / x_n2_float) * math.sin(j / y_n2_float)) for i in range(x_n_int)] for j in range(y_n_int)]

    color_hex_str = "#2591a3"
    svg_viewport_obj = SvgViewport("data_draw.svg", x_n_int, y_n_int, bgcolor="black")
    threshold_list = [0.2, 0.4, 0.6, 0.8]

    start_time_obj = time.time()
    for i in range(5):
        lines_list = marching_square(x_list, y_list, threshold_list[i % 4], data=data_matrix)
        color_hex_str = make_brighter(color_hex_str, 0.5)
        print(color_hex_str)
        for line_item in lines_list:
            svg_viewport_obj.line("l", line_item[0], line_item[1], line_item[2], line_item[3], stroke=color_hex_str)
        # END of FOR ... lines_list
    # END of FOR ... range(5)
    print("--- %s seconds ---" % (time.time() - start_time_obj))
    svg_viewport_obj.close()
# </editor-fold>


def marching_square_func_param_test():
    x_n_int = 400
    y_n_int = 800
    resolution_int = 8
    x_list = [i for i in range(x_n_int)]
    y_list = [i for i in range(y_n_int)]

    color_hex_str = "#2591a3"
    svg_viewport_obj = SvgViewport("function_draw.svg", x_n_int, y_n_int, bgcolor="black")
    threshold_list = [0.2, 0.4, 0.6, 0.8]

    start_time_obj = time.time()
    for i in range(5):
        lines_list = marching_square(x_list, y_list, threshold_list[i % 4], function=sin_sin_x_y, resolution=resolution_int)
        color_hex_str = make_brighter(color_hex_str, 0.5)
        print(color_hex_str)
        for line_item in lines_list:
            svg_viewport_obj.line("l", line_item[0], line_item[1], line_item[2], line_item[3], stroke=color_hex_str)
        # END of FOR ... lines_list
    # END of FOR ... range(5)
    print("--- %s seconds ---" % (time.time() - start_time_obj))
    svg_viewport_obj.close()


if __name__ == "__main__":
    marching_square_func_param_test()
    marching_square_data_test()

