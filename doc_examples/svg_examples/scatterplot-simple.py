import sys

sys.path.append('../../')
from visualife.core import Plot, SvgViewport
from visualife.core.styles import make_darker

N = 11
xy_data = [(i / float(N), i / float(N)) for i in range(N)]
xy = [(i / float(N), i) for i in range(N)]

drawing = SvgViewport("scatterplot-simple.svg", 1300, 1300, "white")
pl = Plot(drawing, 300, 1000, 300, 1000, -100.0, 1.0, 100.0, 1.0, axes_definition="UBLR")

stroke_color = make_darker("SteelBlue", 0.3)
pl.axes["B"].label = "X"
pl.axes["L"].label = "Y"
for key, ax in pl.axes.items():
    ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
    ax.tics(0, 11)
pl.plot_label = "Y = X"

pl.scatter(xy_data, markersize=6, markerstyle='s', title="serie-1")
pl.scatter(xy, markersize=6, markerstyle='s', title="serie-1",converter_type="secondary")

pl.draw()
drawing.close()
