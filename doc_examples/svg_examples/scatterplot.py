import sys
from random import random

sys.path.append('../../')
from visualife.core import Plot, SvgViewport
from visualife.core.styles import make_darker

N = 100
xy_data = [(random(), random()) for i in range(N)]
x1_data = [random() for _ in range(N)]
y1_data = [random() for _ in range(N)]
x2_data = [random() for _ in range(N)]
y2_data = [random() for _ in range(N)]
x3_data = [random() for _ in range(N)]
y3_data = [random() for _ in range(N)]
z_data = [random() for _ in range(N)]

drawing = SvgViewport("scatterplot.svg", 1200, 1200,"white")
pl = Plot(drawing,200,1000,200,1000,0.0,1.0,0.0,1.0,axes_definition="UBLR")

stroke_color = make_darker("SteelBlue", 0.3)
pl.axes["B"].label = "random x"
pl.axes["L"].label = "random y"
pl.axes["L"].are_tics_inside=False
pl.axes["L"].tics(0, 5)
for key,ax in pl.axes.items():
  ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
  ax.tics(0,5)
pl.plot_label = "Random values of x and y"

pl.scatter(xy_data, markersize=6, markerstyle='s', title="serie-1")
pl.scatter(x1_data, y1_data, markersize=6, markerstyle='o', title="serie-2")
pl.scatter(x2_data, y2_data, markersize=10, markerstyle='t', title="serie-3")
pl.scatter(x3_data, y3_data, markersize=6, markerstyle='r', title="serie-4")
pl.draw()
drawing.close()

