import sys
from random import uniform, gauss, betavariate, triangular

sys.path.append('../../')
from math import exp
from visualife.core import Plot, SvgViewport
from visualife.core.styles import color_name_to_hex, make_darker

drawing = SvgViewport("scatterplot4.svg",1000, 1000)

N = 2000
x1_data = [ gauss(0.0,0.2) for i in range(N)]
y1_data = [ gauss(0.0,0.2) for i in range(N)]
z1_data = [ exp(-(x1_data[i]*x1_data[i] + y1_data[i]*y1_data[i])/0.04) for i in range(N) ]
pl11 = Plot(drawing,50,450,50,450,-1.0,1.0,-1.0,1.0, axes_definition="UBLR")

x2_data = [ uniform(0.0,0.5) for i in range(N)]
y2_data = [ uniform(0.0,0.5) for i in range(N)]
pl12 = Plot(drawing,550,950,50,450,0.0,0.5,0.0,0.5, axes_definition="UBLR")

x3_data = [ betavariate(2.0,5.0) for i in range(N)]
y3_data = [ betavariate(2.0,0.2) for i in range(N)]
pl21 = Plot(drawing,50,450,550,950,0.0,1.0,0.0,1.0, axes_definition="UBLR")

x4_data = [ triangular(10.0, 20.0, 15.0) for i in range(N)]
y4_data = [ triangular(0.0,10.0,5.0) for i in range(N)]
pl22 = Plot(drawing,550,950,550,950,10,20,0,10, axes_definition="UBLR")

titles = ["2D normal distribution", "2D uniform distribution", "2D Normal distribution", "2D Normal distribution"]

stroke = make_darker("SteelBlue", 0.3)
i = 0
for pl in [pl11, pl12, pl21, pl22] :
  pl.axes["B"].label = "random x"
  pl.axes["L"].label = "random y"
  pl.plot_label = titles[i]
  i+=1

fill = color_name_to_hex("SteelBlue")
# fill.alpha = 20
pl11.scatter(x1_data, y1_data, markersize=4, markerstyle='o', colors=z1_data, cmap="purples" ,adjust_range=False)
pl12.scatter(x2_data, y2_data, markersize=4, markerstyle='t', colors=2 ,adjust_range=False)
pl21.scatter(x3_data, y3_data, markersize=3, markerstyle='r', colors=3 ,adjust_range=False)
pl22.scatter(x4_data, y4_data, markersize=3, markerstyle='s', colors="Tomato" ,adjust_range=False)
for pl in [pl11, pl12, pl21, pl22] :
  pl.draw()
drawing.close()