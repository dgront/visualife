import sys
sys.path.append('../../')
from visualife.core import Plot, SvgViewport
from visualife.core.styles import make_darker
from random import random

if len(sys.argv) <2 :
  print("USAGE: python3 plot_from_file.py ../inputs/phi_psi.results [1 2]")
  exit()
filename = sys.argv[1]
col_x = 1
col_y = 2
if len(sys.argv)>2:
  col_x = int(sys.argv[2])
  col_y = int(sys.argv[3])
data_x =[]
data_y = []

with open(filename) as file:
	data = file.readlines()
	x_label = data[0].strip().split()[col_x-1]
	y_label = data[0].strip().split()[col_y-1]
	for line in data[1:]:
		tokens = line.strip().split()
		data_x.append(float(tokens[col_x-1]))
		data_y.append(float(tokens[col_y-1]))

drawing = SvgViewport(filename.split(".")[0]+".svg",1200, 1200,"white")
pl = Plot(drawing,200,1000,200,1000,min(data_x)-1,max(data_x)+1,min(data_y)-1,max(data_y)+1,axes_definition="UBLR")

stroke_color = make_darker("SteelBlue", 0.3)
pl.axes["B"].label = x_label
pl.axes["L"].label = y_label
for key,ax in pl.axes.items() :
  ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
  ax.tics(0,5)

pl.scatter(data_x,data_y,markersize=6, markerstyle='c', title="serie-1",colors=0)
pl.line(data_x,data_y, colors=0)

pl.draw(grid=True)
drawing.close()

