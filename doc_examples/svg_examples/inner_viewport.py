from random import random

from visualife.core.styles import make_brighter, known_color_scales
from visualife.core import SvgViewport

y_data = []
x_data = []
palette = known_color_scales["tableau10"]
for i in range(500):
    x_data.append(2 - 4 * random())
    y_data.append(2 - 4 * random())

w = (600 - 3 * 25) / 2
drawing = SvgViewport("plik.svg", 600, 600)
drawing.rect("rect", 5, 5, 590, 590, fill="white", stroke="grey")

inner_circles = SvgViewport("",2 * w + 25, w,viewbox=(-1, -1, 2, 2),id="inner-c")
inner_circles.circles_group("circles", x_data, y_data, palette[0], 0.02, stroke=make_brighter(palette[0], 0.5), stroke_width=0.002)
drawing.insert_viewport(inner_circles, 25, 25)

inner_squares = SvgViewport("", w, w,viewbox=(-1, -1, 2, 2), id="inner-s")
inner_squares.squares_group("squares", x_data, y_data, palette[1], 0.02, stroke=make_brighter(palette[1], 0.5), stroke_width=0.002)
drawing.insert_viewport(inner_squares, 50 + w, 50 + w)

inner_circles = SvgViewport("",  w, w, viewbox=(-1, -1, 2, 2),id="inner-r")
inner_circles.rhomb_group("rhombs", x_data, y_data, palette[2], 0.02, stroke=make_brighter(palette[2], 0.5), stroke_width=0.002)
drawing.insert_viewport(inner_circles, 25, 50 + w)

drawing.close()
