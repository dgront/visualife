import sys

sys.path.append('../../')
from visualife.core import Plot, SvgViewport
from visualife.core.styles import color_name_to_hex, make_darker


def draw_rama_plot(filename):
    phi_psi_angles = []
    for line in open(filename):
        row = line.strip().split()
        for i in range(len(row)): row[i] = float(row[i]) * 3.14159 / 180.0
        phi_psi_angles.append(row)

    stroke = make_darker("SteelBlue", 0.3)

    drawing = SvgViewport("ramachandran.svg",500, 500,
                          style='stroke:"%s";stroke_width:2;fill:"blue"' % str(stroke))
    pl = Plot(drawing, 25, 475, 25, 475, -3.1415, 3.1415, -3.1415, 3.1415, axes_definition="UBLR")

    fill = color_name_to_hex("SteelBlue")
    # fill.alpha = 20

    pl.scatter(phi_psi_angles, markersize=2)
    pl.draw(grid=True)

    drawing.close()


if __name__ == "__main__":
    filename = "../inputs/phi_psi.results"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    draw_rama_plot(filename)
