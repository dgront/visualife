import sys
from random import random
from statistics import NormalDist

sys.path.append('../../')

from visualife.core import Plot, SvgViewport
from visualife.core.styles import make_darker

SKIP=1000	# take only 1 point per 1000
observations = [float(line.strip()) for line in open(sys.argv[1])]
observations = [observations[i] for i in range(len(observations)) if i % SKIP == 0]
observations.sort()
min_o = observations[0]
max_o = observations[-1]
N = len(observations)
nd = NormalDist()
quantiles = [nd.inv_cdf((i+1)/(N+1)) for i in range(N)]
min_q = quantiles[0]
max_q = quantiles[-1]

drawing = SvgViewport("qq_plot.svg", 1200, 1200,"white")
pl = Plot(drawing,200,1000,200,1000,min_q,max_q,min_o,max_o,axes_definition="UBLR")

stroke_color = make_darker("SteelBlue", 0.3)
pl.axes["B"].label = "observation quantiles"
pl.axes["L"].label = "normal quantiles"
pl.axes["L"].are_tics_inside=False
pl.axes["L"].tics(0, 5)
for key,ax in pl.axes.items():
  ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
  ax.tics(0,5)
pl.scatter(quantiles, observations)
pl.plot_label = "Q-Q plot of random sample"

pl.draw()
drawing.close()

