import sys
sys.path.append('../../')
from visualife.core import SvgViewport

from visualife.diagrams import Diagram, Point

# Text that will be placed in box 1
msg_dane = ["Ustaw zmienne:","licznik przebiegów: i", "aktualny wyraz: s_i", "całkowita suma: s", "ile iteracji: N"]
msg_czy  = ["czy","s_i &#62; eps", "i&#60;N"]

drawing = SvgViewport("diagram.svg",  700, 700)
dia   = Diagram(drawing, "diagram1")
start, _ = dia.add_box("start", 80, 30, x=200, y=50, rx=20, ry=20)
dane, l1  = dia.add_box(msg_dane, 150, 70, below=start)
if1, l2   = dia.add_condition(msg_czy, 50, below=dane)
stop, _  = dia.add_box("STOP", 80, 30, center_at=(if1.left() + Point(-50, 50)), rx=20, ry=20, connect_xy=if1)
dodaj, _  = dia.add_box("dodaj s_i do sumy s", 120, 40, center_at=(if1.right() + Point(50, 50)), connect_xy=if1)
nowy, l3  = dia.add_box("policz nowe s_i", 120, 40, below=dodaj)
incr, l4  = dia.add_box("zwiększ i o 1", 120, 40, below=nowy)

p1 = incr.bottom() + Point(0,20)
p2 = Point(nowy.right().x+20, p1.y)
p3 = Point(p2.x, l2.y)

go_back = dia.connect(incr.bottom(), p1, p2, p3, l2)

drawing.close()
