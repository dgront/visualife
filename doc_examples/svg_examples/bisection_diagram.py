from visualife.core import SvgViewport
from visualife.diagrams.Diagram import *

drawing = SvgViewport("bisection_diagram.svg", 700, 700)
bisection = Diagram(drawing, "diagram1")

bisection.add_node(RectNode("start", "START", 200, 50, 80, 30, node_style={"rx": 20, "ry": 20}))
bisection.add_node(RectNode("input", "input data x_l, x_r, x", 0, 0, 150, 40), below=bisection["start"])
connector = bisection.add_node(RectNode("center", "find center x_c", 0, 0, 150, 40), below=bisection["input"])
if1 = DiamondNode("if1", "if x_c == x", 0, 0, 50)
bisection.add_node(if1, below=bisection["center"])
if2 = DiamondNode("if2", "if x > x_c", 0, 0, 50)
bisection.add_node(if2, center_at=if1.right + Point(50, 80))
bisection.add_node(RectNode("stop1", "STOP", 0, 0, 80, 30, node_style={"rx": 20, "ry": 20}), center_at=(if1.left + Point(-50, 50)))
bisection.add_node(RectNode("k_l", "k_l := k", 0, 0, 80, 50), center_at=(if2.left + Point(-50, 50)))
bisection.add_node(RectNode("k_r", "k_r := k", 0, 0, 80, 50), center_at=(if2.right + Point(50, 50)))

bisection.connect_xy(if1.right, if2.top)
kr = bisection["k_r"]
bisection.connect_xy(if1.left, bisection["stop1"].top)
bisection.connect_xy(if2.right, kr.top)
bisection.connect_xy(if2.left, bisection["k_l"].top)
dot_point = if2 + Point(0, 110)
dot = DotNode("dot1",dot_point.x, dot_point.y, 5)
bisection.add_node(dot)
bisection.connect_xy(dot.right, kr.bottom)
bisection.connect_xy(dot.left, bisection["k_l"].bottom)

p1 = Point(kr.right.x+10, dot.bottom.y+10)
bisection.connect(dot.bottom, dot.bottom + Point(0, 10), p1, Point(p1.x, connector.y), connector)
connector

bisection.draw(drawing)
drawing.close()
