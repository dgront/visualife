import sys
from random import random

sys.path.append('../../')
from visualife.core import Plot, SvgViewport
from visualife.core.styles import color_name_to_hex, make_darker
from math import pi,sin,cos

x_data = []
y_data = []
z_data = []

def frange(start, stop, step):
  i = start
  while i < stop:
    yield i
    i += step

for i in frange(0, 2*pi, 0.1):
  x_data.append(i)
  y_data.append( sin(i) )
  z_data.append(cos(i))
drawing = SvgViewport("lineplot.svg", 600, 600)
pl = Plot(drawing,50,550,50,550,0.0,2*pi,-1.0,1.0, axes_definition="UBLR")

stroke = make_darker("SteelBlue", 0.3)
pl.axes["L"].label = "sin(x)"
pl.axes["B"].label = "x"
pl.plot_label = "Sinus function"

fill = color_name_to_hex("SteelBlue")
# fill.alpha = 20
pl.line(x_data,y_data,adjust_range=False)
pl.line(x_data,z_data, width=3.0,adjust_range=False)
pl.draw()
drawing.close()
