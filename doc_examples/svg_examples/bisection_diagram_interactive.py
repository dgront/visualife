from visualife.core import SvgViewport
from visualife.diagrams.InteractiveDiagram import *

drawing = SvgViewport("bisection_diagram_interactive.svg", 345, 1000)

diagram = InteractiveDiagram(drawing, "bisection_diagram")

start = diagram.add_start(center_at=Point(160, 30))

tabl = InteractiveNode(RectNode("tabl", "zainicjuj tablicę tabl", 0, 0, 150, 30), "tabl = [1,2,3,5,7,9]")
diagram.add_node(tabl, below=start, follows=start, dy=20)

iks = InteractiveNode(RectNode("iks", "wczytaj szukane x", 0, 0, 150, 30), "x = input()")
diagram.add_node(iks, below=tabl, follows=tabl, dy=20)

iksi = InteractiveNode(RectNode("iksi", "zamień napis x na int", 0, 0, 150, 30), "x = int(x)")
diagram.add_node(iksi, below=iks, follows=iks, dy=20)

k_l = InteractiveNode(RectNode("k_l", "ustal lewy zakres k_l", 0, 0, 150, 30), "k_l = 0")
diagram.add_node(k_l, below=iksi, follows=iksi, dy=20)

k_p = InteractiveNode(RectNode("k_p", "ustal prawy zakres k_p", 0, 0, 150, 30), "k_p = len(tabl) - 1")
diagram.add_node(k_p, below=k_l, follows=k_l, dy=20)

while1 = InteractiveCondition(DiamondNode("while1", ["czy", "True"], 0, 0, 50), "True")
line_while = diagram.add_node(while1, below=k_p, follows=k_p)

err = InteractiveNode(RectNode("err", "drukuj komunikat", 0, 0, 120, 30), """print("Nie znaleziono",x,"w tablicy")""")
diagram.add_node(err, center_at=(while1.right + Point(50, 50)))
while1.false_command = err
stop1 = diagram.add_stop(below=err)
err.next_command = stop1

mid = InteractiveNode(RectNode("mid", "oblicz indeks srodka k_s", 0, 0, 120, 30), "k_s = int((k_p+k_l)/2)")
diagram.add_node(mid, center_at=(while1.left + Point(-50, 50)))
while1.true_command = mid
diagram.connect_xy(while1.left, mid.top)
diagram.connect_xy(while1.right, err.top)

if1 = InteractiveCondition(DiamondNode("if1", ["czy","tabl[k_s]","==","x"], 0, 0, 50), "tabl[k_s]==x")
diagram.add_node(if1, below=mid, follows=mid)

print_it = InteractiveNode(RectNode("print", "drukuj wynik", 0, 0, 120, 30), """print("znaleziono",x,"na pozycji",k_s)""")
diagram.add_node(print_it, center_at=(if1.left + Point(-50, 60)))

if1.true_command = print_it
stop2 = diagram.add_stop(below=print_it)
print_it.next_command = stop2

if2 = InteractiveCondition(DiamondNode("if2", ["czy","tabl[k_s]","&gt; x"], 0, 0, 50), "tabl[k_s]>x")
diagram.add_node(if2, center_at=(if1.right + Point(+60, 60)))
if1.false_command = if2

diagram.connect_xy(if1.left, print_it.top)
diagram.connect_xy(if1.right, if2.top)

nowy_k_p = InteractiveNode(RectNode("nowy_k_p", "nowy k_p", 0, 0, 70, 30), """k_p = k_s - 1""")
diagram.add_node(nowy_k_p, center_at=(if2.left + Point(-15, 50)))
nowy_k_l = InteractiveNode(RectNode("nowy_k_l", "nowy k_l", 0, 0, 70, 30), """k_l = k_s + 1""")
diagram.add_node(nowy_k_l, center_at=(if2.right + Point(15, 50)))

if2.true_command = nowy_k_p
if2.false_command = nowy_k_l
nowy_k_p.next_command = while1
nowy_k_l.next_command = while1
diagram.connect_xy(if2.left, nowy_k_p.top)
diagram.connect_xy(if2.right, nowy_k_l.top)

p1 = Point((nowy_k_p.node.bottom.x + nowy_k_l.node.bottom.x) / 2, nowy_k_l.node.bottom.y + 30)
dot1 = DotNode("dot1", p1.x, p1.y, 5)
diagram.add_node(dot1)
diagram.connect_yx(nowy_k_p.bottom, dot1.left)
diagram.connect_yx(nowy_k_l.bottom, dot1.right)
p2 = Point(p1.x, p1.y + 20)
p3 = Point(err.right.x + 10, p2.y)
p4 = Point(p3.x, line_while.y)
diagram.connect(dot1.bottom, p2, p3, p4, line_while)

diagram.draw(drawing)
drawing.close()

