from browser import document, html, window
from javascript import JSON

from visualife.core import Plot, HtmlViewport
from visualife.widget import AjaxCommunication, TooltipWidget
from visualife.data import tree_from_dictionary, read_fasta
from visualife.core.styles import make_darker, make_brighter
from visualife.diagrams import CircularDendrogram, collect_leaves

width, height = 500, 500
root = None                                     # --- root of the node that is drawn
top_families = []                               # --- largest families
leaves = []                                     # --- leaf nodes i.e. the actual proteins

# ---------- Generated with http://vrl.cs.brown.edu/color
palette = ['#56ebd3', '#074d65', '#bfd6fa', '#2b72e7', '#710c9e', '#f79dcc', '#834c77', '#ab5cf6', '#9ae871', '#056e12', '#2da0a1', '#cadba5', '#84241a', '#fb2d4c', '#f97930', '#bd854a', '#dfd945', '#52461f', '#36e515', '#75859d']
requests = {}


# ---------- Download a gzip-ed file using an AJAX request and decompress it
def download_gzipped_file(location, process_data):

    def decompress(evt=None):
        bytes = tree_request.response
        content = window.pako.inflate(bytes, {'to': 'string'});
        process_data(content)

    tree_request = window.XMLHttpRequest.new()
    tree_request.open('GET', location)                 #  '../_static/ferrodoxins/7Fe-7S.fasttree.gz'
    tree_request.responseType = 'arraybuffer'
    tree_request.send()
    tree_request.onloadend = decompress

def protein_id_from_fasta_description(description):
    """The function must return a sequence ID that exactly matches one of IDs from a given tree"""
    return description.split()[0]

def process_msa(msa_data):
    global leaves

    nodes_by_value = {node.value:node for node in leaves}
    msa = read_fasta(msa_data)
    for seq



def draw_tree(tree_as_json):

    global root, leaves

    root = tree_from_dictionary(JSON.parse(tree_as_json))
    vp = HtmlViewport(document['svg-tree'], 0, 0, width, height)
    drawing = CircularDendrogram("tree", vp, 0, width, 0, height, width=3, height=3, separation=1)
    drawing.x_margin = 10
    drawing.y_margin = 10
    drawing.draw(root, scale_edges=True, node_size=2, node_stroke_width=0.2, node_color="grey", node_stroke="black",
                 leaf_size=3, leaf_color="black", leaf_stroke_width=0.3, leaves_on_axis=False,
                 edge_width=0.3, arc=350, rotation=210, edge_color="black")
    vp.close()

    leaves = collect_leaves(root)
    document["tree:leaves"].bind("mouseover", mouse_over_node)


def mouse_over_node(ev):

    # --- each node's id is "tree-%d" where %d is the integer ID of that node
    node_id = int(ev.target.id[5:])
    for node in root:
        if node.id == node_id:
            print(node.id, node.value)
            break

download_gzipped_file('../_static/ferrodoxins/7Fe-7S.fasttree.json.gz', draw_tree)
download_gzipped_file('../_static/ferrodoxins/7Fe-7S.fixed.msa.gz', process_msa)

#tooltip = TooltipWidget("tooltip", "menu", "", 200, 30)
