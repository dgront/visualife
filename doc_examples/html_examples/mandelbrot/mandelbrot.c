#include <math.h>
#include <stdio.h>

unsigned int  mandelbrot_point(const float x0,const float y0, const unsigned short n_steps) {
    unsigned int  iteration = 0;
    double x2 = 0;
    double y2 = 0;
    double x, y;
    while ((x2 + y2 <=4) && (iteration < n_steps)) {
        y = (x+x) * y + y0;
        x = x2 - y2 + x0;
        x2 = x * x;
        y2 = y * y;
        ++iteration;
    }

    return iteration;
}

unsigned int mandelbrot(const float x0, const float y0, const float x1, const float y1,
        const unsigned int nxy, const unsigned int n_steps, float *out) {

    double dx = (x1-x0)/nxy;
    double dy = (y1-y0)/nxy;
    double y = y0;
    unsigned int k = 0;
    for(int i=0;i<nxy;++i) {
        double x = x0;
        for(int j=0;j<nxy;++j) {
            out[k] = sqrt(mandelbrot_point(x, y, n_steps) / (double)n_steps);
            ++k;
            x += dx;
        }
        y += dy;
    }

    return k;
}

float scale_to_1(float *data, unsigned int n_data) {
    float mmin = 1;
    float mmax = 0;
    for(int k=0;k<n_data;++k) {
        if(data[k]<mmin) mmin = data[k];
        if(data[k]>mmax) mmax = data[k];
    }
    double p = mmax - mmin;
    for(int k=0;k<n_data;++k) data[k] = (data[k]-mmin)/p;

    return p;
}

int interpolate_color_component(int left, int right, float f) {

    return (right - left) * f + left;
}

// see:
// https://stackoverflow.com/questions/16500656/which-color-gradient-is-used-to-color-mandelbrot-in-wikipedia
// https://en.wikipedia.org/wiki/Plotting_algorithms_for_the_Mandelbrot_set
// https://en.wikibooks.org/wiki/Pictures_of_Julia_and_Mandelbrot_Sets/Landscapes
int cmap_color(float max_val, float *data, unsigned int n_data, unsigned char *pixels) {

    int palette[17][3] = {
        {66, 30, 15},
        {25, 7, 26},
        {9, 1, 47},
        {4, 4, 73},
        {0, 7, 100},
        {12, 44, 138},
        {24, 82, 177},
        {57, 125, 209},
        {134, 181, 229},
        {211, 236, 248},
        {241, 233, 191},
        {248, 201, 95},
        {255, 170, 0},
        {204, 128, 0},
        {153, 87, 0},
        {106, 52, 3},
        {106, 52, 3}};

  int k = -1;
  for(unsigned int i=0;i<n_data;++i) {
    int c = data[i]*15;
    float f = (data[i]*15 - c);

    pixels[++k] = interpolate_color_component(palette[c][0], palette[c+1][0], f);
    pixels[++k] = interpolate_color_component(palette[c][1], palette[c+1][1], f);
    pixels[++k] = interpolate_color_component(palette[c][2], palette[c+1][2], f);
    pixels[++k] = 255;
  }

  return k+1;
}


int cmap_grayscale(float max_val, float *data, unsigned int n_data, unsigned char *pixels) {

  int k = -1;
  for(unsigned int i=0;i<n_data;++i) {
    int c = 1-data[i]*255;
    pixels[++k] = c;
    pixels[++k] = c;
    pixels[++k] = c;
    pixels[++k] = 255;
  }

  return k+1;
}