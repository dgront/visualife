__author__ = "Barbara Musiałowicz"
__copyright__ = "University of Warsaw"
__license__ = "Apache License, Version 2.0"

from browser import document
from visualife.core.HtmlViewport import HtmlViewport

d = 50

drawing = HtmlViewport(document['svg'],1000,700, viewbox=( -105, 0, 910, 700),download_button=False)

def title(in_english = True):
    txt = "Periodic table of elements" if in_english else "Układ okresowy pierwiastków chemicznych"
    drawing.text(id, 7*d, d+20, txt, font_style="italic",font_weight='bold',font_size=25, text_anchor='middle')

def legend(in_english = True):
    # ----------------------------------Legenda----------------------------------------#
    if in_english:
        Kolor = {"1": ("lavender", "Metals"), "2": ("PowderBlue", "Lanthanoids"), "3": ("turquoise", "Actinoids"),
                 "4": ("pink", "Metalloids"), "5": ("LemonChiffon", "Nonmetals"), "6": ("white", "Unknown properties")}
    else:
        Kolor = {"1": ("lavender", "Metale"), "2": ("PowderBlue", "Lantanowce"), "3": ("turquoise", "Aktynowce"),
             "4": ("pink", "Półmetale"), "5": ("LemonChiffon", "Niemetale"), "6": ("white", "Właściwości nieznane")}
    drawing.rect(id, d - 10, 2 * d, 8 * d + 15, 3 * d + 10, fill="white")
    for i in Kolor:
        daneL = Kolor[i]
        kolor2 = daneL[0]
        nazwaL = daneL[1]
        i = int(i)
        drawing.rect(id, 6 * d, (i + 34 / 10) * d / 2, d / 3, d / 3, fill=kolor2)
        drawing.text(id, 65 / 10 * d, (i + 4) * d / 2 - 3, nazwaL, style='underlined', font_style="italic",
                     font_weight='bold', font_size=12, text_anchor='start')
    legenda = "Legend" if in_english else "Legenda"
    masa = "- atomic mass" if in_english else "- masa atomowa"
    nazwa = "- name" if in_english else "- nazwa"
    wodor = "Hydrogen" if in_english else "Wodór"
    drawing.text(id, 3 * d, 27 / 10 * d, legenda, font_style="italic", font_weight='bold', font_size=22, text_anchor='middle')
    drawing.rect(id, d + 13, 3 * d + 10, d * 3 / 2, d * 3 / 2, fill="white")
    drawing.text(id, 2 * d, 3 * d + 25, "1.008", font_style="italic", font_weight='bold', font_size=13, text_anchor='start')
    drawing.text(id, 2 * d, 4 * d + 5, "H", font_style="italic", font_weight='bold', font_size=25, text_anchor='middle')
    drawing.text(id, 2 * d, 45 / 10 * d, wodor, font_style="italic", font_weight='bold', font_size=13, text_anchor='middle')
    drawing.text(id, 3 * d, 3 * d + 25, masa, font_style="italic", font_weight='bold', font_size=13, text_anchor='start')
    drawing.text(id, 3 * d, 4 * d, "- symbol", font_style="italic", font_weight='bold', font_size=13, text_anchor='start')
    drawing.text(id, 3 * d, 45 / 10 * d, nazwa, font_style="italic", font_weight='bold', font_size=13, text_anchor='start')
    pass

def draw_table(in_english=True):
    for i in uop:
        dane = uop[i]
        id = dane[0]
        grupa = dane[1]
        okres = dane[2]
        typ = dane[3]
        masa = dane[4]
        if int(i) >= len(english_names) : continue
        print(int(i), english_names[int(i)], dane[5])
        nazwa = english_names[int(i)] if in_english else dane[5]
        if okres >= 8:
            okres += 3 / 10
            drawing.ellipse(id, -d, (okres + 5 / 2) * d, d * 4 / 5, d / 3, fill=kolor[typ])
            drawing.circle(id, 1 / 2 * d, (okres + 1 / 4) * d, d / 3, fill=kolor[typ])
        drawing.rect(id, (grupa - 3) * d, (okres + 2) * d, d, d, fill=kolor[typ])
        drawing.text(id, (grupa - 3) * d + 25, (okres + 3) * d - 18, id, font_style="italic", font_weight='bold', font_size=17, text_anchor='middle')
        drawing.text(id, (grupa - 3) * d + 46, (okres + 3) * d - 37, masa, font_style="italic", font_size=10, text_anchor='end')
        drawing.text(id, (grupa - 3) * d + 245 / 10, (okres + 3) * d - 5, nazwa, font_style="italic", font_size=9, text_anchor='middle')
    drawing.text(id, 1 / 2 * d, (87 / 10) * d - 2, "L", font_style="italic", font_weight='bold', font_size=16, text_anchor='middle')
    drawing.text(id, 1 / 2 * d, (97 / 10) * d - 2, "A", font_style="italic", font_weight='bold', font_size=16, text_anchor='middle')
    lant = "Lanthanoids" if in_english else "Lantanowce"
    act = "Actinoids" if in_english else "Aktynowce"
    drawing.text(id, -d, (okres + 39 / 10) * d, lant, font_style="italic", font_weight='bold', font_size=11, text_anchor='middle')
    drawing.text(id, -d, (okres + 49 / 10) * d, act, font_style="italic", font_weight='bold', font_size=11, text_anchor='middle')


uop = {"1" : ("H", 1, 1, "nm", "1,0080", "Wodór"), "2" : ("He", 18, 1, "nm", "4,0026", "Hel"), "3" : ("Li", 1, 2, "m","6,9280", "Lit"),
"4" : ("Be", 2, 2, "m","9,0120", "Beryl"), "5" : ("B", 13, 2, "pm","10,810", "Bor"), "6" : ("C", 14, 2, "nm","12,011", "Węgiel"),
"7" : ("N", 15, 2, "nm","14,007", "Azot"),"8" : ("O", 16, 2, "nm","15,999", "Tlen"),
"9" : ("F", 17, 2, "nm","18,998", "Fluor"), "10" : ("Ne", 18, 2, "nm","20,180", "Neon"), "11" : ("Na", 1, 3, "m","22,990", "Sód"),
"12" : ("Mg", 2, 3, "m","24,305", "Magnez"),
"13" : ("Al", 13, 3, "m","26,982", "Glin"), "14" : ("Si", 14, 3, "pm","28,085", "Krzem"), "15" : ("P", 15, 3, "nm","30,974", "Fosfor"),
"16" : ("S", 16, 3, "nm","32,060", "Siarka"),
"17" : ("Cl", 17, 3, "nm","35,450", "Chlor"), "18" : ("Ar", 18, 3, "nm","39,950", "Argon"), "19" : ("K", 1, 4, "m","39,098", "Potas"),
"20" : ("Ca", 2, 4, "m","40,078", "Wapń"),
"21" : ("Sc", 3, 4, "m","44,956", "Skand"), "22" : ("Ti", 4, 4, "m","47,867", "Tytan"), "23" : ("V", 5, 4, "m","50,942", "Wanad"),
"24" : ("Cr", 6, 4, "m","51,996", "Chrom"),
"25" : ("Mn", 7, 4, "m","54,938", "Mangan"), "26" : ("Fe", 8, 4, "m","55,845", "Żelazo"), "27" : ("Co", 9, 4, "m","58,933", "Kobalt"),
"28" : ("Ni", 10, 4, "m","58,693", "Nikiel"),
"29" : ("Cu", 11, 4, "m","63,546", "Miedź"), "30" : ("Zn", 12, 4, "m","65,380", "Cynk"), "31" : ("Ga", 13, 4, "m","69,723", "Gal"),
"32" : ("Ge", 14, 4, "pm","72,630", "German"),
"33" : ("As", 15, 4, "pm","74,922", "Arsen"), "34" : ("Se", 16, 4, "nm","78,971", "Selen"), "35" : ("Br", 17, 4, "nm","79,904", "Brom"),
"36" : ("Kr", 18, 4, "nm","83,798", "Krypton"),
"37" : ("Rb", 1, 5, "m","85,468", "Rubid"), "38" : ("Sr", 2, 5, "m","87,620", "Stront"), "39" : ("Y", 3, 5, "m","88,906", "Itr"),
"40" : ("Zr", 4, 5, "m","91,224", "Cyrkon"),
"41" : ("Nb",5, 5, "m","92,906", "Niob"), "42" : ("Mo", 6, 5, "m","95,950", "Molibden"), "43" : ("Tc", 7, 5, "m","97,910", "Technet"),
"44" : ("Ru", 8, 5, "m","101,07", "Ruten"),
"45" : ("Rh", 9, 5, "m","102,91", "Rod"), "46" : ("Pd", 10, 5, "m","106,42", "Pallad"), "47" : ("Ag", 11, 5, "m","107,87", "Srebro"),
"48" : ("Cd", 12, 5, "m","112,41", "Kadm"),
"49" : ("In", 13, 5, "m","114,82", "Ind"), "50" : ("Sn", 14, 5, "m","118,71", "Cyna"),
"51" : ("Sb", 15, 5, "pm","121,76", "Antymon"), "52" : ("Te", 16, 5, "pm","127,60", "Tellur"), "53" : ("I", 17, 5, "nm","126,90", "Jod"),
"54" : ("Xe", 18, 5, "nm","131,29", "Ksenon"),
"55" : ("Cs", 1, 6, "m","132,91", "Cez"), "56":("Ba", 2, 6, "m","137,33", "Bar"), "57": ("La", 3, 8, "lan","138,91", "Lantan"),
"58" : ("Ce", 4, 8, "lan","140,12", "Cer"),
"59" : ("Pr", 5, 8, "lan","140,91", "Prazeodym"), "60" : ("Nd", 6, 8, "lan","144,24", "Neodym"), "61" : ("Pm", 7, 8, "lan","145,00", "Promet"),
"62" : ("Sm", 8, 8, "lan","150,36", "Samar"), "63" : ("Eu", 9, 8, "lan","151,96", "Europ"),"64" : ("Gd", 10, 8, "lan","157,25", "Gadolin"),
"65" : ("Tb", 11, 8, "lan","158,93", "Terb"), "66" : ("Dy", 12, 8, "lan","162,50", "Dysproz"), "67" : ("Ho", 13, 8, "lan","164,93", "Holm"),
"68" : ("Er", 14, 8, "lan","167,26", "Erb"), "69" : ("Tm", 15, 8, "lan","168,93", "Tul"),"70" : ("Yb", 16, 8, "lan","173,05", "Iterb"),
"71" : ("Lu", 17, 8, "lan","174,97", "Lutet"), "72" : ("Hf", 4, 6, "m","178,49", "Hafn"), "73" : ("Ta", 5, 6, "m","180,95", "Tantal"),
"74" : ("W", 6, 6, "m","183,84", "Wolfram"),
"75" : ("Re", 7, 6, "m","186,21", "Ren"), "76" : ("Os", 8, 6, "m","190,23", "Osm"), "77" : ("Ir", 9, 6, "m","192,22", "Iryd"),
"78" : ("Pt", 10, 6, "m","195,08", "Platyna"),
"79" : ("Au", 11, 6, "m","196,97", "Złoto"), "80" : ("Hg", 12, 6, "m","200,59", "Rtęć"), "81" : ("Tl", 13, 6, "m","204,38", "Tal"),
"82" : ("Pb", 14, 6, "m","207,20", "Ołów"),
"83" : ("Bi", 15, 6, "m","208,98", "Bizmut"), "84" : ("Po", 16, 6, "m","209,00", "Polon"), "85" : ("At", 17, 6, "pm","210,00", "Astat"),
"86" : ("Rn", 18, 6, "nm","222,00", "Radon"), "87" : ("Fr", 1, 7, "m","223,00", "Frans"), "88" : ("Ra", 2, 7, "m","226,00", "Rad"),
"89" : ("Ac", 3, 9, "akt","227,00", "Aktyn"),
"90" : ("Th", 4, 9, "akt","232,04", "Tor"), "91" : ("Pa", 5, 9, "akt","231,04", "Protaktyn"), "92" : ("U", 6, 9, "akt","238,03", "Uran"),
"93" : ("Np", 7, 9, "akt","237,00", "Neptun"),
"94" : ("Pu", 8, 9, "akt","244,00", "Pluton"), "95" : ("Am", 9, 9, "akt","243,00", "Ameryk"), "96" : ("Cm", 10, 9, "akt","247,00", "Klur"),
"97" : ("Bk", 11, 9, "akt","247,00", "Berkel"), "98" : ("Cf", 12, 9, "akt","251,00", "Kaliforn"), "99" : ("Es", 13, 9, "akt","252,00", "Einstein"),
"100" : ("Fm", 14, 9, "akt","257,00", "Ferm"), "101" : ("Md", 15, 9, "akt","258,00", "Medelew"), "102" : ("No", 16, 9, "akt","259,00", "Nobel"),
"103" : ("Lr", 17, 9, "akt","262,00", "Lorens"), "104" : ("Rf", 4, 7, "m","267,00", "Rutherford"), "105" : ("Db", 5, 7, "m","268,00", "Dubn"),
"106" : ("Sg", 6, 7, "m","271,00", "Seaborg"), "107" : ("Bh", 7, 7, "m","270,00", "Bohr"),
"108" : ("Hs", 8, 7, "m","269,00", "Has"), "109" : ("Mt", 9, 7, "nz","276,00", "Meitner"), "110" : ("Ds", 10, 7, "nz","281,00", "Darmsztadt"),
"111" : ("Rg", 11, 7, "nz","281,00", "Roentgen"),
"112" : ("Cn", 12, 7, "m","285,00", "Kopernik"), "113" : ("Nh", 13, 7, "nz","286,00", "Nihonium"), "114" : ("Fl", 14, 7, "nz","289,00", "Flerow"),
"115" : ("Mc", 15, 7, "nz","289,00", "Moscovium"),
"116" : ("Lv", 16, 7, "nz","293,00", "Liwermor"), "117" : ("Ts", 17, 7, "nz","294,00", "Tennessine"), "118" : ("Og", 18, 7, "nz","294,00", "Oganesson")
}
kolor = {"nm" : "LemonChiffon", "m" : "lavender", "lan" : "PowderBlue", "akt" : "turquoise", "pm" : "pink", "nz" : "white"}

english_names =["","Hydrogen", "Helium", "Lithium", "Beryllium", "Boron", "Carbon", "Nitrogen", "Oxygen", "Fluorine", "Neon",
"Sodium", "Magnesium", "Aluminium", "Silicon", "Phosphorus", "Sulfur", "Chlorine", "Argon", "Potassium", "Calcium", "Scandium",
"Titanium", "Vanadium", "Chromium", "Manganese", "Iron", "Cobalt", "Nickel", "Copper", "Zinc", "Gallium", "Germanium",
"Arsenic", "Selenium", "Bromine", "Krypton", "Rubidium", "Strontium", "Yttrium", "Zirconium", "Niobium", "Molybdenum",
"Technetium", "Ruthenium", "Rhodium", "Palladium", "Silver", "Cadmium", "Indium", "Tin", "Antimony", "Tellurium", "Iodine",
"Xenon", "Caesium", "Barium", "Lanthanum", "Cerium", "Praseodymium", "Neodymium", "Promethium", "Samarium", "Europium",
"Gadolinium", "Terbium", "Dysprosium", "Holmium", "Erbium", "Thulium", "Ytterbium", "Lutetium", "Hafnium", "Tantalum",
"Tungsten", "Rhenium", "Osmium", "Iridium", "Platinum", "Gold", "Mercury", "Thallium", "Lead", "Bismuth", "Polonium",
"Astatine", "Radon", "Francium", "Radium", "Actinium", "Thorium", "Protactinium", "Uranium", "Neptunium", "Plutonium",
"Americium", "Curium", "Berkelium", "Californium", "Einsteinium", "Fermium", "Mendelevium", "Nobelium", "Lawrencium",
"Rutherfordium", "Dubnium", "Seaborgium", "Bohrium", "Hassium", "Meitnerium", "Darmstadtium", "Roentgenium", "Copernicium",
"Nihonium", "Flerovium", "Moscovium", "Livermorium", "Tennessine", "Oganesson"]

title(True)             # --- draw title
legend(True)            # --- draw legend
draw_table(True)        # --- draw table
drawing.close()
