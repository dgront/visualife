import random, math
import matplotlib.pyplot as plt
SIZE = 500


def wavelength_to_rgb(wavelength, gamma=0.8):

    '''This converts a given wavelength of light to an
    approximate RGB color value. The wavelength must be given
    in nanometers in the range from 380 nm through 750 nm
    (789 THz through 400 THz).
    Based on code by Dan Bruton
    http://www.physics.sfasu.edu/astro/color/spectra.html
    The code taken from:
    http://www.noah.org/wiki/Wavelength_to_RGB_in_Python
    '''

    wavelength = float(wavelength)
    if wavelength >= 380 and wavelength <= 440:
        attenuation = 0.3 + 0.7 * (wavelength - 380) / (440 - 380)
        R = ((-(wavelength - 440) / (440 - 380)) * attenuation) ** gamma
        G = 0.0
        B = (1.0 * attenuation) ** gamma
    elif wavelength >= 440 and wavelength <= 490:
        R = 0.0
        G = ((wavelength - 440) / (490 - 440)) ** gamma
        B = 1.0
    elif wavelength >= 490 and wavelength <= 510:
        R = 0.0
        G = 1.0
        B = (-(wavelength - 510) / (510 - 490)) ** gamma
    elif wavelength >= 510 and wavelength <= 580:
        R = ((wavelength - 510) / (580 - 510)) ** gamma
        G = 1.0
        B = 0.0
    elif wavelength >= 580 and wavelength <= 645:
        R = 1.0
        G = (-(wavelength - 645) / (645 - 580)) ** gamma
        B = 0.0
    elif wavelength >= 645 and wavelength <= 750:
        attenuation = 0.3 + 0.7 * (750 - wavelength) / (750 - 645)
        R = (1.0 * attenuation) ** gamma
        G = 0.0
        B = 0.0
    else:
        R = 0.0
        G = 0.0
        B = 0.0
    # R *= 255
    # G *= 255
    # B *= 255
    return (int(R), int(G), int(B))


X = []
Y = []
C = []
for i in range(100000):
    wvl = random.uniform(380,750)
    l = 1.0 / wvl
    # see X. Quan and E. S. Fry, “Empirical equation for the index of refraction of seawater,” Appl. Opt.34,3477–3480~1995
    n = 1.31279 + 15.762 * l - 4382 * l * l + 1.1455e6 * l * l * l
    # ---------- select impact parameter from random numbers
    x, y = random.uniform(-1.0,1.0), random.uniform(-1.0,1.0)
    b = math.sqrt(x*x + y*y)
    if b > 1: continue
    theta_i = math.asin(b)
    theta_r = math.asin(b / n)

    theta_p = 4 * theta_r - 2 * theta_i             #  primary reflection
    theta_s = 6 * theta_r - 2 * theta_i - math.pi   # secondary reflection
    # intensities of a principle and a secondary reflection
    p = math.sin(theta_i - theta_r) / math.sin(theta_i + theta_r)
    p = p*p
    s = math.tan(theta_i - theta_r) / math.tan(theta_i + theta_r)
    s = s*s
    intensity_p = 0.5 * (s * (1 - s) * (1 - s) + p * (1 - p) * (1 - p))
    intensity_s = 0.5 * (s * s * (1 - s) * (1 - s) + p * p * (1 - p) * (1 - p))
    if intensity_p > 0.1 * random.random():
        theta = math.fabs(theta_p)
    elif intensity_s > 0.02 * random.random():
        theta = math.fabs(theta_s)
    else:
        continue

    rgb = wavelength_to_rgb(wvl)
    xp = 150*(theta/3.0*math.pi)*(x/b)
    if xp > 200 or xp < -200 : continue
    yp = 159 + 159*(theta/3.0*math.pi)*math.fabs(y/b)
    X.append(xp)
    Y.append(yp)
    C.append(rgb)
#    print(xp, yp, wvl, n, rgb)

plt.scatter(X, Y, c=C, s=0.01)
plt.show()
