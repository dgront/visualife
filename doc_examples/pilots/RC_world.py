import sys
import re
import math
import time
import datetime
import numpy as np
sys.path.append('../../')
from visualife.core import Plot, SvgViewport
from visualife.core.styles import color_by_name

import xml.etree.ElementTree as ET

camera = {'x': -15, 'y': 15, 'z': 30}
focalLength = 1000

cameraForward = {'x': -1 * camera['x'], 'y': -1 * camera['y'], 'z': -1 * camera['z']}
cameraPerpendicular = {'x': cameraForward['y'], 'y': -1 * cameraForward['x'], 'z': 0}
radius = 10
pi = 3.14159

# magnitude of a 3D vector
def sumOfSquares(vector):
    return vector['x'] ** 2 + vector['y'] ** 2 + vector['z'] ** 2


def magnitude(vector):
    return math.sqrt(sumOfSquares(vector))


# converts dictionary vector to list vector
def vectorToList(vector):
    return [vector['x'], vector['y'], vector['z']]


# projects u onto v
def vectorProject(u, v):
    return np.dot(vectorToList(v), vectorToList(u)) / magnitude(v)


# get unit vector
def unitVector(vector):
    magVector = magnitude(vector)
    return {'x': vector['x'] / magVector, 'y': vector['y'] / magVector, 'z': vector['z'] / magVector}


# Calculates vector from two points, dictionary form
def findVector(origin, point):
    return {'x': point['x'] - origin['x'], 'y': point['y'] - origin['y'], 'z': point['z'] - origin['z']}


# Calculates horizon plane vector (points upward)
cameraHorizon = {'x': np.cross(vectorToList(cameraForward), vectorToList(cameraPerpendicular))[0],
                 'y': np.cross(vectorToList(cameraForward), vectorToList(cameraPerpendicular))[1],
                 'z': np.cross(vectorToList(cameraForward), vectorToList(cameraPerpendicular))[2]}


def physicalProjection(point):
    pointVector = findVector(camera, point)
    # pointVector is a vector starting from the camera and ending at a point in question
    return {'x': vectorProject(pointVector, cameraPerpendicular), 'y': vectorProject(pointVector, cameraHorizon),
            'z': vectorProject(pointVector, cameraForward)}


# draws points onto camera sensor using xDistance, yDistance, and zDistance
def perspectiveProjection(pCoords):
    scaleFactor = focalLength / pCoords['z']
    return {'x': pCoords['x'] * scaleFactor, 'y': pCoords['y'] * scaleFactor}


# converts spherical coordinates to rectangular coordinates
def sphereToRect(r, a, b):
    return {'x': r * math.sin(b * pi / 180) * math.cos(a * pi / 180),
            'y': r * math.sin(b * pi / 180) * math.sin(a * pi / 180), 'z': r * math.cos(b * pi / 180)}


# converts spherical coordinates to rectangular coordinates
def sphereToRect(r, a, b):
    aRad = math.radians(a)
    bRad = math.radians(b)
    r_sin_b = r * math.sin(bRad)
    return {'x': r_sin_b * math.cos(aRad), 'y': r_sin_b * math.sin(aRad), 'z': r * math.cos(bRad)}


# functions for plotting points
def rectPlot(coordinate):
    return perspectiveProjection(physicalProjection(coordinate))


def spherePlot(coordinate, sRadius):
    return rectPlot(sphereToRect(sRadius, coordinate['long'], coordinate['lat']))


def parse_map(map_fname):
    tree = ET.parse(map_fname)
    root = tree.getroot()

    countries = len(root[4][0]) - 1
    for x in range(countries):
        root[4][0][x + 1]

if __name__ == "__main__":
    stroke = color_by_name("SteelBlue").create_darker(0.3)

    drawing = SvgViewport("world.svg", 0, 0, 800, 800, style='stroke:"%s";stroke_width:1;fill:"blue"' % str(stroke))
    for x in range(72):
        for y in range(36):
            aRad = math.radians(5*x)
            bRad = math.radians(5*y)
            r_sin_b = radius * math.sin(bRad)
            cx = r_sin_b * math.cos(aRad)
            cy = r_sin_b * math.sin(aRad)
            cz = radius * math.cos(bRad)
            out = rectPlot({'x': r_sin_b * math.cos(aRad), 'y': r_sin_b * math.sin(aRad), 'z': radius * math.cos(bRad)})
            drawing.circle("c",out['x']+400,out['y']+400,1)
    drawing.close()

    parse_map("BlankMap-Equirectangular_abs-1.svg")
