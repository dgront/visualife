import sys
sys.path.append('../../')
from visualife.core import Plot, SvgViewport
from visualife.core.styles import make_darker
from random import random

filename = sys.argv[1]
res1 = "38"
res2 = "101"
data_x =[]
data_y =[]
res_data = {}
with open(filename) as file:
    data = file.readlines()
    x_label = "distance"
    y_label = "energy"
    for line in data:
        tokens = line.strip().split()
        data_x =[]
        data_y =[]
        if tokens[0]=="AtomPair":
            
            for i in tokens[12:47]:
                data_x.append(float(i))
            for j in tokens[48:]:
                data_y.append(float(j))
            res_data[tokens[2]+"-"+tokens[4]]=(data_x,data_y)
        else:
            continue
if res1+"-"+res2 not in res_data:
    print("no data for residues %s and %s" % (res1,res2))
    exit()
data_x = res_data[res1 +"-"+res2][0]
data_y = res_data[res1 +"-"+res2][1]
title = "residues "+res1+" and "+res2
drawing = SvgViewport(filename.split(".")[0]+".svg", 0, 0, 1200, 1200,"white")
pl = Plot(drawing,200,1000,200,1000,min(data_x)-1,max(data_x)+1,min(data_y)-1,max(data_y)+1,axes_definition="UBLR")
pl.plot_label = title
stroke_color = make_darker("SteelBlue", 0.3)
pl.axes["B"].label = x_label
pl.axes["L"].label = y_label
for key,ax in pl.axes.items() :
  ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
  ax.tics(0,5)

pl.scatter(data_x,data_y,markersize=6, markerstyle='c', title="serie-1",colors=0)
#pl.line(data_x,data_y, colors=0)

pl.draw(grid=True)
drawing.close()

