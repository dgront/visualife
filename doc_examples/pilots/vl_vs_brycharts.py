import timeit, math

from browser import document
import brycharts
from visualife.core import Plot, HtmlViewport
from visualife.core.styles import make_darker


def load_data(fname, n_repeats=1):
    lines = open(fname).readlines()
    data = [line.strip().split(",") for line in lines]
    data_x = [float(d[7]) for d in data[1:501]]
    data_y = [float(d[5]) for d in data[1:501]]
    labels = [d[0] for d in data[1:501]]
    x, y, l = [], [], []
    for i in range(n_repeats):
        x.extend(data_x)
        y.extend(data_y)
        l.extend(labels)
    return x, y, l


def plot_vl(data_x, data_y):
    x_label = "Average Salary Index"
    y_label = "Total Cost of Living Index"

    drawing = HtmlViewport(document["svg"], 0, 0, 600, 600, "white")
    pl = Plot(drawing, 100, 500, 100, 500, min(data_x) - 1, max(data_x) + 1, min(data_y) - 1, max(data_y) + 1,
              axes_definition="UBLR")

    stroke_color = make_darker("SteelBlue", 0.3)
    pl.axes["B"].label = x_label
    pl.axes["L"].label = y_label
    for key, ax in pl.axes.items():
        ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
        ax.tics(0, 5)

    pl.scatter(data_x, data_y, markersize=3, markerstyle='r', title="serie-1", colors=0)

    pl.draw(grid=True)
    drawing.close()

def plot_brycharts(data_x, data_y, labels):
    data = {l: (x, y) for l, x, y in zip(labels, data_x, data_y)}
    lpd = brycharts.LabelledPairedData("Average Salary Index", "Total Cost of Living Index", data)
    brycharts.ScatterGraph(document, lpd, width="600px")

x, y, l = load_data("cost-of-living-2018.csv",10)
timing = []
for n in range(10):
    document["svg"].innerHTML = ""
    start = timeit.default_timer()
    plot_vl(x, y)
    #plot_brycharts(x, y, l)
    stop = timeit.default_timer()
    print(stop-start)
    timing.append(stop-start)
avg = sum(timing) / len(timing)
var = sum((x-avg)**2 for x in timing) / len(timing)

print(avg, math.sqrt(var))
